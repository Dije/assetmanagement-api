package main

import (
	"deliportal-api/config"
	"deliportal-api/controller"
	"deliportal-api/middleware"
	"deliportal-api/repository"
	"deliportal-api/service"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

var (
	db                       *gorm.DB                            = config.ConnectDataBase()
	divisionRepository       repository.DivisionRepository       = repository.NewDivisionRepository(db)
	departmentRepository     repository.DepartmentRepository     = repository.NewDepartmentRepository(db)
	sectionRepository        repository.SectionRepository        = repository.NewSectionRepository(db)
	positionRepository       repository.PositionRepository       = repository.NewPositionRepository(db)
	locationRepository       repository.LocationRepository       = repository.NewLocationRepository(db)
	employeeRepository       repository.EmployeeRepository       = repository.NewEmployeeRepository(db)
	userRepository           repository.UserRepository           = repository.NewUserRepository(db)
	formRepository           repository.FormRepository           = repository.NewFormRepository(db)
	formTypeRepository       repository.FormTypeRepository       = repository.NewFormTypeRepository(db)
	roleRepository           repository.RoleRepository           = repository.NewRoleRepository(db)
	userRoleRepository       repository.UserRoleRepository       = repository.NewUserRoleRepository(db)
	roleFormRepository       repository.RoleFormRepository       = repository.NewRoleFormRepository(db)
	processorRepository      repository.ProcessorRepository      = repository.NewProcessorRepository(db)
	motherboardRepository    repository.MotherboardRepository    = repository.NewMotherboardRepository(db)
	ramRepository            repository.RAMRepository            = repository.NewRAMRepository(db)
	hddRepository            repository.HDDRepository            = repository.NewHDDRepository(db)
	casingRepository         repository.CasingRepository         = repository.NewCasingRepository(db)
	keyboardRepository       repository.KeyboardRepository       = repository.NewKeyboardRepository(db)
	mouseRepository          repository.MouseRepository          = repository.NewMouseRepository(db)
	monitorRepository        repository.MonitorRepository        = repository.NewMonitorRepository(db)
	osRepository             repository.OSRepository             = repository.NewOSRepository(db)
	officeRepository         repository.OfficeRepository         = repository.NewOfficeRepository(db)
	computerRepository       repository.ComputerRepository       = repository.NewComputerRepository(db)
	laptopRepository         repository.LaptopRepository         = repository.NewLaptopRepository(db)
	serviceLaptopRepository  repository.ServiceLaptopRepository  = repository.NewServiceLaptopRepository(db)
	inkRepository            repository.InkRepository            = repository.NewInkRepository(db)
	printerRepository        repository.PrinterRepository        = repository.NewPrinterRepository(db)
	userPrinterRepository    repository.UserPrinterRepository    = repository.NewUserPrinterRepository(db)
	servicePrinterRepository repository.ServicePrinterRepository = repository.NewServicePrinterRepository(db)
	upsRepository            repository.UPSRepository            = repository.NewUPSRepository(db)
	serviceUPSRepository     repository.ServiceUPSRepository     = repository.NewServiceUPSRepository(db)
	antivirusRepository      repository.AntivirusRepository      = repository.NewAntivirusRepository(db)
	harddiskRepository       repository.HarddiskRepository       = repository.NewHarddiskRepository(db)
	subscriptionRepository   repository.SubscriptionRepository   = repository.NewSubscriptionRepository(db)
	authService              service.AuthService                 = service.NewAuthService(userRepository)
	divisionService          service.DivisionService             = service.NewDivisionService(divisionRepository)
	departmentService        service.DepartmentService           = service.NewDepartmentService(departmentRepository)
	sectionService           service.SectionService              = service.NewSectionService(sectionRepository)
	positionService          service.PositionService             = service.NewPositionService(positionRepository)
	locationService          service.LocationService             = service.NewLocationService(locationRepository)
	employeeService          service.EmployeeService             = service.NewEmployeeService(employeeRepository)
	userService              service.UserService                 = service.NewUserService(userRepository)
	formService              service.FormService                 = service.NewFormService(formRepository)
	formTypeService          service.FormTypeService             = service.NewFormTypeService(formTypeRepository)
	roleService              service.RoleService                 = service.NewRoleService(roleRepository)
	userRoleService          service.UserRoleService             = service.NewUserRoleService(userRoleRepository)
	roleFormService          service.RoleFormService             = service.NewRoleFormService(roleFormRepository)
	processorService         service.ProcessorService            = service.NewProcessorService(processorRepository)
	motherboardService       service.MotherboardService          = service.NewMotherboardService(motherboardRepository)
	ramService               service.RAMService                  = service.NewRAMService(ramRepository)
	hddService               service.HDDService                  = service.NewHDDService(hddRepository)
	casingService            service.CasingService               = service.NewCasingService(casingRepository)
	keyboardService          service.KeyboardService             = service.NewKeyboardService(keyboardRepository)
	mouseService             service.MouseService                = service.NewMouseService(mouseRepository)
	monitorService           service.MonitorService              = service.NewMonitorService(monitorRepository)
	osService                service.OSService                   = service.NewOSService(osRepository)
	officeService            service.OfficeService               = service.NewOfficeService(officeRepository)
	computerService          service.ComputerService             = service.NewComputerService(computerRepository)
	laptopService            service.LaptopService               = service.NewLaptopService(laptopRepository)
	serviceLaptopService     service.ServiceLaptopService        = service.NewServiceLaptopService(serviceLaptopRepository)
	inkService               service.InkService                  = service.NewInkService(inkRepository)
	printerService           service.PrinterService              = service.NewPrinterService(printerRepository)
	userPrinterService       service.UserPrinterService          = service.NewUserPrinterService(userPrinterRepository)
	servicePrinterService    service.ServicePrinterService       = service.NewServicePrinterService(servicePrinterRepository)
	upsService               service.UPSService                  = service.NewUPSService(upsRepository)
	serviceUPSService        service.ServiceUPSService           = service.NewServiceUPSService(serviceUPSRepository)
	antivirusService         service.AntivirusService            = service.NewAntivirusService(antivirusRepository)
	harddiskService          service.HarddiskService             = service.NewHarddiskService(harddiskRepository)
	subscriptionService      service.SubscriptionService         = service.NewSubscriptionService(subscriptionRepository)
	jwtService               service.JWTService                  = service.NewJWTService()
	authController           controller.AuthController           = controller.NewAuthController(authService, jwtService)
	divisionController       controller.DivisionController       = controller.NewDivisionController(divisionService, jwtService)
	departmentController     controller.DepartmentController     = controller.NewDepartmentController(departmentService, jwtService)
	sectionController        controller.SectionController        = controller.NewSectionController(sectionService, jwtService)
	positionController       controller.PositionController       = controller.NewPositionController(positionService, jwtService)
	locationController       controller.LocationController       = controller.NewLocationController(locationService, jwtService)
	employeeController       controller.EmployeeController       = controller.NewEmployeeController(employeeService, jwtService)
	userController           controller.UserController           = controller.NewUserController(userService, jwtService)
	formController           controller.FormController           = controller.NewFormController(formService, jwtService)
	formTypeController       controller.FormTypeController       = controller.NewFormTypeController(formTypeService, jwtService)
	roleController           controller.RoleController           = controller.NewRoleController(roleService, jwtService)
	userRoleController       controller.UserRoleController       = controller.NewUserRoleController(userRoleService, jwtService)
	roleFormController       controller.RoleFormController       = controller.NewRoleFormController(roleFormService, jwtService)
	processorController      controller.ProcessorController      = controller.NewProcessorController(processorService, jwtService)
	motherboardController    controller.MotherboardController    = controller.NewMotherboardController(motherboardService, jwtService)
	ramController            controller.RAMController            = controller.NewRAMController(ramService, jwtService)
	hddController            controller.HDDController            = controller.NewHDDController(hddService, jwtService)
	casingController         controller.CasingController         = controller.NewCasingController(casingService, jwtService)
	keyboardController       controller.KeyboardController       = controller.NewKeyboardController(keyboardService, jwtService)
	mouseController          controller.MouseController          = controller.NewMouseController(mouseService, jwtService)
	monitorController        controller.MonitorController        = controller.NewMonitorController(monitorService, jwtService)
	osController             controller.OSController             = controller.NewOSController(osService, jwtService)
	officeController         controller.OfficeController         = controller.NewOfficeController(officeService, jwtService)
	computerController       controller.ComputerController       = controller.NewComputerController(computerService, jwtService)
	laptopController         controller.LaptopController         = controller.NewLaptopController(laptopService, jwtService)
	serviceLaptopController  controller.ServiceLaptopController  = controller.NewServiceLaptopController(serviceLaptopService, jwtService)
	inkController            controller.InkController            = controller.NewInkController(inkService, jwtService)
	printerController        controller.PrinterController        = controller.NewPrinterController(printerService, jwtService)
	userPrinterController    controller.UserPrinterController    = controller.NewUserPrinterController(userPrinterService, jwtService)
	servicePrinterController controller.ServicePrinterController = controller.NewServicePrinterController(servicePrinterService, jwtService)
	upsController            controller.UPSController            = controller.NewUPSController(upsService, jwtService)
	serviceUPSController     controller.ServiceUPSController     = controller.NewServiceUPSController(serviceUPSService, jwtService)
	antivirusController      controller.AntivirusController      = controller.NewAntivirusController(antivirusService, jwtService)
	harddiskController       controller.HarddiskController       = controller.NewHarddiskController(harddiskService, jwtService)
	subscriptionController   controller.SubscriptionController   = controller.NewSubscriptionController(subscriptionService, jwtService)
)

func main() {
	defer config.CloseDatabaseConnection(db)

	r := gin.Default()

	authGroup := r.Group("/api/auth")
	{
		authGroup.POST("/login", authController.Login)
		authGroup.POST("/register", authController.Register)
		authGroup.POST("/checkEx", authController.CheckExisting)
		authGroup.POST("/sendMail", authController.SendMail)
		authGroup.POST("/password/:username/:email", authController.UpdateDataPassword)
		authGroup.POST("/upReq/:username/:email", authController.UpdateDataRequest)
	}

	divisionGroup := r.Group("/api/division", middleware.AuthorizeJWT(jwtService))
	{
		divisionGroup.GET("/", divisionController.FindDivisions)
		divisionGroup.GET("/:id", divisionController.FindDivisionById)
		divisionGroup.GET("/exc/:id", divisionController.FindExcDivision)
		divisionGroup.POST("/", divisionController.InsertDivision)
		divisionGroup.PUT("/:id", divisionController.UpdateDivision)
		divisionGroup.DELETE("/:id", divisionController.DeleteDivision)
	}

	departmentGroup := r.Group("/api/department", middleware.AuthorizeJWT(jwtService))
	{
		departmentGroup.GET("/", departmentController.FindDepartments)
		departmentGroup.GET("/:id", departmentController.FindDepartmentById)
		departmentGroup.GET("/exc/:divId/:id", departmentController.FindExcDepartment)
		departmentGroup.GET("/byDivision/:divId", departmentController.FindDepartmentByDivId)
		departmentGroup.POST("/", departmentController.InsertDepartment)
		departmentGroup.PUT("/:id", departmentController.UpdateDepartment)
		departmentGroup.DELETE("/:id", departmentController.DeleteDepartment)
	}

	sectionGroup := r.Group("/api/section", middleware.AuthorizeJWT(jwtService))
	{
		sectionGroup.GET("/", sectionController.FindSections)
		sectionGroup.GET("/:id", sectionController.FindSectionById)
		sectionGroup.GET("/exc/:depId/:id", sectionController.FindExcSection)
		sectionGroup.GET("/byDepartment/:depId", sectionController.FindSectionByDepId)
		sectionGroup.POST("/", sectionController.InsertSection)
		sectionGroup.PUT("/:id", sectionController.UpdateSection)
		sectionGroup.DELETE("/:id", sectionController.DeleteSection)
	}

	positionGroup := r.Group("/api/position", middleware.AuthorizeJWT(jwtService))
	{
		positionGroup.GET("/", positionController.FindPositions)
		positionGroup.GET("/:id", positionController.FindPositionById)
		positionGroup.GET("/exc/:id", positionController.FindExcPosition)
		positionGroup.POST("/", positionController.InsertPosition)
		positionGroup.PUT("/:id", positionController.UpdatePosition)
		positionGroup.DELETE("/:id", positionController.DeletePosition)
	}

	locationGroup := r.Group("/api/location", middleware.AuthorizeJWT(jwtService))
	{
		locationGroup.GET("/", locationController.FindLocations)
		locationGroup.GET("/:id", locationController.FindLocationById)
		locationGroup.GET("/exc/:id", locationController.FindExcLocation)
		locationGroup.POST("/", locationController.InsertLocation)
		locationGroup.PUT("/:id", locationController.UpdateLocation)
		locationGroup.DELETE("/:id", locationController.DeleteLocation)
	}

	employeeGroup := r.Group("/api/employee", middleware.AuthorizeJWT(jwtService))
	{
		employeeGroup.GET("/", employeeController.FindEmployees)
		employeeGroup.GET("/:id", employeeController.FindEmployeeById)
		employeeGroup.GET("/byNik/:nik", employeeController.FindEmployeeByNik)
		employeeGroup.GET("/exc/:id", employeeController.FindExcEmployee)
		employeeGroup.POST("/", employeeController.InsertEmployee)
		employeeGroup.PUT("/:id", employeeController.UpdateEmployee)
		employeeGroup.DELETE("/:id", employeeController.DeleteEmployee)
	}

	userGroup := r.Group("/api/user", middleware.AuthorizeJWT(jwtService))
	{
		userGroup.GET("/", userController.FindUsers)
		userGroup.GET("/:id", userController.FindUserById)
		userGroup.GET("/byNUName/:uName", userController.FindUserByUName)
		userGroup.GET("/exc/:id", userController.FindExcUser)
		userGroup.POST("/", userController.InsertUser)
		userGroup.PUT("/:id", userController.UpdateUser)
		userGroup.DELETE("/:id", userController.DeleteUser)
	}

	formGroup := r.Group("/api/form", middleware.AuthorizeJWT(jwtService))
	{
		formGroup.GET("/", formController.FindForms)
		formGroup.GET("/formJoinRole/:uId/:fpId", formController.FindFormJoinRole)
		formGroup.GET("/formByRole/:uId", formController.FindFormByRole)
		formGroup.GET("/formByType/:tyId", formController.FindFormByType)
		formGroup.GET("/excFormByType/:tyId/:id", formController.FindExcFormByType)
		formGroup.GET("/:id", formController.FindFormById)
		formGroup.GET("/formByFormTypeId/:ftId", formController.FindFormByFormTypeId)
		formGroup.GET("/excForm/:ftId/:id", formController.FindExcForm)
		formGroup.GET("/formHead/:ftId", formController.FindFormHead)
		formGroup.GET("/formHeadDetail/:id", formController.FindFormHeadDetail)
		formGroup.GET("/excFormHead/:id", formController.FindExcFormHead)
		formGroup.GET("/excFormOnly/:id", formController.FindExcFormOnly)
		formGroup.POST("/", formController.InsertForm)
		formGroup.PUT("/:id", formController.UpdateForm)
		formGroup.DELETE("/:id", formController.DeleteForm)
	}

	formTypeGroup := r.Group("/api/FormType", middleware.AuthorizeJWT(jwtService))
	{
		formTypeGroup.GET("/", formTypeController.FindFormTypes)
		formTypeGroup.GET("/:id", formTypeController.FindFormTypeById)
		formTypeGroup.GET("/exc/:id", formTypeController.FindExcFormType)
		formTypeGroup.POST("/", formTypeController.InsertFormType)
		formTypeGroup.PUT("/:id", formTypeController.UpdateFormType)
		formTypeGroup.DELETE("/:id", formTypeController.DeleteFormType)
	}

	roleGroup := r.Group("/api/role", middleware.AuthorizeJWT(jwtService))
	{
		roleGroup.GET("/", roleController.FindRoles)
		roleGroup.GET("/:id", roleController.FindRoleById)
		roleGroup.GET("/exc/:id", roleController.FindExcRole)
		roleGroup.POST("/", roleController.InsertRole)
		roleGroup.PUT("/:id", roleController.UpdateRole)
		roleGroup.DELETE("/:id", roleController.DeleteRole)
	}

	userRoleGroup := r.Group("/api/UserRole", middleware.AuthorizeJWT(jwtService))
	{
		userRoleGroup.GET("/", userRoleController.FindUserRoles)
		userRoleGroup.GET("/:id", userRoleController.FindUserRoleById)
		userRoleGroup.GET("/byUserId/:uid", userRoleController.FindUserRoleByUserId)
		userRoleGroup.GET("/exc/:id/:uid", userRoleController.FindExcUserRole)
		userRoleGroup.GET("/excOnly/:id", userRoleController.FindExcUserRoleOnly)
		userRoleGroup.POST("/", userRoleController.InsertUserRole)
		userRoleGroup.PUT("/:id", userRoleController.UpdateUserRole)
		userRoleGroup.DELETE("/:id", userRoleController.DeleteUserRole)
	}

	roleFormGroup := r.Group("/api/RoleForm", middleware.AuthorizeJWT(jwtService))
	{
		roleFormGroup.GET("/", roleFormController.FindRoleForms)
		roleFormGroup.GET("/:id", roleFormController.FindRoleFormById)
		roleFormGroup.GET("/byFormId/:fid/:rid", roleFormController.FindRoleFormByFormId)
		roleFormGroup.GET("/exc/:id/:rid", roleFormController.FindExcRoleForm)
		roleFormGroup.GET("/excOnly/:id", roleFormController.FindExcRoleFormOnly)
		roleFormGroup.POST("/", roleFormController.InsertRoleForm)
		roleFormGroup.PUT("/:id", roleFormController.UpdateRoleForm)
		roleFormGroup.DELETE("/:id", roleFormController.DeleteRoleForm)
	}

	processorGroup := r.Group("/api/processor", middleware.AuthorizeJWT(jwtService))
	{
		processorGroup.GET("/", processorController.FindProcessors)
		processorGroup.GET("/:id", processorController.FindProcessorById)
		processorGroup.GET("/exc/:id", processorController.FindExcProcessor)
		processorGroup.POST("/", processorController.InsertProcessor)
		processorGroup.PUT("/:id", processorController.UpdateProcessor)
		processorGroup.DELETE("/:id", processorController.DeleteProcessor)
	}

	motherboardGroup := r.Group("/api/motherboard", middleware.AuthorizeJWT(jwtService))
	{
		motherboardGroup.GET("/", motherboardController.FindMotherboards)
		motherboardGroup.GET("/:id", motherboardController.FindMotherboardById)
		motherboardGroup.GET("/exc/:id", motherboardController.FindExcMotherboard)
		motherboardGroup.POST("/", motherboardController.InsertMotherboard)
		motherboardGroup.PUT("/:id", motherboardController.UpdateMotherboard)
		motherboardGroup.DELETE("/:id", motherboardController.DeleteMotherboard)
	}

	ramGroup := r.Group("/api/ram", middleware.AuthorizeJWT(jwtService))
	{
		ramGroup.GET("/", ramController.FindRAMs)
		ramGroup.GET("/:id", ramController.FindRAMById)
		ramGroup.GET("/exc/:id", ramController.FindExcRAM)
		ramGroup.POST("/", ramController.InsertRAM)
		ramGroup.PUT("/:id", ramController.UpdateRAM)
		ramGroup.DELETE("/:id", ramController.DeleteRAM)
	}

	hddGroup := r.Group("/api/hdd", middleware.AuthorizeJWT(jwtService))
	{
		hddGroup.GET("/", hddController.FindHDDs)
		hddGroup.GET("/:id", hddController.FindHDDById)
		hddGroup.GET("/exc/:id", hddController.FindExcHDD)
		hddGroup.POST("/", hddController.InsertHDD)
		hddGroup.PUT("/:id", hddController.UpdateHDD)
		hddGroup.DELETE("/:id", hddController.DeleteHDD)
	}

	casingGroup := r.Group("/api/casing", middleware.AuthorizeJWT(jwtService))
	{
		casingGroup.GET("/", casingController.FindCasings)
		casingGroup.GET("/:id", casingController.FindCasingById)
		casingGroup.GET("/exc/:id", casingController.FindExcCasing)
		casingGroup.POST("/", casingController.InsertCasing)
		casingGroup.PUT("/:id", casingController.UpdateCasing)
		casingGroup.DELETE("/:id", casingController.DeleteCasing)
	}

	keyboardGroup := r.Group("/api/keyboard", middleware.AuthorizeJWT(jwtService))
	{
		keyboardGroup.GET("/", keyboardController.FindKeyboards)
		keyboardGroup.GET("/:id", keyboardController.FindKeyboardById)
		keyboardGroup.GET("/exc/:id", keyboardController.FindExcKeyboard)
		keyboardGroup.POST("/", keyboardController.InsertKeyboard)
		keyboardGroup.PUT("/:id", keyboardController.UpdateKeyboard)
		keyboardGroup.DELETE("/:id", keyboardController.DeleteKeyboard)
	}

	mouseGroup := r.Group("/api/mouse", middleware.AuthorizeJWT(jwtService))
	{
		mouseGroup.GET("/", mouseController.FindMouses)
		mouseGroup.GET("/:id", mouseController.FindMouseById)
		mouseGroup.GET("/exc/:id", mouseController.FindExcMouse)
		mouseGroup.POST("/", mouseController.InsertMouse)
		mouseGroup.PUT("/:id", mouseController.UpdateMouse)
		mouseGroup.DELETE("/:id", mouseController.DeleteMouse)
	}

	monitorGroup := r.Group("/api/monitor", middleware.AuthorizeJWT(jwtService))
	{
		monitorGroup.GET("/", monitorController.FindMonitors)
		monitorGroup.GET("/:id", monitorController.FindMonitorById)
		monitorGroup.GET("/exc/:id", monitorController.FindExcMonitor)
		monitorGroup.POST("/", monitorController.InsertMonitor)
		monitorGroup.PUT("/:id", monitorController.UpdateMonitor)
		monitorGroup.DELETE("/:id", monitorController.DeleteMonitor)
	}

	osGroup := r.Group("/api/os", middleware.AuthorizeJWT(jwtService))
	{
		osGroup.GET("/", osController.FindOSs)
		osGroup.GET("/:id", osController.FindOSById)
		osGroup.GET("/exc/:id", osController.FindExcOS)
		osGroup.POST("/", osController.InsertOS)
		osGroup.PUT("/:id", osController.UpdateOS)
		osGroup.DELETE("/:id", osController.DeleteOS)
	}

	officeGroup := r.Group("/api/office", middleware.AuthorizeJWT(jwtService))
	{
		officeGroup.GET("/", officeController.FindOffices)
		officeGroup.GET("/:id", officeController.FindOfficeById)
		officeGroup.GET("/exc/:id", officeController.FindExcOffice)
		officeGroup.POST("/", officeController.InsertOffice)
		officeGroup.PUT("/:id", officeController.UpdateOffice)
		officeGroup.DELETE("/:id", officeController.DeleteOffice)
	}

	computerGroup := r.Group("/api/Computer", middleware.AuthorizeJWT(jwtService))
	{
		computerGroup.GET("/", computerController.FindComputers)
		computerGroup.GET("/:id", computerController.FindComputerById)
		computerGroup.POST("/", computerController.InsertComputer)
		computerGroup.PUT("/:id", computerController.UpdateComputer)
		computerGroup.DELETE("/:id", computerController.DeleteComputer)
	}

	laptopGroup := r.Group("/api/Laptop", middleware.AuthorizeJWT(jwtService))
	{
		laptopGroup.GET("/", laptopController.FindLaptops)
		laptopGroup.GET("/:id", laptopController.FindLaptopById)
		laptopGroup.GET("/exc/:id", laptopController.FindExcLaptop)
		laptopGroup.POST("/", laptopController.InsertLaptop)
		laptopGroup.PUT("/:id", laptopController.UpdateLaptop)
		laptopGroup.DELETE("/:id", laptopController.DeleteLaptop)
	}

	serviceLaptopGroup := r.Group("/api/serviceLaptop", middleware.AuthorizeJWT(jwtService))
	{
		serviceLaptopGroup.GET("/", serviceLaptopController.FindServiceLaptops)
		serviceLaptopGroup.GET("/:id", serviceLaptopController.FindServiceLaptopById)
		serviceLaptopGroup.GET("/exc/:id", serviceLaptopController.FindExcServiceLaptop)
		serviceLaptopGroup.POST("/", serviceLaptopController.InsertServiceLaptop)
		serviceLaptopGroup.PUT("/:id", serviceLaptopController.UpdateServiceLaptop)
		serviceLaptopGroup.DELETE("/:id", serviceLaptopController.DeleteServiceLaptop)
	}

	inkGroup := r.Group("/api/ink", middleware.AuthorizeJWT(jwtService))
	{
		inkGroup.GET("/", inkController.FindInks)
		inkGroup.GET("/:id", inkController.FindInkById)
		inkGroup.GET("/exc/:id", inkController.FindExcInk)
		inkGroup.GET("/black/", inkController.FindBlackInks)
		inkGroup.GET("/black/exc/:id", inkController.FindExcBlackInks)
		inkGroup.GET("/cyan/", inkController.FindCyanInks)
		inkGroup.GET("/cyan/exc/:id", inkController.FindExcCyanInks)
		inkGroup.GET("/magenta/", inkController.FindMagentaInks)
		inkGroup.GET("/magenta/exc/:id", inkController.FindExcMagentaInks)
		inkGroup.GET("/yellow/", inkController.FindYellowInks)
		inkGroup.GET("/yellow/exc/:id", inkController.FindExcYellowInks)
		inkGroup.GET("/color/", inkController.FindColorInks)
		inkGroup.GET("/color/exc/:id", inkController.FindExcColorInks)
		inkGroup.GET("/tricolor/", inkController.FindTricolorInks)
		inkGroup.GET("/tricolor/exc/:id", inkController.FindExcTricolorInks)
		inkGroup.GET("/others/", inkController.FindOthersInks)
		inkGroup.GET("/others/exc/:id", inkController.FindExcOthersInks)
		inkGroup.POST("/", inkController.InsertInk)
		inkGroup.PUT("/:id", inkController.UpdateInk)
		inkGroup.DELETE("/:id", inkController.DeleteInk)
	}

	printerGroup := r.Group("/api/printer", middleware.AuthorizeJWT(jwtService))
	{
		printerGroup.GET("/", printerController.FindPrinters)
		printerGroup.GET("/:id", printerController.FindPrinterById)
		printerGroup.GET("/exc/:id", printerController.FindExcPrinter)
		printerGroup.POST("/", printerController.InsertPrinter)
		printerGroup.PUT("/:id", printerController.UpdatePrinter)
		printerGroup.DELETE("/:id", printerController.DeletePrinter)
	}

	userPrinterGroup := r.Group("/api/userPrinter", middleware.AuthorizeJWT(jwtService))
	{
		userPrinterGroup.GET("/", userPrinterController.FindUserPrinters)
		userPrinterGroup.GET("/:id", userPrinterController.FindUserPrinterById)
		userPrinterGroup.GET("/exc/:id", userPrinterController.FindExcUserPrinter)
		userPrinterGroup.POST("/", userPrinterController.InsertUserPrinter)
		userPrinterGroup.PUT("/:id", userPrinterController.UpdateUserPrinter)
		userPrinterGroup.DELETE("/:id", userPrinterController.DeleteUserPrinter)
	}

	servicePrinterGroup := r.Group("/api/servicePrinter", middleware.AuthorizeJWT(jwtService))
	{
		servicePrinterGroup.GET("/", servicePrinterController.FindServicePrinters)
		servicePrinterGroup.GET("/:id", servicePrinterController.FindServicePrinterById)
		servicePrinterGroup.GET("/exc/:id", servicePrinterController.FindExcServicePrinter)
		servicePrinterGroup.POST("/", servicePrinterController.InsertServicePrinter)
		servicePrinterGroup.PUT("/:id", servicePrinterController.UpdateServicePrinter)
		servicePrinterGroup.DELETE("/:id", servicePrinterController.DeleteServicePrinter)
	}

	upsGroup := r.Group("/api/ups", middleware.AuthorizeJWT(jwtService))
	{
		upsGroup.GET("/", upsController.FindUPSs)
		upsGroup.GET("/:id", upsController.FindUPSById)
		upsGroup.GET("/exc/:id", upsController.FindExcUPS)
		upsGroup.POST("/", upsController.InsertUPS)
		upsGroup.PUT("/:id", upsController.UpdateUPS)
		upsGroup.DELETE("/:id", upsController.DeleteUPS)
	}

	serviceUPSGroup := r.Group("/api/serviceUPS", middleware.AuthorizeJWT(jwtService))
	{
		serviceUPSGroup.GET("/", serviceUPSController.FindServiceUPSs)
		serviceUPSGroup.GET("/:id", serviceUPSController.FindServiceUPSById)
		serviceUPSGroup.GET("/exc/:id", serviceUPSController.FindExcServiceUPS)
		serviceUPSGroup.POST("/", serviceUPSController.InsertServiceUPS)
		serviceUPSGroup.PUT("/:id", serviceUPSController.UpdateServiceUPS)
		serviceUPSGroup.DELETE("/:id", serviceUPSController.DeleteServiceUPS)
	}

	antivirusGroup := r.Group("/api/antivirus", middleware.AuthorizeJWT(jwtService))
	{
		antivirusGroup.GET("/", antivirusController.FindAntiviruss)
		antivirusGroup.GET("/:id", antivirusController.FindAntivirusById)
		antivirusGroup.GET("/exc/:id", antivirusController.FindExcAntivirus)
		antivirusGroup.POST("/", antivirusController.InsertAntivirus)
		antivirusGroup.PUT("/:id", antivirusController.UpdateAntivirus)
		antivirusGroup.DELETE("/:id", antivirusController.DeleteAntivirus)
	}

	harddiskGroup := r.Group("/api/harddisk", middleware.AuthorizeJWT(jwtService))
	{
		harddiskGroup.GET("/", harddiskController.FindHarddisks)
		harddiskGroup.GET("/:id", harddiskController.FindHarddiskById)
		harddiskGroup.GET("/exc/:id", harddiskController.FindExcHarddisk)
		harddiskGroup.POST("/", harddiskController.InsertHarddisk)
		harddiskGroup.PUT("/:id", harddiskController.UpdateHarddisk)
		harddiskGroup.DELETE("/:id", harddiskController.DeleteHarddisk)
	}

	subscriptionGroup := r.Group("/api/subscription", middleware.AuthorizeJWT(jwtService))
	{
		subscriptionGroup.GET("/", subscriptionController.FindSubscriptions)
		subscriptionGroup.GET("/:id", subscriptionController.FindSubscriptionById)
		subscriptionGroup.GET("/exc/:id", subscriptionController.FindExcSubscription)
		subscriptionGroup.POST("/", subscriptionController.InsertSubscription)
		subscriptionGroup.PUT("/:id", subscriptionController.UpdateSubscription)
		subscriptionGroup.DELETE("/:id", subscriptionController.DeleteSubscription)
	}

	tokenGroup := r.Group("/api/token", middleware.AuthorizeJWTRefreshToken(jwtService))
	{
		tokenGroup.POST("/renew", authController.RenewToken)
	}

	// Handle error response when a route is not defined
	r.NoRoute(func(c *gin.Context) {
		// In gin this is how you return a JSON response
		c.JSON(404, gin.H{"message": "Not found"})
	})

	r.Run()
}
