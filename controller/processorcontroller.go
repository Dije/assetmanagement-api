package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ProcessorController interface {
	FindProcessors(c *gin.Context)
	FindProcessorById(c *gin.Context)
	FindExcProcessor(c *gin.Context)
	InsertProcessor(c *gin.Context)
	UpdateProcessor(c *gin.Context)
	DeleteProcessor(c *gin.Context)
}

type processorController struct {
	processorService service.ProcessorService
	jwtService       service.JWTService
}

func NewProcessorController(processorServ service.ProcessorService, jwtServ service.JWTService) ProcessorController {
	return &processorController{
		processorService: processorServ,
		jwtService:       jwtServ,
	}
}

func (b *processorController) FindProcessors(c *gin.Context) {
	var (
		processors []model.Processor
		response   helper.Response
	)
	processors, err := b.processorService.FindProcessors()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", processors)
		c.JSON(http.StatusOK, response)
	}
}

func (b *processorController) FindProcessorById(c *gin.Context) {
	var (
		processor model.Processor
		response  helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		processor, err = b.processorService.FindProcessorById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", processor)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *processorController) FindExcProcessor(c *gin.Context) {
	var (
		processors []model.Processor
		response   helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		processors, err = b.processorService.FindExcProcessor(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", processors)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *processorController) InsertProcessor(c *gin.Context) {
	var (
		processor                model.Processor
		response                 helper.Response
		CreateProcessorParameter model.CreateProcessorParameter
	)
	err := c.ShouldBindJSON(&CreateProcessorParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		processor, err = b.processorService.InsertProcessor(CreateProcessorParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register processor", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", processor)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *processorController) UpdateProcessor(c *gin.Context) {
	var (
		newData  model.Processor
		oldData  model.Processor
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.processorService.FindProcessorById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Processor{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				processor, err := b.processorService.UpdateProcessor(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update processor", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", processor)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *processorController) DeleteProcessor(c *gin.Context) {
	var (
		newData  model.Processor
		oldData  model.Processor
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.processorService.FindProcessorById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Processor{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				processor, err := b.processorService.DeleteProcessor(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete processor", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", processor)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
