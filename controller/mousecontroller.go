package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type MouseController interface {
	FindMouses(c *gin.Context)
	FindMouseById(c *gin.Context)
	FindExcMouse(c *gin.Context)
	InsertMouse(c *gin.Context)
	UpdateMouse(c *gin.Context)
	DeleteMouse(c *gin.Context)
}

type mouseController struct {
	mouseService service.MouseService
	jwtService   service.JWTService
}

func NewMouseController(mouseServ service.MouseService, jwtServ service.JWTService) MouseController {
	return &mouseController{
		mouseService: mouseServ,
		jwtService:   jwtServ,
	}
}

func (b *mouseController) FindMouses(c *gin.Context) {
	var (
		mouses   []model.Mouse
		response helper.Response
	)
	mouses, err := b.mouseService.FindMouses()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", mouses)
		c.JSON(http.StatusOK, response)
	}
}

func (b *mouseController) FindMouseById(c *gin.Context) {
	var (
		mouse    model.Mouse
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		mouse, err = b.mouseService.FindMouseById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", mouse)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *mouseController) FindExcMouse(c *gin.Context) {
	var (
		mouses   []model.Mouse
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		mouses, err = b.mouseService.FindExcMouse(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", mouses)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *mouseController) InsertMouse(c *gin.Context) {
	var (
		mouse                model.Mouse
		response             helper.Response
		CreateMouseParameter model.CreateMouseParameter
	)
	err := c.ShouldBindJSON(&CreateMouseParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		mouse, err = b.mouseService.InsertMouse(CreateMouseParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register mouse", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", mouse)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *mouseController) UpdateMouse(c *gin.Context) {
	var (
		newData  model.Mouse
		oldData  model.Mouse
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.mouseService.FindMouseById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Mouse{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				mouse, err := b.mouseService.UpdateMouse(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update mouse", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", mouse)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *mouseController) DeleteMouse(c *gin.Context) {
	var (
		newData  model.Mouse
		oldData  model.Mouse
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.mouseService.FindMouseById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Mouse{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				mouse, err := b.mouseService.DeleteMouse(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete mouse", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", mouse)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
