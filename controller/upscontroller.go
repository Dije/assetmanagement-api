package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/go-cmp/cmp"
)

type UPSController interface {
	FindUPSs(c *gin.Context)
	FindUPSById(c *gin.Context)
	FindExcUPS(c *gin.Context)
	InsertUPS(c *gin.Context)
	UpdateUPS(c *gin.Context)
	DeleteUPS(c *gin.Context)
}

type upsController struct {
	upsService service.UPSService
	jwtService service.JWTService
}

func NewUPSController(upsServ service.UPSService, jwtServ service.JWTService) UPSController {
	return &upsController{
		upsService: upsServ,
		jwtService: jwtServ,
	}
}

func (b *upsController) FindUPSs(c *gin.Context) {
	var (
		upss     []model.SelectUPSParameter
		response helper.Response
	)
	upss, err := b.upsService.FindUPSs()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", upss)
		c.JSON(http.StatusOK, response)
	}
}

func (b *upsController) FindUPSById(c *gin.Context) {
	var (
		ups      model.SelectUPSParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		ups, err = b.upsService.FindUPSById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", ups)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *upsController) FindExcUPS(c *gin.Context) {
	var (
		upss     []model.SelectUPSParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		upss, err = b.upsService.FindExcUPS(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", upss)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *upsController) InsertUPS(c *gin.Context) {
	var (
		ups                model.UPS
		response           helper.Response
		CreateUPSParameter model.CreateUPSParameter
	)
	err := c.ShouldBindJSON(&CreateUPSParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		ups, err = b.upsService.InsertUPS(CreateUPSParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register ups", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", ups)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *upsController) UpdateUPS(c *gin.Context) {
	var (
		newData  model.UPS
		oldData  model.SelectUPSParameter
		response helper.Response
	)

	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.upsService.FindUPSById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (cmp.Equal(oldData, model.SelectEmployeeParameter{})) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				ups, err := b.upsService.UpdateUPS(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update ups", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", ups)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
func (b *upsController) DeleteUPS(c *gin.Context) {
	var (
		newData  model.UPS
		oldData  model.SelectUPSParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.upsService.FindUPSById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (cmp.Equal(oldData, model.SelectEmployeeParameter{})) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				ups, err := b.upsService.DeleteUPS(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete ups", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", ups)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
