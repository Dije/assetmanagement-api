package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type RAMController interface {
	FindRAMs(c *gin.Context)
	FindRAMById(c *gin.Context)
	FindExcRAM(c *gin.Context)
	InsertRAM(c *gin.Context)
	UpdateRAM(c *gin.Context)
	DeleteRAM(c *gin.Context)
}

type ramController struct {
	ramService service.RAMService
	jwtService service.JWTService
}

func NewRAMController(ramServ service.RAMService, jwtServ service.JWTService) RAMController {
	return &ramController{
		ramService: ramServ,
		jwtService: jwtServ,
	}
}

func (b *ramController) FindRAMs(c *gin.Context) {
	var (
		rams     []model.RAM
		response helper.Response
	)
	rams, err := b.ramService.FindRAMs()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", rams)
		c.JSON(http.StatusOK, response)
	}
}

func (b *ramController) FindRAMById(c *gin.Context) {
	var (
		ram      model.RAM
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		ram, err = b.ramService.FindRAMById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", ram)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *ramController) FindExcRAM(c *gin.Context) {
	var (
		rams     []model.RAM
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		rams, err = b.ramService.FindExcRAM(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", rams)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *ramController) InsertRAM(c *gin.Context) {
	var (
		ram                model.RAM
		response           helper.Response
		CreateRAMParameter model.CreateRAMParameter
	)
	err := c.ShouldBindJSON(&CreateRAMParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		ram, err = b.ramService.InsertRAM(CreateRAMParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register ram", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", ram)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *ramController) UpdateRAM(c *gin.Context) {
	var (
		newData  model.RAM
		oldData  model.RAM
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.ramService.FindRAMById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.RAM{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				ram, err := b.ramService.UpdateRAM(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update ram", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", ram)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *ramController) DeleteRAM(c *gin.Context) {
	var (
		newData  model.RAM
		oldData  model.RAM
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.ramService.FindRAMById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.RAM{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				ram, err := b.ramService.DeleteRAM(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete ram", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", ram)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
