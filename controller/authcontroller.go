package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type AuthController interface {
	Login(c *gin.Context)
	Register(c *gin.Context)
	RenewToken(c *gin.Context)
	CheckExisting(c *gin.Context)
	SendMail(c *gin.Context)
	UpdateDataPassword(c *gin.Context)
	UpdateDataRequest(c *gin.Context)
}

type authController struct {
	authService service.AuthService
	jwtService  service.JWTService
}

func NewAuthController(authService service.AuthService, jwtService service.JWTService) AuthController {
	return &authController{
		authService: authService,
		jwtService:  jwtService,
	}
}

func (a *authController) Login(c *gin.Context) {
	var loginParameter model.LoginParameter
	var loginResponse model.LoginResponse
	err := c.ShouldBind(&loginParameter)
	if err != nil {
		if loginParameter.Username == "" && loginParameter.Password == "" {
			response := helper.BuildErrorResponse("Failed to process request", "Username and Password are missing", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}
		if loginParameter.Username == "" {
			response := helper.BuildErrorResponse("Failed to process request", "Username is missing", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}
		if loginParameter.Password == "" {
			response := helper.BuildErrorResponse("Failed to process request", "Password is missing", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}
	}

	authResult := a.authService.VerifyCredential(loginParameter.Username, loginParameter.Password)
	if user, ok := authResult.(model.User); ok {
		if user.ID != 0 {
			now := time.Now()
			iat := now.Unix()
			exp := now.Add(time.Hour * 24).Unix()
			expRefreshToken := now.AddDate(0, 0, 7).Unix()

			generatedToken := a.jwtService.GenerateToken(uint64(user.ID), user.Username, iat, exp)
			generatedRefreshToken := a.jwtService.GenerateRefreshToken(uint64(user.ID), user.Username, iat, expRefreshToken)

			loginResponse.User = user
			loginResponse.AccessToken = generatedToken
			loginResponse.RefreshToken = generatedRefreshToken

			response := helper.BuildResponse(true, "OK", loginResponse)
			c.JSON(http.StatusOK, response)
		} else {
			response := helper.BuildResponse(false, "User not found", helper.EmptyObj{})
			c.JSON(http.StatusOK, response)
		}

		return
	}
	response := helper.BuildErrorResponse("Please check again your credential", "Invalid Credential", helper.EmptyObj{})
	c.AbortWithStatusJSON(http.StatusUnauthorized, response)
}

func (a *authController) Register(c *gin.Context) {
	var createUserParameter model.CreateUserParameter
	var loginResponse model.LoginResponse
	var response helper.Response

	errDTO := c.ShouldBind(&createUserParameter)
	if errDTO != nil {
		response = helper.BuildErrorResponse("Failed to process request", errDTO.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	if !a.authService.IsDuplicateUsername(createUserParameter.Username) {
		response = helper.BuildErrorResponse("Failed to process request", "Duplicate username", helper.EmptyObj{})
		c.JSON(http.StatusConflict, response)
	} else {
		createdUser, err := a.authService.CreateUser(createUserParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register user", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			now := time.Now()
			iat := now.Unix()
			exp := now.Add(time.Hour * 24).Unix()
			expRefreshToken := now.AddDate(0, 0, 7).Unix()

			accessToken := a.jwtService.GenerateToken(uint64(createdUser.ID), createdUser.Username, iat, exp)
			refreshToken := a.jwtService.GenerateRefreshToken(uint64(createdUser.ID), createdUser.Username, iat, expRefreshToken)

			loginResponse.User = createdUser
			loginResponse.AccessToken = accessToken
			loginResponse.RefreshToken = refreshToken

			response = helper.BuildResponse(true, "OK", loginResponse)
			c.JSON(http.StatusCreated, response)
		}
	}
}

func (a *authController) RenewToken(c *gin.Context) {
	var renewToken model.RenewToken
	err := c.ShouldBind(&renewToken)

	var renewTokenResult model.RenewTokenResult

	if err != nil {
		response := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	if err != nil {
		response := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	if a.authService.IsUserRegistered(renewToken.ID, renewToken.Username) {
		response := helper.BuildErrorResponse("Failed to process request", "User not registered", helper.EmptyObj{})
		c.JSON(http.StatusConflict, response)
		return
	} else {

		now := time.Now()
		iat := now.Unix()
		exp := now.Add(time.Hour * 24).Unix()
		expRefreshToken := now.AddDate(0, 0, 7).Unix()

		generatedToken := a.jwtService.GenerateToken(renewToken.ID, renewToken.Username, iat, exp)
		generatedRefreshToken := a.jwtService.GenerateRefreshToken(renewToken.ID, renewToken.Username, iat, expRefreshToken)

		renewTokenResult.AccessToken = generatedToken
		renewTokenResult.RefreshToken = generatedRefreshToken

		response := helper.BuildResponse(true, "OK", renewTokenResult)
		c.JSON(http.StatusOK, response)
		return
	}
}

func (b *authController) CheckExisting(c *gin.Context) {
	var (
		Data     model.User
		response helper.Response
	)
	err := c.ShouldBindJSON(&Data)
	if err != nil {
		if Data.Username == "" && Data.Email == "" {
			response := helper.BuildErrorResponse("Failed to process request", "Username and Email are missing", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}
		if Data.Username == "" {
			response := helper.BuildErrorResponse("Failed to process request", "Username is missing", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}
		if Data.Email == "" {
			response := helper.BuildErrorResponse("Failed to process request", "Email is missing", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}
	}
	user, err := b.authService.CheckExisting(Data)
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", "Please check your username or email", helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", user)
		c.JSON(http.StatusOK, response)
	}
}

func (b *authController) SendMail(c *gin.Context) {
	var (
		Data     model.Mail
		response helper.Response
	)
	err := c.ShouldBindJSON(&Data)
	if err != nil {
		if Data.To == "" && Data.Subject == "" && Data.Body == "" {
			response := helper.BuildErrorResponse("Failed to process request", "Recipient, Subject, and Body are missing", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}
		if Data.To == "" {
			response := helper.BuildErrorResponse("Failed to process request", "Recipient is missing", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}
		if Data.Subject == "" {
			response := helper.BuildErrorResponse("Failed to process request", "Subject is missing", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}
		if Data.Body == "" {
			response := helper.BuildErrorResponse("Failed to process request", "Body is missing", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}
	}
	mail, err := b.authService.SendMail(Data)
	if err != nil {
		response = helper.BuildErrorResponse("Email Sent Failed", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", mail)
		c.JSON(http.StatusOK, response)
	}
}

func (b *authController) UpdateDataPassword(c *gin.Context) {
	var (
		Data     model.ResetPasswordParameter
		response helper.Response
	)

	username := c.Param("username")
	if username == "" {
		response = helper.BuildErrorResponse("No param username was found", "No data with given username", helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		email := c.Param("email")
		if email == "" {
			response = helper.BuildErrorResponse("No param email was found", "No data with given email", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			err := c.ShouldBindJSON(&Data)
			if err != nil {
				if Data.Password == "" && Data.ConfirmPassword == "" {
					response := helper.BuildErrorResponse("Failed to process request", "Password and Confirm Password are missing", helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
					return
				}
				if Data.Password == "" {
					response := helper.BuildErrorResponse("Failed to process request", "Password is missing", helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
					return
				}
				if Data.ConfirmPassword == "" {
					response := helper.BuildErrorResponse("Failed to process request", "Confirm Password is missing", helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
					return
				}
			}
			if Data.Password != Data.ConfirmPassword {
				response := helper.BuildErrorResponse("Failed to process request", "Password and Confirm Password are not match", helper.EmptyObj{})
				c.AbortWithStatusJSON(http.StatusBadRequest, response)
				return
			} else {
				user, err := b.authService.UpdateDataPassword(Data, username, email)
				if err != nil {
					response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
					c.JSON(http.StatusNotFound, response)
				} else {
					response = helper.BuildResponse(true, "OK", user)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
func (b *authController) UpdateDataRequest(c *gin.Context) {
	var (
		Data     model.RequestPasswordChangeParameter
		response helper.Response
	)

	username := c.Param("username")
	if username == "" {
		response = helper.BuildErrorResponse("No param username was found", "No data with given username", helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		email := c.Param("email")
		if email == "" {
			response = helper.BuildErrorResponse("No param email was found", "No data with given email", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			err := c.ShouldBindJSON(&Data)
			user, err := b.authService.UpdateDataRequest(Data, username, email)
			if err != nil {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				response = helper.BuildResponse(true, "OK", user)
				c.JSON(http.StatusOK, response)
			}
		}
	}
}
