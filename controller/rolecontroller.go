package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type RoleController interface {
	FindRoles(c *gin.Context)
	FindRoleById(c *gin.Context)
	FindExcRole(c *gin.Context)
	InsertRole(c *gin.Context)
	UpdateRole(c *gin.Context)
	DeleteRole(c *gin.Context)
}

type roleController struct {
	roleService service.RoleService
	jwtService  service.JWTService
}

func NewRoleController(roleServ service.RoleService, jwtServ service.JWTService) RoleController {
	return &roleController{
		roleService: roleServ,
		jwtService:  jwtServ,
	}
}

func (b *roleController) FindRoles(c *gin.Context) {
	var (
		roles    []model.Role
		response helper.Response
	)
	roles, err := b.roleService.FindRoles()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", roles)
		c.JSON(http.StatusOK, response)
	}
}

func (b *roleController) FindRoleById(c *gin.Context) {
	var (
		role     model.Role
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		role, err = b.roleService.FindRoleById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", role)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *roleController) FindExcRole(c *gin.Context) {
	var (
		roles    []model.Role
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		roles, err = b.roleService.FindExcRole(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", roles)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *roleController) InsertRole(c *gin.Context) {
	var (
		role                model.Role
		response            helper.Response
		CreateRoleParameter model.CreateRoleParameter
	)
	err := c.ShouldBindJSON(&CreateRoleParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		role, err = b.roleService.InsertRole(CreateRoleParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register role", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", role)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *roleController) UpdateRole(c *gin.Context) {
	var (
		newData  model.Role
		oldData  model.Role
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.roleService.FindRoleById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Role{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				role, err := b.roleService.UpdateRole(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update role", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", role)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *roleController) DeleteRole(c *gin.Context) {
	var (
		newData  model.Role
		oldData  model.Role
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.roleService.FindRoleById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Role{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				role, err := b.roleService.DeleteRole(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete role", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", role)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
