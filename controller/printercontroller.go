package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type PrinterController interface {
	FindPrinters(c *gin.Context)
	FindPrinterById(c *gin.Context)
	FindExcPrinter(c *gin.Context)
	InsertPrinter(c *gin.Context)
	UpdatePrinter(c *gin.Context)
	DeletePrinter(c *gin.Context)
}

type printerController struct {
	printerService service.PrinterService
	jwtService     service.JWTService
}

func NewPrinterController(printerServ service.PrinterService, jwtServ service.JWTService) PrinterController {
	return &printerController{
		printerService: printerServ,
		jwtService:     jwtServ,
	}
}

func (b *printerController) FindPrinters(c *gin.Context) {
	var (
		printers []model.SelectPrinterParameter
		response helper.Response
	)
	printers, err := b.printerService.FindPrinters()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", printers)
		c.JSON(http.StatusOK, response)
	}
}

func (b *printerController) FindPrinterById(c *gin.Context) {
	var (
		printer  model.SelectPrinterParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		printer, err = b.printerService.FindPrinterById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", printer)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *printerController) FindExcPrinter(c *gin.Context) {
	var (
		printers []model.SelectPrinterParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		printers, err = b.printerService.FindExcPrinter(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", printers)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *printerController) InsertPrinter(c *gin.Context) {
	var (
		printer                model.Printer
		response               helper.Response
		CreatePrinterParameter model.CreatePrinterParameter
	)
	err := c.ShouldBindJSON(&CreatePrinterParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		printer, err = b.printerService.InsertPrinter(CreatePrinterParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register printer", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", printer)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *printerController) UpdatePrinter(c *gin.Context) {
	var (
		newData  model.Printer
		oldData  model.SelectPrinterParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.printerService.FindPrinterById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.SelectPrinterParameter{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				printer, err := b.printerService.UpdatePrinter(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update printer", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", printer)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *printerController) DeletePrinter(c *gin.Context) {
	var (
		newData  model.Printer
		oldData  model.SelectPrinterParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.printerService.FindPrinterById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.SelectPrinterParameter{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				printer, err := b.printerService.DeletePrinter(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete printer", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", printer)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
