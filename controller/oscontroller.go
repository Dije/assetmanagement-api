package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type OSController interface {
	FindOSs(c *gin.Context)
	FindOSById(c *gin.Context)
	FindExcOS(c *gin.Context)
	InsertOS(c *gin.Context)
	UpdateOS(c *gin.Context)
	DeleteOS(c *gin.Context)
}

type osController struct {
	osService  service.OSService
	jwtService service.JWTService
}

func NewOSController(osServ service.OSService, jwtServ service.JWTService) OSController {
	return &osController{
		osService:  osServ,
		jwtService: jwtServ,
	}
}

func (b *osController) FindOSs(c *gin.Context) {
	var (
		oss      []model.OS
		response helper.Response
	)
	oss, err := b.osService.FindOSs()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", oss)
		c.JSON(http.StatusOK, response)
	}
}

func (b *osController) FindOSById(c *gin.Context) {
	var (
		os       model.OS
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		os, err = b.osService.FindOSById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", os)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *osController) FindExcOS(c *gin.Context) {
	var (
		oss      []model.OS
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		oss, err = b.osService.FindExcOS(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", oss)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *osController) InsertOS(c *gin.Context) {
	var (
		os                model.OS
		response          helper.Response
		CreateOSParameter model.CreateOSParameter
	)
	err := c.ShouldBindJSON(&CreateOSParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		os, err = b.osService.InsertOS(CreateOSParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register os", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", os)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *osController) UpdateOS(c *gin.Context) {
	var (
		newData  model.OS
		oldData  model.OS
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.osService.FindOSById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.OS{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				os, err := b.osService.UpdateOS(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update os", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", os)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *osController) DeleteOS(c *gin.Context) {
	var (
		newData  model.OS
		oldData  model.OS
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.osService.FindOSById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.OS{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				os, err := b.osService.DeleteOS(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete os", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", os)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
