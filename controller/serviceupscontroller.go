package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ServiceUPSController interface {
	FindServiceUPSs(c *gin.Context)
	FindServiceUPSById(c *gin.Context)
	FindExcServiceUPS(c *gin.Context)
	InsertServiceUPS(c *gin.Context)
	UpdateServiceUPS(c *gin.Context)
	DeleteServiceUPS(c *gin.Context)
}

type serviceUPSController struct {
	serviceUPSService service.ServiceUPSService
	jwtService        service.JWTService
}

func NewServiceUPSController(serviceUPSServ service.ServiceUPSService, jwtServ service.JWTService) ServiceUPSController {
	return &serviceUPSController{
		serviceUPSService: serviceUPSServ,
		jwtService:        jwtServ,
	}
}

func (b *serviceUPSController) FindServiceUPSs(c *gin.Context) {
	var (
		serviceUPSs []model.SelectServiceUPSParameter
		response    helper.Response
	)
	serviceUPSs, err := b.serviceUPSService.FindServiceUPSs()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", serviceUPSs)
		c.JSON(http.StatusOK, response)
	}
}

func (b *serviceUPSController) FindServiceUPSById(c *gin.Context) {
	var (
		serviceUPS model.SelectServiceUPSParameter
		response   helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		serviceUPS, err = b.serviceUPSService.FindServiceUPSById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", serviceUPS)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *serviceUPSController) FindExcServiceUPS(c *gin.Context) {
	var (
		serviceUPSs []model.SelectServiceUPSParameter
		response    helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		serviceUPSs, err = b.serviceUPSService.FindExcServiceUPS(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", serviceUPSs)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *serviceUPSController) InsertServiceUPS(c *gin.Context) {
	var (
		serviceUPS                model.ServiceUPS
		response                  helper.Response
		CreateServiceUPSParameter model.CreateServiceUPSParameter
	)
	err := c.ShouldBindJSON(&CreateServiceUPSParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		serviceUPS, err = b.serviceUPSService.InsertServiceUPS(CreateServiceUPSParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register serviceUPS", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", serviceUPS)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *serviceUPSController) UpdateServiceUPS(c *gin.Context) {
	var (
		newData  model.ServiceUPS
		oldData  model.SelectServiceUPSParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.serviceUPSService.FindServiceUPSById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.SelectServiceUPSParameter{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				serviceUPS, err := b.serviceUPSService.UpdateServiceUPS(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update serviceUPS", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", serviceUPS)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *serviceUPSController) DeleteServiceUPS(c *gin.Context) {
	var (
		newData  model.ServiceUPS
		oldData  model.SelectServiceUPSParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.serviceUPSService.FindServiceUPSById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.SelectServiceUPSParameter{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				serviceUPS, err := b.serviceUPSService.DeleteServiceUPS(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete serviceUPS", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", serviceUPS)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
