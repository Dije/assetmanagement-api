package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ServicePrinterController interface {
	FindServicePrinters(c *gin.Context)
	FindServicePrinterById(c *gin.Context)
	FindExcServicePrinter(c *gin.Context)
	InsertServicePrinter(c *gin.Context)
	UpdateServicePrinter(c *gin.Context)
	DeleteServicePrinter(c *gin.Context)
}

type servicePrinterController struct {
	servicePrinterService service.ServicePrinterService
	jwtService            service.JWTService
}

func NewServicePrinterController(servicePrinterServ service.ServicePrinterService, jwtServ service.JWTService) ServicePrinterController {
	return &servicePrinterController{
		servicePrinterService: servicePrinterServ,
		jwtService:            jwtServ,
	}
}

func (b *servicePrinterController) FindServicePrinters(c *gin.Context) {
	var (
		servicePrinters []model.SelectServicePrinterParameter
		response        helper.Response
	)
	servicePrinters, err := b.servicePrinterService.FindServicePrinters()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", servicePrinters)
		c.JSON(http.StatusOK, response)
	}
}

func (b *servicePrinterController) FindServicePrinterById(c *gin.Context) {
	var (
		servicePrinter model.SelectServicePrinterParameter
		response       helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		servicePrinter, err = b.servicePrinterService.FindServicePrinterById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", servicePrinter)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *servicePrinterController) FindExcServicePrinter(c *gin.Context) {
	var (
		servicePrinters []model.SelectServicePrinterParameter
		response        helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		servicePrinters, err = b.servicePrinterService.FindExcServicePrinter(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", servicePrinters)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *servicePrinterController) InsertServicePrinter(c *gin.Context) {
	var (
		servicePrinter                model.ServicePrinter
		response                      helper.Response
		CreateServicePrinterParameter model.CreateServicePrinterParameter
	)
	err := c.ShouldBindJSON(&CreateServicePrinterParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		servicePrinter, err = b.servicePrinterService.InsertServicePrinter(CreateServicePrinterParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register servicePrinter", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", servicePrinter)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *servicePrinterController) UpdateServicePrinter(c *gin.Context) {
	var (
		newData  model.ServicePrinter
		oldData  model.SelectServicePrinterParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.servicePrinterService.FindServicePrinterById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.SelectServicePrinterParameter{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				servicePrinter, err := b.servicePrinterService.UpdateServicePrinter(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update servicePrinter", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", servicePrinter)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *servicePrinterController) DeleteServicePrinter(c *gin.Context) {
	var (
		newData  model.ServicePrinter
		oldData  model.SelectServicePrinterParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.servicePrinterService.FindServicePrinterById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.SelectServicePrinterParameter{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				servicePrinter, err := b.servicePrinterService.DeleteServicePrinter(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete servicePrinter", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", servicePrinter)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
