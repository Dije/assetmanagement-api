package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type AntivirusController interface {
	FindAntiviruss(c *gin.Context)
	FindAntivirusById(c *gin.Context)
	FindExcAntivirus(c *gin.Context)
	InsertAntivirus(c *gin.Context)
	UpdateAntivirus(c *gin.Context)
	DeleteAntivirus(c *gin.Context)
}

type antivirusController struct {
	antivirusService service.AntivirusService
	jwtService       service.JWTService
}

func NewAntivirusController(antivirusServ service.AntivirusService, jwtServ service.JWTService) AntivirusController {
	return &antivirusController{
		antivirusService: antivirusServ,
		jwtService:       jwtServ,
	}
}

func (b *antivirusController) FindAntiviruss(c *gin.Context) {
	var (
		antiviruss []model.SelectAntivirusParameter
		response   helper.Response
	)
	antiviruss, err := b.antivirusService.FindAntiviruss()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", antiviruss)
		c.JSON(http.StatusOK, response)
	}
}

func (b *antivirusController) FindAntivirusById(c *gin.Context) {
	var (
		antivirus model.SelectAntivirusParameter
		response  helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		antivirus, err = b.antivirusService.FindAntivirusById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", antivirus)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *antivirusController) FindExcAntivirus(c *gin.Context) {
	var (
		antiviruss []model.SelectAntivirusParameter
		response   helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		antiviruss, err = b.antivirusService.FindExcAntivirus(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", antiviruss)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *antivirusController) InsertAntivirus(c *gin.Context) {
	var (
		antivirus                model.Antivirus
		response                 helper.Response
		CreateAntivirusParameter model.CreateAntivirusParameter
	)
	err := c.ShouldBindJSON(&CreateAntivirusParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		antivirus, err = b.antivirusService.InsertAntivirus(CreateAntivirusParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register antivirus", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", antivirus)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *antivirusController) UpdateAntivirus(c *gin.Context) {
	var (
		newData  model.Antivirus
		oldData  model.SelectAntivirusParameter
		response helper.Response
	)

	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.antivirusService.FindAntivirusById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.SelectAntivirusParameter{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				antivirus, err := b.antivirusService.UpdateAntivirus(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update antivirus", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", antivirus)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *antivirusController) DeleteAntivirus(c *gin.Context) {
	var (
		newData  model.Antivirus
		oldData  model.SelectAntivirusParameter
		response helper.Response
	)

	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.antivirusService.FindAntivirusById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.SelectAntivirusParameter{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				antivirus, err := b.antivirusService.DeleteAntivirus(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete antivirus", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", antivirus)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
