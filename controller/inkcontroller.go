package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type InkController interface {
	FindInks(c *gin.Context)
	FindInkById(c *gin.Context)
	FindExcInk(c *gin.Context)
	FindBlackInks(c *gin.Context)
	FindExcBlackInks(c *gin.Context)
	FindCyanInks(c *gin.Context)
	FindExcCyanInks(c *gin.Context)
	FindMagentaInks(c *gin.Context)
	FindExcMagentaInks(c *gin.Context)
	FindYellowInks(c *gin.Context)
	FindExcYellowInks(c *gin.Context)
	FindColorInks(c *gin.Context)
	FindExcColorInks(c *gin.Context)
	FindTricolorInks(c *gin.Context)
	FindExcTricolorInks(c *gin.Context)
	FindOthersInks(c *gin.Context)
	FindExcOthersInks(c *gin.Context)
	InsertInk(c *gin.Context)
	UpdateInk(c *gin.Context)
	DeleteInk(c *gin.Context)
}

type inkController struct {
	inkService service.InkService
	jwtService service.JWTService
}

func NewInkController(inkServ service.InkService, jwtServ service.JWTService) InkController {
	return &inkController{
		inkService: inkServ,
		jwtService: jwtServ,
	}
}

func (b *inkController) FindInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	inks, err := b.inkService.FindInks()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", inks)
		c.JSON(http.StatusOK, response)
	}
}

func (b *inkController) FindInkById(c *gin.Context) {
	var (
		ink      model.Ink
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		ink, err = b.inkService.FindInkById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", ink)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *inkController) FindExcInk(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		inks, err = b.inkService.FindExcInk(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", inks)
			c.JSON(http.StatusOK, response)
		}
	}
}

// FindBlackInks implements InkController
func (b *inkController) FindBlackInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	inks, err := b.inkService.FindBlackInks()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", inks)
		c.JSON(http.StatusOK, response)
	}
}

// FindColorInks implements InkController
func (b *inkController) FindColorInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	inks, err := b.inkService.FindColorInks()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", inks)
		c.JSON(http.StatusOK, response)
	}
}

// FindCyanInks implements InkController
func (b *inkController) FindCyanInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	inks, err := b.inkService.FindCyanInks()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", inks)
		c.JSON(http.StatusOK, response)
	}
}

// FindExcBlackInks implements InkController
func (b *inkController) FindExcBlackInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		inks, err = b.inkService.FindExcBlackInks(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", inks)
			c.JSON(http.StatusOK, response)
		}
	}
}

// FindExcColorInks implements InkController
func (b *inkController) FindExcColorInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		inks, err = b.inkService.FindExcColorInks(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", inks)
			c.JSON(http.StatusOK, response)
		}
	}
}

// FindExcCyanInks implements InkController
func (b *inkController) FindExcCyanInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		inks, err = b.inkService.FindExcCyanInks(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", inks)
			c.JSON(http.StatusOK, response)
		}
	}
}

// FindExcMagentaInks implements InkController
func (b *inkController) FindExcMagentaInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		inks, err = b.inkService.FindExcMagentaInks(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", inks)
			c.JSON(http.StatusOK, response)
		}
	}
}

// FindExcOthersInks implements InkController
func (b *inkController) FindExcOthersInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		inks, err = b.inkService.FindExcOthersInks(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", inks)
			c.JSON(http.StatusOK, response)
		}
	}
}

// FindExcTricolorInks implements InkController
func (b *inkController) FindExcTricolorInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		inks, err = b.inkService.FindExcTricolorInks(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", inks)
			c.JSON(http.StatusOK, response)
		}
	}
}

// FindExcYellowInks implements InkController
func (b *inkController) FindExcYellowInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		inks, err = b.inkService.FindExcYellowInks(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", inks)
			c.JSON(http.StatusOK, response)
		}
	}
}

// FindMagentaInks implements InkController
func (b *inkController) FindMagentaInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	inks, err := b.inkService.FindMagentaInks()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", inks)
		c.JSON(http.StatusOK, response)
	}
}

// FindOthersInks implements InkController
func (b *inkController) FindOthersInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	inks, err := b.inkService.FindOthersInks()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", inks)
		c.JSON(http.StatusOK, response)
	}
}

// FindTricolorInks implements InkController
func (b *inkController) FindTricolorInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	inks, err := b.inkService.FindTricolorInks()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", inks)
		c.JSON(http.StatusOK, response)
	}
}

// FindYellowInks implements InkController
func (b *inkController) FindYellowInks(c *gin.Context) {
	var (
		inks     []model.Ink
		response helper.Response
	)
	inks, err := b.inkService.FindYellowInks()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", inks)
		c.JSON(http.StatusOK, response)
	}
}

func (b *inkController) InsertInk(c *gin.Context) {
	var (
		ink                model.Ink
		response           helper.Response
		CreateInkParameter model.CreateInkParameter
	)
	err := c.ShouldBindJSON(&CreateInkParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		ink, err = b.inkService.InsertInk(CreateInkParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register ink", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", ink)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *inkController) UpdateInk(c *gin.Context) {
	var (
		newData  model.Ink
		oldData  model.Ink
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.inkService.FindInkById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Ink{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				ink, err := b.inkService.UpdateInk(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update ink", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", ink)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *inkController) DeleteInk(c *gin.Context) {
	var (
		newData  model.Ink
		oldData  model.Ink
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.inkService.FindInkById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Ink{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				ink, err := b.inkService.DeleteInk(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete ink", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", ink)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
