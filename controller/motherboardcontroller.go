package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type MotherboardController interface {
	FindMotherboards(c *gin.Context)
	FindMotherboardById(c *gin.Context)
	FindExcMotherboard(c *gin.Context)
	InsertMotherboard(c *gin.Context)
	UpdateMotherboard(c *gin.Context)
	DeleteMotherboard(c *gin.Context)
}

type motherboardController struct {
	motherboardService service.MotherboardService
	jwtService         service.JWTService
}

func NewMotherboardController(motherboardServ service.MotherboardService, jwtServ service.JWTService) MotherboardController {
	return &motherboardController{
		motherboardService: motherboardServ,
		jwtService:         jwtServ,
	}
}

func (b *motherboardController) FindMotherboards(c *gin.Context) {
	var (
		motherboards []model.Motherboard
		response     helper.Response
	)
	motherboards, err := b.motherboardService.FindMotherboards()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", motherboards)
		c.JSON(http.StatusOK, response)
	}
}

func (b *motherboardController) FindMotherboardById(c *gin.Context) {
	var (
		motherboard model.Motherboard
		response    helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		motherboard, err = b.motherboardService.FindMotherboardById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", motherboard)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *motherboardController) FindExcMotherboard(c *gin.Context) {
	var (
		motherboards []model.Motherboard
		response     helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		motherboards, err = b.motherboardService.FindExcMotherboard(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", motherboards)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *motherboardController) InsertMotherboard(c *gin.Context) {
	var (
		motherboard                model.Motherboard
		response                   helper.Response
		CreateMotherboardParameter model.CreateMotherboardParameter
	)
	err := c.ShouldBindJSON(&CreateMotherboardParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		motherboard, err = b.motherboardService.InsertMotherboard(CreateMotherboardParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register motherboard", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", motherboard)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *motherboardController) UpdateMotherboard(c *gin.Context) {
	var (
		newData  model.Motherboard
		oldData  model.Motherboard
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.motherboardService.FindMotherboardById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Motherboard{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				motherboard, err := b.motherboardService.UpdateMotherboard(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update motherboard", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", motherboard)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *motherboardController) DeleteMotherboard(c *gin.Context) {
	var (
		newData  model.Motherboard
		oldData  model.Motherboard
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.motherboardService.FindMotherboardById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Motherboard{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				motherboard, err := b.motherboardService.DeleteMotherboard(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete motherboard", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", motherboard)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
