package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type MonitorController interface {
	FindMonitors(c *gin.Context)
	FindMonitorById(c *gin.Context)
	FindExcMonitor(c *gin.Context)
	InsertMonitor(c *gin.Context)
	UpdateMonitor(c *gin.Context)
	DeleteMonitor(c *gin.Context)
}

type monitorController struct {
	monitorService service.MonitorService
	jwtService     service.JWTService
}

func NewMonitorController(monitorServ service.MonitorService, jwtServ service.JWTService) MonitorController {
	return &monitorController{
		monitorService: monitorServ,
		jwtService:     jwtServ,
	}
}

func (b *monitorController) FindMonitors(c *gin.Context) {
	var (
		monitors []model.Monitor
		response helper.Response
	)
	monitors, err := b.monitorService.FindMonitors()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", monitors)
		c.JSON(http.StatusOK, response)
	}
}

func (b *monitorController) FindMonitorById(c *gin.Context) {
	var (
		monitor  model.Monitor
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		monitor, err = b.monitorService.FindMonitorById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", monitor)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *monitorController) FindExcMonitor(c *gin.Context) {
	var (
		monitors []model.Monitor
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		monitors, err = b.monitorService.FindExcMonitor(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", monitors)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *monitorController) InsertMonitor(c *gin.Context) {
	var (
		monitor                model.Monitor
		response               helper.Response
		CreateMonitorParameter model.CreateMonitorParameter
	)
	err := c.ShouldBindJSON(&CreateMonitorParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		monitor, err = b.monitorService.InsertMonitor(CreateMonitorParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register monitor", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", monitor)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *monitorController) UpdateMonitor(c *gin.Context) {
	var (
		newData  model.Monitor
		oldData  model.Monitor
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.monitorService.FindMonitorById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Monitor{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				monitor, err := b.monitorService.UpdateMonitor(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update monitor", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", monitor)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *monitorController) DeleteMonitor(c *gin.Context) {
	var (
		newData  model.Monitor
		oldData  model.Monitor
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.monitorService.FindMonitorById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Monitor{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				monitor, err := b.monitorService.DeleteMonitor(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete monitor", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", monitor)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
