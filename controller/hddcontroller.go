package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type HDDController interface {
	FindHDDs(c *gin.Context)
	FindHDDById(c *gin.Context)
	FindExcHDD(c *gin.Context)
	InsertHDD(c *gin.Context)
	UpdateHDD(c *gin.Context)
	DeleteHDD(c *gin.Context)
}

type hddController struct {
	hddService service.HDDService
	jwtService service.JWTService
}

func NewHDDController(hddServ service.HDDService, jwtServ service.JWTService) HDDController {
	return &hddController{
		hddService: hddServ,
		jwtService: jwtServ,
	}
}

func (b *hddController) FindHDDs(c *gin.Context) {
	var (
		hdds     []model.HDD
		response helper.Response
	)
	hdds, err := b.hddService.FindHDDs()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", hdds)
		c.JSON(http.StatusOK, response)
	}
}

func (b *hddController) FindHDDById(c *gin.Context) {
	var (
		hdd      model.HDD
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		hdd, err = b.hddService.FindHDDById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", hdd)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *hddController) FindExcHDD(c *gin.Context) {
	var (
		hdds     []model.HDD
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		hdds, err = b.hddService.FindExcHDD(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", hdds)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *hddController) InsertHDD(c *gin.Context) {
	var (
		hdd                model.HDD
		response           helper.Response
		CreateHDDParameter model.CreateHDDParameter
	)
	err := c.ShouldBindJSON(&CreateHDDParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		hdd, err = b.hddService.InsertHDD(CreateHDDParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register hdd", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", hdd)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *hddController) UpdateHDD(c *gin.Context) {
	var (
		newData  model.HDD
		oldData  model.HDD
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.hddService.FindHDDById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.HDD{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				hdd, err := b.hddService.UpdateHDD(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update hdd", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", hdd)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *hddController) DeleteHDD(c *gin.Context) {
	var (
		newData  model.HDD
		oldData  model.HDD
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.hddService.FindHDDById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.HDD{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				hdd, err := b.hddService.DeleteHDD(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete hdd", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", hdd)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
