package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/go-cmp/cmp"
)

type UserPrinterController interface {
	FindUserPrinters(c *gin.Context)
	FindUserPrinterById(c *gin.Context)
	FindExcUserPrinter(c *gin.Context)
	InsertUserPrinter(c *gin.Context)
	UpdateUserPrinter(c *gin.Context)
	DeleteUserPrinter(c *gin.Context)
}

type userPrinterController struct {
	userPrinterService service.UserPrinterService
	jwtService         service.JWTService
}

func NewUserPrinterController(userPrinterServ service.UserPrinterService, jwtServ service.JWTService) UserPrinterController {
	return &userPrinterController{
		userPrinterService: userPrinterServ,
		jwtService:         jwtServ,
	}
}

func (b *userPrinterController) FindUserPrinters(c *gin.Context) {
	var (
		userPrinters []model.SelectUserPrinterParameter
		response     helper.Response
	)
	userPrinters, err := b.userPrinterService.FindUserPrinters()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", userPrinters)
		c.JSON(http.StatusOK, response)
	}
}

func (b *userPrinterController) FindUserPrinterById(c *gin.Context) {
	var (
		userPrinter model.SelectUserPrinterParameter
		response    helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		userPrinter, err = b.userPrinterService.FindUserPrinterById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", userPrinter)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *userPrinterController) FindExcUserPrinter(c *gin.Context) {
	var (
		userPrinters []model.SelectUserPrinterParameter
		response     helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		userPrinters, err = b.userPrinterService.FindExcUserPrinter(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", userPrinters)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *userPrinterController) InsertUserPrinter(c *gin.Context) {
	var (
		userPrinter                model.UserPrinter
		response                   helper.Response
		CreateUserPrinterParameter model.CreateUserPrinterParameter
	)
	err := c.ShouldBindJSON(&CreateUserPrinterParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		userPrinter, err = b.userPrinterService.InsertUserPrinter(CreateUserPrinterParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register userPrinter", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", userPrinter)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *userPrinterController) UpdateUserPrinter(c *gin.Context) {
	var (
		newData  model.UserPrinter
		oldData  model.SelectUserPrinterParameter
		response helper.Response
	)

	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.userPrinterService.FindUserPrinterById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (cmp.Equal(oldData, model.SelectEmployeeParameter{})) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				userPrinter, err := b.userPrinterService.UpdateUserPrinter(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update userPrinter", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", userPrinter)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
func (b *userPrinterController) DeleteUserPrinter(c *gin.Context) {
	var (
		newData  model.UserPrinter
		oldData  model.SelectUserPrinterParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.userPrinterService.FindUserPrinterById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (cmp.Equal(oldData, model.SelectEmployeeParameter{})) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				userPrinter, err := b.userPrinterService.DeleteUserPrinter(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete userPrinter", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", userPrinter)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
