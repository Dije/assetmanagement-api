package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type HarddiskController interface {
	FindHarddisks(c *gin.Context)
	FindHarddiskById(c *gin.Context)
	FindExcHarddisk(c *gin.Context)
	InsertHarddisk(c *gin.Context)
	UpdateHarddisk(c *gin.Context)
	DeleteHarddisk(c *gin.Context)
}

type harddiskController struct {
	harddiskService service.HarddiskService
	jwtService      service.JWTService
}

func NewHarddiskController(harddiskServ service.HarddiskService, jwtServ service.JWTService) HarddiskController {
	return &harddiskController{
		harddiskService: harddiskServ,
		jwtService:      jwtServ,
	}
}

func (b *harddiskController) FindHarddisks(c *gin.Context) {
	var (
		harddisks []model.SelectHarddiskParameter
		response  helper.Response
	)
	harddisks, err := b.harddiskService.FindHarddisks()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", harddisks)
		c.JSON(http.StatusOK, response)
	}
}

func (b *harddiskController) FindHarddiskById(c *gin.Context) {
	var (
		harddisk model.SelectHarddiskParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		harddisk, err = b.harddiskService.FindHarddiskById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", harddisk)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *harddiskController) FindExcHarddisk(c *gin.Context) {
	var (
		harddisks []model.SelectHarddiskParameter
		response  helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		harddisks, err = b.harddiskService.FindExcHarddisk(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", harddisks)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *harddiskController) InsertHarddisk(c *gin.Context) {
	var (
		harddisk                model.Harddisk
		response                helper.Response
		CreateHarddiskParameter model.CreateHarddiskParameter
	)
	err := c.ShouldBindJSON(&CreateHarddiskParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		harddisk, err = b.harddiskService.InsertHarddisk(CreateHarddiskParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register harddisk", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", harddisk)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *harddiskController) UpdateHarddisk(c *gin.Context) {
	var (
		newData  model.Harddisk
		oldData  model.SelectHarddiskParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.harddiskService.FindHarddiskById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.SelectHarddiskParameter{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				harddisk, err := b.harddiskService.UpdateHarddisk(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update harddisk", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", harddisk)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *harddiskController) DeleteHarddisk(c *gin.Context) {
	var (
		newData  model.Harddisk
		oldData  model.SelectHarddiskParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.harddiskService.FindHarddiskById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.SelectHarddiskParameter{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				harddisk, err := b.harddiskService.DeleteHarddisk(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete harddisk", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", harddisk)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
