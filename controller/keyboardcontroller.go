package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type KeyboardController interface {
	FindKeyboards(c *gin.Context)
	FindKeyboardById(c *gin.Context)
	FindExcKeyboard(c *gin.Context)
	InsertKeyboard(c *gin.Context)
	UpdateKeyboard(c *gin.Context)
	DeleteKeyboard(c *gin.Context)
}

type keyboardController struct {
	keyboardService service.KeyboardService
	jwtService      service.JWTService
}

func NewKeyboardController(keyboardServ service.KeyboardService, jwtServ service.JWTService) KeyboardController {
	return &keyboardController{
		keyboardService: keyboardServ,
		jwtService:      jwtServ,
	}
}

func (b *keyboardController) FindKeyboards(c *gin.Context) {
	var (
		keyboards []model.Keyboard
		response  helper.Response
	)
	keyboards, err := b.keyboardService.FindKeyboards()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", keyboards)
		c.JSON(http.StatusOK, response)
	}
}

func (b *keyboardController) FindKeyboardById(c *gin.Context) {
	var (
		keyboard model.Keyboard
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		keyboard, err = b.keyboardService.FindKeyboardById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", keyboard)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *keyboardController) FindExcKeyboard(c *gin.Context) {
	var (
		keyboards []model.Keyboard
		response  helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		keyboards, err = b.keyboardService.FindExcKeyboard(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", keyboards)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *keyboardController) InsertKeyboard(c *gin.Context) {
	var (
		keyboard                model.Keyboard
		response                helper.Response
		CreateKeyboardParameter model.CreateKeyboardParameter
	)
	err := c.ShouldBindJSON(&CreateKeyboardParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		keyboard, err = b.keyboardService.InsertKeyboard(CreateKeyboardParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register keyboard", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", keyboard)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *keyboardController) UpdateKeyboard(c *gin.Context) {
	var (
		newData  model.Keyboard
		oldData  model.Keyboard
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.keyboardService.FindKeyboardById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Keyboard{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				keyboard, err := b.keyboardService.UpdateKeyboard(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update keyboard", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", keyboard)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *keyboardController) DeleteKeyboard(c *gin.Context) {
	var (
		newData  model.Keyboard
		oldData  model.Keyboard
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.keyboardService.FindKeyboardById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Keyboard{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				keyboard, err := b.keyboardService.DeleteKeyboard(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete keyboard", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", keyboard)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
