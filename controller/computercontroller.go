package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/go-cmp/cmp"
)

type ComputerController interface {
	FindComputers(c *gin.Context)
	FindComputerById(c *gin.Context)
	InsertComputer(c *gin.Context)
	UpdateComputer(c *gin.Context)
	DeleteComputer(c *gin.Context)
}

type computerController struct {
	computerService service.ComputerService
	jwtService      service.JWTService
}

func NewComputerController(computerServ service.ComputerService, jwtServ service.JWTService) ComputerController {
	return &computerController{
		computerService: computerServ,
		jwtService:      jwtServ,
	}
}

func (b *computerController) FindComputers(c *gin.Context) {
	var (
		computers []model.SelectComputerParameter
		response  helper.Response
	)
	computers, err := b.computerService.FindComputers()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", computers)
		c.JSON(http.StatusOK, response)
	}
}

func (b *computerController) FindComputerById(c *gin.Context) {
	var (
		computer model.SelectComputerParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		computer, err = b.computerService.FindComputerById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", computer)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *computerController) InsertComputer(c *gin.Context) {
	var (
		computer                model.Computer
		response                helper.Response
		CreateComputerParameter model.CreateComputerParameter
	)
	err := c.ShouldBindJSON(&CreateComputerParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		computer, err = b.computerService.InsertComputer(CreateComputerParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register computer", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", computer)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *computerController) UpdateComputer(c *gin.Context) {
	var (
		newData  model.Computer
		oldData  model.SelectComputerParameter
		response helper.Response
	)

	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.computerService.FindComputerById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (cmp.Equal(oldData, model.SelectEmployeeParameter{})) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				computer, err := b.computerService.UpdateComputer(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update computer", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", computer)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
func (b *computerController) DeleteComputer(c *gin.Context) {
	var (
		newData  model.Computer
		oldData  model.SelectComputerParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.computerService.FindComputerById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (cmp.Equal(oldData, model.SelectEmployeeParameter{})) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				computer, err := b.computerService.DeleteComputer(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete computer", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", computer)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
