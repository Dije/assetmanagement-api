package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type SubscriptionController interface {
	FindSubscriptions(c *gin.Context)
	FindSubscriptionById(c *gin.Context)
	FindExcSubscription(c *gin.Context)
	InsertSubscription(c *gin.Context)
	UpdateSubscription(c *gin.Context)
	DeleteSubscription(c *gin.Context)
}

type subscriptionController struct {
	subscriptionService service.SubscriptionService
	jwtService          service.JWTService
}

func NewSubscriptionController(subscriptionServ service.SubscriptionService, jwtServ service.JWTService) SubscriptionController {
	return &subscriptionController{
		subscriptionService: subscriptionServ,
		jwtService:          jwtServ,
	}
}

func (b *subscriptionController) FindSubscriptions(c *gin.Context) {
	var (
		subscriptions []model.Subscription
		response      helper.Response
	)
	subscriptions, err := b.subscriptionService.FindSubscriptions()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", subscriptions)
		c.JSON(http.StatusOK, response)
	}
}

func (b *subscriptionController) FindSubscriptionById(c *gin.Context) {
	var (
		subscription model.Subscription
		response     helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		subscription, err = b.subscriptionService.FindSubscriptionById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", subscription)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *subscriptionController) FindExcSubscription(c *gin.Context) {
	var (
		subscriptions []model.Subscription
		response      helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		subscriptions, err = b.subscriptionService.FindExcSubscription(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", subscriptions)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *subscriptionController) InsertSubscription(c *gin.Context) {
	var (
		subscription                model.Subscription
		response                    helper.Response
		CreateSubscriptionParameter model.CreateSubscriptionParameter
	)
	err := c.ShouldBindJSON(&CreateSubscriptionParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		subscription, err = b.subscriptionService.InsertSubscription(CreateSubscriptionParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register subscription", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", subscription)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *subscriptionController) UpdateSubscription(c *gin.Context) {
	var (
		newData  model.Subscription
		oldData  model.Subscription
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.subscriptionService.FindSubscriptionById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Subscription{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				subscription, err := b.subscriptionService.UpdateSubscription(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update subscription", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", subscription)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *subscriptionController) DeleteSubscription(c *gin.Context) {
	var (
		newData  model.Subscription
		oldData  model.Subscription
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.subscriptionService.FindSubscriptionById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Subscription{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				subscription, err := b.subscriptionService.DeleteSubscription(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete subscription", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", subscription)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
