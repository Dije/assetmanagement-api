package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/go-cmp/cmp"
)

type LaptopController interface {
	FindLaptops(c *gin.Context)
	FindLaptopById(c *gin.Context)
	FindExcLaptop(c *gin.Context)
	InsertLaptop(c *gin.Context)
	UpdateLaptop(c *gin.Context)
	DeleteLaptop(c *gin.Context)
}

type laptopController struct {
	laptopService service.LaptopService
	jwtService    service.JWTService
}

func NewLaptopController(laptopServ service.LaptopService, jwtServ service.JWTService) LaptopController {
	return &laptopController{
		laptopService: laptopServ,
		jwtService:    jwtServ,
	}
}

func (b *laptopController) FindLaptops(c *gin.Context) {
	var (
		laptops  []model.SelectLaptopParameter
		response helper.Response
	)
	laptops, err := b.laptopService.FindLaptops()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", laptops)
		c.JSON(http.StatusOK, response)
	}
}

func (b *laptopController) FindLaptopById(c *gin.Context) {
	var (
		laptop   model.SelectLaptopParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		laptop, err = b.laptopService.FindLaptopById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", laptop)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *laptopController) FindExcLaptop(c *gin.Context) {
	var (
		laptop   []model.SelectLaptopParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		laptop, err = b.laptopService.FindExcLaptop(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", laptop)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *laptopController) InsertLaptop(c *gin.Context) {
	var (
		laptop                model.Laptop
		response              helper.Response
		CreateLaptopParameter model.CreateLaptopParameter
	)
	err := c.ShouldBindJSON(&CreateLaptopParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		laptop, err = b.laptopService.InsertLaptop(CreateLaptopParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register laptop", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", laptop)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *laptopController) UpdateLaptop(c *gin.Context) {
	var (
		newData  model.Laptop
		oldData  model.SelectLaptopParameter
		response helper.Response
	)

	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.laptopService.FindLaptopById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (cmp.Equal(oldData, model.SelectEmployeeParameter{})) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				laptop, err := b.laptopService.UpdateLaptop(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update laptop", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", laptop)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
func (b *laptopController) DeleteLaptop(c *gin.Context) {
	var (
		newData  model.Laptop
		oldData  model.SelectLaptopParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.laptopService.FindLaptopById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (cmp.Equal(oldData, model.SelectEmployeeParameter{})) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				laptop, err := b.laptopService.DeleteLaptop(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete laptop", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", laptop)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
