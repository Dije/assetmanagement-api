package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type OfficeController interface {
	FindOffices(c *gin.Context)
	FindOfficeById(c *gin.Context)
	FindExcOffice(c *gin.Context)
	InsertOffice(c *gin.Context)
	UpdateOffice(c *gin.Context)
	DeleteOffice(c *gin.Context)
}

type officeController struct {
	officeService service.OfficeService
	jwtService    service.JWTService
}

func NewOfficeController(officeServ service.OfficeService, jwtServ service.JWTService) OfficeController {
	return &officeController{
		officeService: officeServ,
		jwtService:    jwtServ,
	}
}

func (b *officeController) FindOffices(c *gin.Context) {
	var (
		offices  []model.Office
		response helper.Response
	)
	offices, err := b.officeService.FindOffices()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", offices)
		c.JSON(http.StatusOK, response)
	}
}

func (b *officeController) FindOfficeById(c *gin.Context) {
	var (
		office   model.Office
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		office, err = b.officeService.FindOfficeById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", office)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *officeController) FindExcOffice(c *gin.Context) {
	var (
		offices  []model.Office
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		offices, err = b.officeService.FindExcOffice(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", offices)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *officeController) InsertOffice(c *gin.Context) {
	var (
		office                model.Office
		response              helper.Response
		CreateOfficeParameter model.CreateOfficeParameter
	)
	err := c.ShouldBindJSON(&CreateOfficeParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		office, err = b.officeService.InsertOffice(CreateOfficeParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register office", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", office)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *officeController) UpdateOffice(c *gin.Context) {
	var (
		newData  model.Office
		oldData  model.Office
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.officeService.FindOfficeById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Office{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				office, err := b.officeService.UpdateOffice(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update office", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", office)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *officeController) DeleteOffice(c *gin.Context) {
	var (
		newData  model.Office
		oldData  model.Office
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.officeService.FindOfficeById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Office{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				office, err := b.officeService.DeleteOffice(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete office", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", office)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
