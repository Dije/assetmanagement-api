package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CasingController interface {
	FindCasings(c *gin.Context)
	FindCasingById(c *gin.Context)
	FindExcCasing(c *gin.Context)
	InsertCasing(c *gin.Context)
	UpdateCasing(c *gin.Context)
	DeleteCasing(c *gin.Context)
}

type casingController struct {
	casingService service.CasingService
	jwtService    service.JWTService
}

func NewCasingController(casingServ service.CasingService, jwtServ service.JWTService) CasingController {
	return &casingController{
		casingService: casingServ,
		jwtService:    jwtServ,
	}
}

func (b *casingController) FindCasings(c *gin.Context) {
	var (
		casings  []model.Casing
		response helper.Response
	)
	casings, err := b.casingService.FindCasings()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", casings)
		c.JSON(http.StatusOK, response)
	}
}

func (b *casingController) FindCasingById(c *gin.Context) {
	var (
		casing   model.Casing
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		casing, err = b.casingService.FindCasingById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", casing)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *casingController) FindExcCasing(c *gin.Context) {
	var (
		casings  []model.Casing
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		casings, err = b.casingService.FindExcCasing(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", casings)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *casingController) InsertCasing(c *gin.Context) {
	var (
		casing                model.Casing
		response              helper.Response
		CreateCasingParameter model.CreateCasingParameter
	)
	err := c.ShouldBindJSON(&CreateCasingParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		casing, err = b.casingService.InsertCasing(CreateCasingParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register casing", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", casing)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *casingController) UpdateCasing(c *gin.Context) {
	var (
		newData  model.Casing
		oldData  model.Casing
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.casingService.FindCasingById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Casing{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				casing, err := b.casingService.UpdateCasing(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update casing", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", casing)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *casingController) DeleteCasing(c *gin.Context) {
	var (
		newData  model.Casing
		oldData  model.Casing
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.casingService.FindCasingById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.Casing{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				casing, err := b.casingService.DeleteCasing(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete casing", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", casing)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
