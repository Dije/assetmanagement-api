package controller

import (
	"deliportal-api/helper"
	"deliportal-api/model"
	"deliportal-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ServiceLaptopController interface {
	FindServiceLaptops(c *gin.Context)
	FindServiceLaptopById(c *gin.Context)
	FindExcServiceLaptop(c *gin.Context)
	InsertServiceLaptop(c *gin.Context)
	UpdateServiceLaptop(c *gin.Context)
	DeleteServiceLaptop(c *gin.Context)
}

type serviceLaptopController struct {
	serviceLaptopService service.ServiceLaptopService
	jwtService           service.JWTService
}

func NewServiceLaptopController(serviceLaptopServ service.ServiceLaptopService, jwtServ service.JWTService) ServiceLaptopController {
	return &serviceLaptopController{
		serviceLaptopService: serviceLaptopServ,
		jwtService:           jwtServ,
	}
}

func (b *serviceLaptopController) FindServiceLaptops(c *gin.Context) {
	var (
		serviceLaptops []model.SelectServiceLaptopParameter
		response       helper.Response
	)
	serviceLaptops, err := b.serviceLaptopService.FindServiceLaptops()
	if err != nil {
		response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusNotFound, response)
	} else {
		response = helper.BuildResponse(true, "OK", serviceLaptops)
		c.JSON(http.StatusOK, response)
	}
}

func (b *serviceLaptopController) FindServiceLaptopById(c *gin.Context) {
	var (
		serviceLaptop model.SelectServiceLaptopParameter
		response      helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		serviceLaptop, err = b.serviceLaptopService.FindServiceLaptopById(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", serviceLaptop)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *serviceLaptopController) FindExcServiceLaptop(c *gin.Context) {
	var (
		serviceLaptops []model.SelectServiceLaptopParameter
		response       helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		serviceLaptops, err = b.serviceLaptopService.FindExcServiceLaptop(uint(id))
		if err != nil {
			response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusNotFound, response)
		} else {
			response = helper.BuildResponse(true, "OK", serviceLaptops)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *serviceLaptopController) InsertServiceLaptop(c *gin.Context) {
	var (
		serviceLaptop                model.ServiceLaptop
		response                     helper.Response
		CreateServiceLaptopParameter model.CreateServiceLaptopParameter
	)
	err := c.ShouldBindJSON(&CreateServiceLaptopParameter)
	if err != nil {
		response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		c.JSON(http.StatusBadRequest, response)
	} else {
		serviceLaptop, err = b.serviceLaptopService.InsertServiceLaptop(CreateServiceLaptopParameter)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to register serviceLaptop", err.Error(), helper.EmptyObj{})
			c.JSON(http.StatusBadRequest, response)
		} else {
			response = helper.BuildResponse(true, "OK", serviceLaptop)
			c.JSON(http.StatusOK, response)
		}
	}
}

func (b *serviceLaptopController) UpdateServiceLaptop(c *gin.Context) {
	var (
		newData  model.ServiceLaptop
		oldData  model.SelectServiceLaptopParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.serviceLaptopService.FindServiceLaptopById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.SelectServiceLaptopParameter{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				serviceLaptop, err := b.serviceLaptopService.UpdateServiceLaptop(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to update serviceLaptop", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", serviceLaptop)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}

func (b *serviceLaptopController) DeleteServiceLaptop(c *gin.Context) {
	var (
		newData  model.ServiceLaptop
		oldData  model.SelectServiceLaptopParameter
		response helper.Response
	)
	id, err := strconv.ParseUint(c.Param("id"), 0, 0)
	if err != nil {
		response = helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusBadRequest, response)
	} else {
		err := c.ShouldBindJSON(&newData)
		if err != nil {
			response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
		} else {
			oldData, err = b.serviceLaptopService.FindServiceLaptopById(uint(id))
			if err != nil {
				response = helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else if (oldData == model.SelectServiceLaptopParameter{}) {
				response = helper.BuildErrorResponse("Data not found", err.Error(), helper.EmptyObj{})
				c.JSON(http.StatusNotFound, response)
			} else {
				serviceLaptop, err := b.serviceLaptopService.DeleteServiceLaptop(newData, uint(id))
				if err != nil {
					response = helper.BuildErrorResponse("Failed to delete serviceLaptop", err.Error(), helper.EmptyObj{})
					c.AbortWithStatusJSON(http.StatusBadRequest, response)
				} else {
					response = helper.BuildResponse(true, "OK", serviceLaptop)
					c.JSON(http.StatusOK, response)
				}
			}
		}
	}
}
