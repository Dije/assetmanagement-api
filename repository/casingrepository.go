package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type CasingRepository interface {
	FindCasings() (casingOutput []model.Casing, err error)
	FindCasingById(id uint) (casingOutput model.Casing, err error)
	FindExcCasing(id uint) (casingOutput []model.Casing, err error)
	InsertCasing(casing model.Casing) (casingOutput model.Casing, err error)
	UpdateCasing(casing model.Casing, id uint) (casingOutput model.Casing, err error)
}

type CasingConnection struct {
	connection *gorm.DB
}

func NewCasingRepository(db *gorm.DB) CasingRepository {
	return &CasingConnection{
		connection: db,
	}
}

func (db *CasingConnection) FindCasings() (casingOutput []model.Casing, err error) {
	var (
		casings []model.Casing
	)
	res := db.connection.Where("deleted_at = 0").Order("casing_name").Find(&casings)
	return casings, res.Error
}

func (db *CasingConnection) FindCasingById(id uint) (casingOutput model.Casing, err error) {
	var (
		casing model.Casing
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&casing)
	return casing, res.Error
}

func (db *CasingConnection) FindExcCasing(id uint) (casingOutput []model.Casing, err error) {
	var (
		casings []model.Casing
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("casing_name").Find(&casings)
	return casings, res.Error
}

func (db *CasingConnection) InsertCasing(casing model.Casing) (casingOutput model.Casing, err error) {
	res := db.connection.Save(&casing)
	return casing, res.Error
}

func (db *CasingConnection) UpdateCasing(casing model.Casing, id uint) (casingOutput model.Casing, err error) {
	res := db.connection.Where("id=?", id).Updates(&casing)
	return casing, res.Error
}
