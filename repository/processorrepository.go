package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type ProcessorRepository interface {
	FindProcessors() (processorOutput []model.Processor, err error)
	FindProcessorById(id uint) (processorOutput model.Processor, err error)
	FindExcProcessor(id uint) (processorOutput []model.Processor, err error)
	InsertProcessor(processor model.Processor) (processorOutput model.Processor, err error)
	UpdateProcessor(processor model.Processor, id uint) (processorOutput model.Processor, err error)
}

type ProcessorConnection struct {
	connection *gorm.DB
}

func NewProcessorRepository(db *gorm.DB) ProcessorRepository {
	return &ProcessorConnection{
		connection: db,
	}
}

func (db *ProcessorConnection) FindProcessors() (processorOutput []model.Processor, err error) {
	var (
		processors []model.Processor
	)
	res := db.connection.Where("deleted_at = 0").Order("processor_name").Find(&processors)
	return processors, res.Error
}

func (db *ProcessorConnection) FindProcessorById(id uint) (processorOutput model.Processor, err error) {
	var (
		processor model.Processor
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&processor)
	return processor, res.Error
}

func (db *ProcessorConnection) FindExcProcessor(id uint) (processorOutput []model.Processor, err error) {
	var (
		processors []model.Processor
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("processor_name").Find(&processors)
	return processors, res.Error
}

func (db *ProcessorConnection) InsertProcessor(processor model.Processor) (processorOutput model.Processor, err error) {
	res := db.connection.Save(&processor)
	return processor, res.Error
}

func (db *ProcessorConnection) UpdateProcessor(processor model.Processor, id uint) (processorOutput model.Processor, err error) {
	res := db.connection.Where("id=?", id).Updates(&processor)
	return processor, res.Error
}
