package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type RAMRepository interface {
	FindRAMs() (ramOutput []model.RAM, err error)
	FindRAMById(id uint) (ramOutput model.RAM, err error)
	FindExcRAM(id uint) (ramOutput []model.RAM, err error)
	InsertRAM(ram model.RAM) (ramOutput model.RAM, err error)
	UpdateRAM(ram model.RAM, id uint) (ramOutput model.RAM, err error)
}

type RAMConnection struct {
	connection *gorm.DB
}

func NewRAMRepository(db *gorm.DB) RAMRepository {
	return &RAMConnection{
		connection: db,
	}
}

func (db *RAMConnection) FindRAMs() (ramOutput []model.RAM, err error) {
	var (
		rams []model.RAM
	)
	res := db.connection.Where("deleted_at = 0").Order("ram_name").Find(&rams)
	return rams, res.Error
}

func (db *RAMConnection) FindRAMById(id uint) (ramOutput model.RAM, err error) {
	var (
		ram model.RAM
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&ram)
	return ram, res.Error
}

func (db *RAMConnection) FindExcRAM(id uint) (ramOutput []model.RAM, err error) {
	var (
		rams []model.RAM
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("ram_name").Find(&rams)
	return rams, res.Error
}

func (db *RAMConnection) InsertRAM(ram model.RAM) (ramOutput model.RAM, err error) {
	res := db.connection.Save(&ram)
	return ram, res.Error
}

func (db *RAMConnection) UpdateRAM(ram model.RAM, id uint) (ramOutput model.RAM, err error) {
	res := db.connection.Where("id=?", id).Updates(&ram)
	return ram, res.Error
}
