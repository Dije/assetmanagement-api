package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type ServiceLaptopRepository interface {
	FindServiceLaptops() (serviceLaptopOutput []model.SelectServiceLaptopParameter, err error)
	FindServiceLaptopById(id uint) (serviceLaptopOutput model.SelectServiceLaptopParameter, err error)
	FindExcServiceLaptop(id uint) (serviceLaptopOutput []model.SelectServiceLaptopParameter, err error)
	InsertServiceLaptop(serviceLaptop model.ServiceLaptop) (serviceLaptopOutput model.ServiceLaptop, err error)
	UpdateServiceLaptop(serviceLaptop model.ServiceLaptop, id uint) (serviceLaptopOutput model.ServiceLaptop, err error)
}

type ServiceLaptopConnection struct {
	connection *gorm.DB
}

func NewServiceLaptopRepository(db *gorm.DB) ServiceLaptopRepository {
	return &ServiceLaptopConnection{
		connection: db,
	}
}

func (db *ServiceLaptopConnection) FindServiceLaptops() (serviceLaptopOutput []model.SelectServiceLaptopParameter, err error) {
	var (
		serviceLaptops []model.SelectServiceLaptopParameter
	)
	res := db.connection.Debug().Table("service_laptops").Select("service_laptops.id, service_laptops.laptop_id, laptops.laptop_code, service_laptops.date_service, service_laptops.date_done, service_laptops.company, service_laptops.problem, service_laptops.price, service_laptops.solved, service_laptops.remark, service_laptops.created_user_id, service_laptops.updated_user_id, service_laptops.deleted_user_id, service_laptops.created_at, service_laptops.updated_at, service_laptops.deleted_at").Joins("left join laptops ON service_laptops.laptop_id = laptops.id").Where("service_laptops.deleted_at = 0").Order("service_laptops.id").Find(&serviceLaptops)
	return serviceLaptops, res.Error
}

func (db *ServiceLaptopConnection) FindServiceLaptopById(id uint) (serviceLaptopOutput model.SelectServiceLaptopParameter, err error) {
	var (
		serviceLaptop model.SelectServiceLaptopParameter
	)
	res := db.connection.Debug().Table("service_laptops").Select("service_laptops.id, service_laptops.laptop_id, laptops.laptop_code, service_laptops.date_service, service_laptops.date_done, service_laptops.company, service_laptops.problem, service_laptops.price, service_laptops.solved, service_laptops.remark, service_laptops.created_user_id, service_laptops.updated_user_id, service_laptops.deleted_user_id, service_laptops.created_at, service_laptops.updated_at, service_laptops.deleted_at").Joins("left join laptops ON service_laptops.laptop_id = laptops.id").Where("service_laptops.id=? AND service_laptops.deleted_at = 0", id).Take(&serviceLaptop)
	return serviceLaptop, res.Error
}

func (db *ServiceLaptopConnection) FindExcServiceLaptop(id uint) (serviceLaptopOutput []model.SelectServiceLaptopParameter, err error) {
	var (
		serviceLaptops []model.SelectServiceLaptopParameter
	)
	res := db.connection.Debug().Table("service_laptops").Select("service_laptops.id, service_laptops.laptop_id, laptops.laptop_code, service_laptops.date_service, service_laptops.date_done, service_laptops.company, service_laptops.problem, service_laptops.price, service_laptops.solved, service_laptops.remark, service_laptops.created_user_id, service_laptops.updated_user_id, service_laptops.deleted_user_id, service_laptops.created_at, service_laptops.updated_at, service_laptops.deleted_at").Joins("left join laptops ON service_laptops.laptop_id = laptops.id").Where("service_laptops.id!=? AND service_laptops.deleted_at = 0", id).Order("id").Find(&serviceLaptops)
	return serviceLaptops, res.Error
}

func (db *ServiceLaptopConnection) InsertServiceLaptop(serviceLaptop model.ServiceLaptop) (serviceLaptopOutput model.ServiceLaptop, err error) {
	res := db.connection.Save(&serviceLaptop)
	return serviceLaptop, res.Error
}

func (db *ServiceLaptopConnection) UpdateServiceLaptop(serviceLaptop model.ServiceLaptop, id uint) (serviceLaptopOutput model.ServiceLaptop, err error) {
	res := db.connection.Where("id=?", id).Updates(&serviceLaptop)
	return serviceLaptop, res.Error
}
