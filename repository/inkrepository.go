package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type InkRepository interface {
	FindInks() (inkOutput []model.Ink, err error)
	FindInkById(id uint) (inkOutput model.Ink, err error)
	FindExcInk(id uint) (inkOutput []model.Ink, err error)
	FindBlackInks() (inkOutput []model.Ink, err error)
	FindExcBlackInks(id uint) (inkOutput []model.Ink, err error)
	FindCyanInks() (inkOutput []model.Ink, err error)
	FindExcCyanInks(id uint) (inkOutput []model.Ink, err error)
	FindMagentaInks() (inkOutput []model.Ink, err error)
	FindExcMagentaInks(id uint) (inkOutput []model.Ink, err error)
	FindYellowInks() (inkOutput []model.Ink, err error)
	FindExcYellowInks(id uint) (inkOutput []model.Ink, err error)
	FindColorInks() (inkOutput []model.Ink, err error)
	FindExcColorInks(id uint) (inkOutput []model.Ink, err error)
	FindTricolorInks() (inkOutput []model.Ink, err error)
	FindExcTricolorInks(id uint) (inkOutput []model.Ink, err error)
	FindOthersInks() (inkOutput []model.Ink, err error)
	FindExcOthersInks(id uint) (inkOutput []model.Ink, err error)
	InsertInk(ink model.Ink) (inkOutput model.Ink, err error)
	UpdateInk(ink model.Ink, id uint) (inkOutput model.Ink, err error)
}

type InkConnection struct {
	connection *gorm.DB
}

func NewInkRepository(db *gorm.DB) InkRepository {
	return &InkConnection{
		connection: db,
	}
}

func (db *InkConnection) FindInks() (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("deleted_at = 0").Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindInkById(id uint) (inkOutput model.Ink, err error) {
	var (
		ink model.Ink
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&ink)
	return ink, res.Error
}

func (db *InkConnection) FindExcInk(id uint) (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindBlackInks() (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("ink_name LIKE '%BK-%' OR ink_name LIKE '%N/A' AND deleted_at = 0").Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindExcBlackInks(id uint) (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("id!= ? AND ink_name LIKE '%BK-%' OR ink_name LIKE '%N/A' AND deleted_at = 0", id).Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindCyanInks() (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("ink_name LIKE '%C-%' OR ink_name LIKE '%N/A' AND deleted_at = 0").Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindExcCyanInks(id uint) (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("id!= ? AND ink_name LIKE '%C-%' OR ink_name LIKE '%N/A' AND deleted_at = 0", id).Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindMagentaInks() (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("ink_name LIKE '%M-%' OR ink_name LIKE '%N/A' AND deleted_at = 0").Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindExcMagentaInks(id uint) (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("id!= ? AND ink_name LIKE '%M-%' OR ink_name LIKE '%N/A' AND deleted_at = 0", id).Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindYellowInks() (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("ink_name LIKE '%Y-%' OR ink_name LIKE '%N/A' AND deleted_at = 0").Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindExcYellowInks(id uint) (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("id!= ? AND ink_name LIKE '%Y-%' OR ink_name LIKE '%N/A' AND deleted_at = 0", id).Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindColorInks() (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("ink_name LIKE '%CL-%' OR ink_name LIKE '%N/A' AND deleted_at = 0").Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindExcColorInks(id uint) (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("id!= ? AND ink_name LIKE '%CL-%' OR ink_name LIKE '%N/A' AND deleted_at = 0", id).Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindTricolorInks() (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("ink_name LIKE '%Tricolor-%' OR ink_name LIKE '%N/A' AND deleted_at = 0").Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindExcTricolorInks(id uint) (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("id!= ? AND ink_name LIKE '%Tricolor-%' OR ink_name LIKE '%N/A' AND deleted_at = 0", id).Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindOthersInks() (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("ink_name LIKE '%Others-%' OR ink_name LIKE '%N/A' AND deleted_at = 0").Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) FindExcOthersInks(id uint) (inkOutput []model.Ink, err error) {
	var (
		inks []model.Ink
	)
	res := db.connection.Where("id!= ? AND ink_name LIKE '%Others-%' OR ink_name LIKE '%N/A' AND deleted_at = 0", id).Order("ink_name").Find(&inks)
	return inks, res.Error
}

func (db *InkConnection) InsertInk(ink model.Ink) (inkOutput model.Ink, err error) {
	res := db.connection.Save(&ink)
	return ink, res.Error
}

func (db *InkConnection) UpdateInk(ink model.Ink, id uint) (inkOutput model.Ink, err error) {
	res := db.connection.Where("id=?", id).Updates(&ink)
	return ink, res.Error
}
