package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type ServicePrinterRepository interface {
	FindServicePrinters() (servicePrinterOutput []model.SelectServicePrinterParameter, err error)
	FindServicePrinterById(id uint) (servicePrinterOutput model.SelectServicePrinterParameter, err error)
	FindExcServicePrinter(id uint) (servicePrinterOutput []model.SelectServicePrinterParameter, err error)
	InsertServicePrinter(servicePrinter model.ServicePrinter) (servicePrinterOutput model.ServicePrinter, err error)
	UpdateServicePrinter(servicePrinter model.ServicePrinter, id uint) (servicePrinterOutput model.ServicePrinter, err error)
}

type ServicePrinterConnection struct {
	connection *gorm.DB
}

func NewServicePrinterRepository(db *gorm.DB) ServicePrinterRepository {
	return &ServicePrinterConnection{
		connection: db,
	}
}

func (db *ServicePrinterConnection) FindServicePrinters() (servicePrinterOutput []model.SelectServicePrinterParameter, err error) {
	var (
		servicePrinters []model.SelectServicePrinterParameter
	)
	res := db.connection.Debug().Table("service_printers").Select("service_printers.id, service_printers.user_printer_id, user_printers.printer_code, service_printers.date_service, service_printers.date_done, service_printers.company, service_printers.problem, service_printers.price, service_printers.solved, service_printers.remark, service_printers.created_user_id, service_printers.updated_user_id, service_printers.deleted_user_id, service_printers.created_at, service_printers.updated_at, service_printers.deleted_at").Joins("left join user_printers ON service_printers.user_printer_id = user_printers.id").Where("service_printers.deleted_at = 0").Order("service_printers.id").Find(&servicePrinters)
	return servicePrinters, res.Error
}

func (db *ServicePrinterConnection) FindServicePrinterById(id uint) (servicePrinterOutput model.SelectServicePrinterParameter, err error) {
	var (
		servicePrinter model.SelectServicePrinterParameter
	)
	res := db.connection.Debug().Table("service_printers").Select("service_printers.id, service_printers.user_printer_id, user_printers.printer_code, service_printers.date_service, service_printers.date_done, service_printers.company, service_printers.problem, service_printers.price, service_printers.solved, service_printers.remark, service_printers.created_user_id, service_printers.updated_user_id, service_printers.deleted_user_id, service_printers.created_at, service_printers.updated_at, service_printers.deleted_at").Joins("left join user_printers ON service_printers.user_printer_id = user_printers.id").Where("service_printers.id=? AND service_printers.deleted_at = 0", id).Take(&servicePrinter)
	return servicePrinter, res.Error
}

func (db *ServicePrinterConnection) FindExcServicePrinter(id uint) (servicePrinterOutput []model.SelectServicePrinterParameter, err error) {
	var (
		servicePrinters []model.SelectServicePrinterParameter
	)
	res := db.connection.Debug().Table("service_printers").Select("service_printers.id, service_printers.user_printer_id, user_printers.printer_code, service_printers.date_service, service_printers.date_done, service_printers.company, service_printers.problem, service_printers.price, service_printers.solved, service_printers.remark, service_printers.created_user_id, service_printers.updated_user_id, service_printers.deleted_user_id, service_printers.created_at, service_printers.updated_at, service_printers.deleted_at").Joins("left join user_printers ON service_printers.user_printer_id = user_printers.id").Where("service_printers.id!=? AND service_printers.deleted_at = 0", id).Order("id").Find(&servicePrinters)
	return servicePrinters, res.Error
}

func (db *ServicePrinterConnection) InsertServicePrinter(servicePrinter model.ServicePrinter) (servicePrinterOutput model.ServicePrinter, err error) {
	res := db.connection.Save(&servicePrinter)
	return servicePrinter, res.Error
}

func (db *ServicePrinterConnection) UpdateServicePrinter(servicePrinter model.ServicePrinter, id uint) (servicePrinterOutput model.ServicePrinter, err error) {
	res := db.connection.Where("id=?", id).Updates(&servicePrinter)
	return servicePrinter, res.Error
}
