package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type UserPrinterRepository interface {
	FindUserPrinters() (userPrinterOutput []model.SelectUserPrinterParameter, err error)
	FindUserPrinterById(id uint) (userPrinterOutput model.SelectUserPrinterParameter, err error)
	FindExcUserPrinter(id uint) (userPrinterOutput []model.SelectUserPrinterParameter, err error)
	InsertUserPrinter(userPrinter model.UserPrinter) (userPrinterOutput model.UserPrinter, err error)
	UpdateUserPrinter(userPrinter model.UserPrinter, id uint) (userPrinterOutput model.UserPrinter, err error)
}

type UserPrinterConnection struct {
	connection *gorm.DB
}

func NewUserPrinterRepository(db *gorm.DB) UserPrinterRepository {
	return &UserPrinterConnection{
		connection: db,
	}
}

func (db *UserPrinterConnection) FindUserPrinters() (userPrinterOutput []model.SelectUserPrinterParameter, err error) {
	var (
		user_printers []model.SelectUserPrinterParameter
	)

	res := db.connection.Debug().Table("user_printers").Select("user_printers.id, user_printers.printer_code, user_printers.printer_id, printers.printer_name, user_printers.user_id, users.username, user_printers.price, user_printers.warranty, user_printers.remark,	user_printers.created_user_id, user_printers.updated_user_id,	user_printers.deleted_user_id, user_printers.created_at, user_printers.updated_at, user_printers.deleted_at").Joins("left join users ON user_printers.user_id = users.id").Joins("left join printers ON user_printers.printer_id = printers.id").Where("user_printers.deleted_at = 0").Order("user_printers.id").Find(&user_printers)
	return user_printers, res.Error
}

func (db *UserPrinterConnection) FindUserPrinterById(id uint) (userPrinterOutput model.SelectUserPrinterParameter, err error) {
	var (
		userPrinter model.SelectUserPrinterParameter
	)

	res := db.connection.Debug().Table("user_printers").Select("user_printers.id, user_printers.printer_code, user_printers.printer_id, printers.printer_name, user_printers.user_id, users.username, user_printers.price, user_printers.warranty, user_printers.remark,	user_printers.created_user_id, user_printers.updated_user_id,	user_printers.deleted_user_id, user_printers.created_at, user_printers.updated_at, user_printers.deleted_at").Joins("left join users ON user_printers.user_id = users.id").Joins("left join printers ON user_printers.printer_id = printers.id").Where("user_printers.id=? AND user_printers.deleted_at = 0", id).Take(&userPrinter)
	return userPrinter, res.Error
}

func (db *UserPrinterConnection) FindExcUserPrinter(id uint) (userPrinterOutput []model.SelectUserPrinterParameter, err error) {
	var (
		user_printers []model.SelectUserPrinterParameter
	)

	res := db.connection.Debug().Table("user_printers").Select("user_printers.id, user_printers.printer_code, user_printers.printer_id, printers.printer_name, user_printers.user_id, users.username, user_printers.price, user_printers.warranty, user_printers.remark,	user_printers.created_user_id, user_printers.updated_user_id,	user_printers.deleted_user_id, user_printers.created_at, user_printers.updated_at, user_printers.deleted_at").Joins("left join users ON user_printers.user_id = users.id").Joins("left join printers ON user_printers.printer_id = printers.id").Where("user_printers.id!=? AND user_printers.deleted_at = 0", id).Find(&user_printers)
	return user_printers, res.Error
}

func (db *UserPrinterConnection) InsertUserPrinter(userPrinter model.UserPrinter) (userPrinterOutput model.UserPrinter, err error) {
	res := db.connection.Save(&userPrinter)
	return userPrinter, res.Error
}

func (db *UserPrinterConnection) UpdateUserPrinter(userPrinter model.UserPrinter, id uint) (userPrinterOutput model.UserPrinter, err error) {
	res := db.connection.Where("id=?", id).Updates(&userPrinter)
	return userPrinter, res.Error
}
