package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type PrinterRepository interface {
	FindPrinters() (printerOutput []model.SelectPrinterParameter, err error)
	FindPrinterById(id uint) (printerOutput model.SelectPrinterParameter, err error)
	FindExcPrinter(id uint) (printerOutput []model.SelectPrinterParameter, err error)
	InsertPrinter(printer model.Printer) (printerOutput model.Printer, err error)
	UpdatePrinter(printer model.Printer, id uint) (printerOutput model.Printer, err error)
}

type PrinterConnection struct {
	connection *gorm.DB
}

func NewPrinterRepository(db *gorm.DB) PrinterRepository {
	return &PrinterConnection{
		connection: db,
	}
}

func (db *PrinterConnection) FindPrinters() (printerOutput []model.SelectPrinterParameter, err error) {
	var (
		printers []model.SelectPrinterParameter
	)

	res := db.connection.Debug().Table("printers").Select("printers.id, printers.printer_name, printers.black_id, A.ink_name AS black_name, printers.cyan_id, B.ink_name AS cyan_name, printers.magenta_id, C.ink_name AS magenta_name, printers.yellow_id, D.ink_name AS yellow_name, printers.color_id, E.ink_name AS color_name, printers.tricolor_id, F.ink_name AS tricolor_name, printers.others_id, G.ink_name AS others_name, printers.remark, printers.created_user_id, printers.updated_user_id, printers.deleted_user_id, printers.created_at, printers.updated_at, printers.deleted_at").Joins("left join inks AS A ON printers.black_id = A.id").Joins("left join inks AS B ON printers.cyan_id = B.id").Joins("left join inks AS C ON printers.magenta_id = C.id").Joins("left join inks AS D ON printers.yellow_id = D.id").Joins("left join inks AS E ON printers.color_id = E.id").Joins("left join inks AS F ON printers.tricolor_id = F.id").Joins("left join inks AS G ON printers.others_id = G.id").Where("printers.deleted_at = 0").Order("printers.id").Find(&printers)
	return printers, res.Error
}

func (db *PrinterConnection) FindPrinterById(id uint) (printerOutput model.SelectPrinterParameter, err error) {
	var (
		printer model.SelectPrinterParameter
	)

	res := db.connection.Debug().Table("printers").Select("printers.id, printers.printer_name, printers.black_id, A.ink_name AS black_name, printers.cyan_id, B.ink_name AS cyan_name, printers.magenta_id, C.ink_name AS magenta_name, printers.yellow_id, D.ink_name AS yellow_name, printers.color_id, E.ink_name AS color_name, printers.tricolor_id, F.ink_name AS tricolor_name, printers.others_id, G.ink_name AS others_name, printers.remark, printers.created_user_id, printers.updated_user_id, printers.deleted_user_id, printers.created_at, printers.updated_at, printers.deleted_at").Joins("left join inks AS A ON printers.black_id = A.id").Joins("left join inks AS B ON printers.cyan_id = B.id").Joins("left join inks AS C ON printers.magenta_id = C.id").Joins("left join inks AS D ON printers.yellow_id = D.id").Joins("left join inks AS E ON printers.color_id = E.id").Joins("left join inks AS F ON printers.tricolor_id = F.id").Joins("left join inks AS G ON printers.others_id = G.id").Where("printers.id=? AND printers.deleted_at = 0", id).Take(&printer)
	return printer, res.Error
}

func (db *PrinterConnection) FindExcPrinter(id uint) (printerOutput []model.SelectPrinterParameter, err error) {
	var (
		printers []model.SelectPrinterParameter
	)

	res := db.connection.Debug().Table("printers").Select("printers.id, printers.printer_name, printers.black_id, A.ink_name AS black_name, printers.cyan_id, B.ink_name AS cyan_name, printers.magenta_id, C.ink_name AS magenta_name, printers.yellow_id, D.ink_name AS yellow_name, printers.color_id, E.ink_name AS color_name, printers.tricolor_id, F.ink_name AS tricolor_name, printers.others_id, G.ink_name AS others_name, printers.remark, printers.created_user_id, printers.updated_user_id, printers.deleted_user_id, printers.created_at, printers.updated_at, printers.deleted_at").Joins("left join inks AS A ON printers.black_id = A.id").Joins("left join inks AS B ON printers.cyan_id = B.id").Joins("left join inks AS C ON printers.magenta_id = C.id").Joins("left join inks AS D ON printers.yellow_id = D.id").Joins("left join inks AS E ON printers.color_id = E.id").Joins("left join inks AS F ON printers.tricolor_id = F.id").Joins("left join inks AS G ON printers.others_id = G.id").Where("printers.id!=? AND printers.deleted_at = 0", id).Order("printers.id").Find(&printers)
	return printers, res.Error
}

func (db *PrinterConnection) InsertPrinter(printer model.Printer) (printerOutput model.Printer, err error) {
	res := db.connection.Save(&printer)
	return printer, res.Error
}

func (db *PrinterConnection) UpdatePrinter(printer model.Printer, id uint) (printerOutput model.Printer, err error) {
	res := db.connection.Where("id=?", id).Updates(&printer)
	return printer, res.Error
}
