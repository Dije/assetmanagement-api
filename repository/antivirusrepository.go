package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type AntivirusRepository interface {
	FindAntiviruss() (antivirusOutput []model.SelectAntivirusParameter, err error)
	FindAntivirusById(id uint) (antivirusOutput model.SelectAntivirusParameter, err error)
	FindExcAntivirus(id uint) (antivirusOutput []model.SelectAntivirusParameter, err error)
	InsertAntivirus(antivirus model.Antivirus) (antivirusOutput model.Antivirus, err error)
	UpdateAntivirus(antivirus model.Antivirus, id uint) (antivirusOutput model.Antivirus, err error)
}

type AntivirusConnection struct {
	connection *gorm.DB
}

func NewAntivirusRepository(db *gorm.DB) AntivirusRepository {
	return &AntivirusConnection{
		connection: db,
	}
}

func (db *AntivirusConnection) FindAntiviruss() (antivirusOutput []model.SelectAntivirusParameter, err error) {
	var (
		antiviri []model.SelectAntivirusParameter
	)

	res := db.connection.Debug().Raw("? UNION ?", db.connection.Table("antiviri").Select("antiviri.id, users.id AS user_id, computers.computer_code AS device, antiviri.license_key, antiviri.activation_id, antiviri.activation_code_old, antiviri.activation_code_new, antiviri.activation_date, antiviri.expiration_date, antiviri.remark, antiviri.created_user_id, antiviri.updated_user_id, antiviri.deleted_user_id, antiviri.created_at, antiviri.updated_at, antiviri.deleted_at").Joins("join computers ON antiviri.device = computers.computer_code").Joins("join users ON computers.user_id = users.id").Where("antiviri.deleted_at = 0"), db.connection.Table("antiviri").Select("antiviri.id, users.id AS user_id, laptops.laptop_code AS device, antiviri.license_key, antiviri.activation_id, antiviri.activation_code_old, antiviri.activation_code_new, antiviri.activation_date, antiviri.expiration_date, antiviri.remark, antiviri.created_user_id, antiviri.updated_user_id, antiviri.deleted_user_id, antiviri.created_at, antiviri.updated_at, antiviri.deleted_at").Joins("join laptops ON antiviri.device = laptops.laptop_code").Joins("join users ON laptops.user_id = users.id").Where("antiviri.deleted_at = 0")).Order("antiviri.device").Find(&antiviri)
	return antiviri, res.Error
}

func (db *AntivirusConnection) FindAntivirusById(id uint) (antivirusOutput model.SelectAntivirusParameter, err error) {
	var (
		antivirus model.SelectAntivirusParameter
	)

	res := db.connection.Debug().Raw("? UNION ?", db.connection.Table("antiviri").Select("antiviri.id, users.id AS user_id, computers.computer_code AS device, antiviri.license_key, antiviri.activation_id, antiviri.activation_code_old, antiviri.activation_code_new, antiviri.activation_date, antiviri.expiration_date, antiviri.remark, antiviri.created_user_id, antiviri.updated_user_id, antiviri.deleted_user_id, antiviri.created_at, antiviri.updated_at, antiviri.deleted_at").Joins("join computers ON antiviri.device = computers.computer_code").Joins("join users ON computers.user_id = users.id").Where("antiviri.id=? AND antiviri.deleted_at = 0", id), db.connection.Table("antiviri").Select("antiviri.id, users.id AS user_id, laptops.laptop_code AS device, antiviri.license_key, antiviri.activation_id, antiviri.activation_code_old, antiviri.activation_code_new, antiviri.activation_date, antiviri.expiration_date, antiviri.remark, antiviri.created_user_id, antiviri.updated_user_id, antiviri.deleted_user_id, antiviri.created_at, antiviri.updated_at, antiviri.deleted_at").Joins("join laptops ON antiviri.device = laptops.laptop_code").Joins("join users ON laptops.user_id = users.id").Where("antiviri.id=? AND antiviri.deleted_at = 0", id)).Take(&antivirus)
	return antivirus, res.Error
}

func (db *AntivirusConnection) FindExcAntivirus(id uint) (antivirusOutput []model.SelectAntivirusParameter, err error) {
	var (
		antiviri []model.SelectAntivirusParameter
	)

	res := db.connection.Debug().Raw("? UNION ?", db.connection.Table("antiviri").Select("antiviri.id, users.id AS user_id, computers.computer_code AS device, antiviri.license_key, antiviri.activation_id, antiviri.activation_code_old, antiviri.activation_code_new, antiviri.activation_date, antiviri.expiration_date, antiviri.remark, antiviri.created_user_id, antiviri.updated_user_id, antiviri.deleted_user_id, antiviri.created_at, antiviri.updated_at, antiviri.deleted_at").Joins("join computers ON antiviri.device = computers.computer_code").Joins("join users ON computers.user_id = users.id").Where("antiviri.id != ? AND antiviri.deleted_at = 0", id), db.connection.Table("antiviri").Select("antiviri.id, users.id AS user_id, laptops.laptop_code AS device, antiviri.license_key, antiviri.activation_id, antiviri.activation_code_old, antiviri.activation_code_new, antiviri.activation_date, antiviri.expiration_date, antiviri.remark, antiviri.created_user_id, antiviri.updated_user_id, antiviri.deleted_user_id, antiviri.created_at, antiviri.updated_at, antiviri.deleted_at").Joins("join laptops ON antiviri.device = laptops.laptop_code").Joins("join users ON laptops.user_id = users.id").Where("antiviri.id != ? AND antiviri.deleted_at = 0", id)).Order("antiviri.device").Find(&antiviri)
	return antiviri, res.Error
}

func (db *AntivirusConnection) InsertAntivirus(antivirus model.Antivirus) (antivirusOutput model.Antivirus, err error) {
	res := db.connection.Save(&antivirus)
	return antivirus, res.Error
}

func (db *AntivirusConnection) UpdateAntivirus(antivirus model.Antivirus, id uint) (antivirusOutput model.Antivirus, err error) {
	res := db.connection.Where("id=?", id).Updates(&antivirus)
	return antivirus, res.Error
}
