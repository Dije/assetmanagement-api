package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type OSRepository interface {
	FindOSs() (osOutput []model.OS, err error)
	FindOSById(id uint) (osOutput model.OS, err error)
	FindExcOS(id uint) (osOutput []model.OS, err error)
	InsertOS(os model.OS) (osOutput model.OS, err error)
	UpdateOS(os model.OS, id uint) (osOutput model.OS, err error)
}

type OSConnection struct {
	connection *gorm.DB
}

func NewOSRepository(db *gorm.DB) OSRepository {
	return &OSConnection{
		connection: db,
	}
}

func (db *OSConnection) FindOSs() (osOutput []model.OS, err error) {
	var (
		oss []model.OS
	)
	res := db.connection.Where("deleted_at = 0").Order("os_name").Find(&oss)
	return oss, res.Error
}

func (db *OSConnection) FindOSById(id uint) (osOutput model.OS, err error) {
	var (
		os model.OS
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&os)
	return os, res.Error
}

func (db *OSConnection) FindExcOS(id uint) (osOutput []model.OS, err error) {
	var (
		oss []model.OS
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("os_name").Find(&oss)
	return oss, res.Error
}

func (db *OSConnection) InsertOS(os model.OS) (osOutput model.OS, err error) {
	res := db.connection.Save(&os)
	return os, res.Error
}

func (db *OSConnection) UpdateOS(os model.OS, id uint) (osOutput model.OS, err error) {
	res := db.connection.Where("id=?", id).Updates(&os)
	return os, res.Error
}
