package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type SubscriptionRepository interface {
	FindSubscriptions() (subscriptionOutput []model.Subscription, err error)
	FindSubscriptionById(id uint) (subscriptionOutput model.Subscription, err error)
	FindExcSubscription(id uint) (subscriptionOutput []model.Subscription, err error)
	InsertSubscription(subscription model.Subscription) (subscriptionOutput model.Subscription, err error)
	UpdateSubscription(subscription model.Subscription, id uint) (subscriptionOutput model.Subscription, err error)
}

type SubscriptionConnection struct {
	connection *gorm.DB
}

func NewSubscriptionRepository(db *gorm.DB) SubscriptionRepository {
	return &SubscriptionConnection{
		connection: db,
	}
}

func (db *SubscriptionConnection) FindSubscriptions() (subscriptionOutput []model.Subscription, err error) {
	var (
		subscriptions []model.Subscription
	)
	res := db.connection.Where("deleted_at = 0").Order("subscription_name").Find(&subscriptions)
	return subscriptions, res.Error
}

func (db *SubscriptionConnection) FindSubscriptionById(id uint) (subscriptionOutput model.Subscription, err error) {
	var (
		subscription model.Subscription
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&subscription)
	return subscription, res.Error
}

func (db *SubscriptionConnection) FindExcSubscription(id uint) (subscriptionOutput []model.Subscription, err error) {
	var (
		subscriptions []model.Subscription
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("subscription_name").Find(&subscriptions)
	return subscriptions, res.Error
}

func (db *SubscriptionConnection) InsertSubscription(subscription model.Subscription) (subscriptionOutput model.Subscription, err error) {
	res := db.connection.Save(&subscription)
	return subscription, res.Error
}

func (db *SubscriptionConnection) UpdateSubscription(subscription model.Subscription, id uint) (subscriptionOutput model.Subscription, err error) {
	res := db.connection.Where("id=?", id).Updates(&subscription)
	return subscription, res.Error
}
