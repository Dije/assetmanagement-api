package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type ComputerRepository interface {
	FindComputers() (computerOutput []model.SelectComputerParameter, err error)
	FindComputerById(id uint) (computerOutput model.SelectComputerParameter, err error)
	InsertComputer(computer model.Computer) (computerOutput model.Computer, err error)
	UpdateComputer(computer model.Computer, id uint) (computerOutput model.Computer, err error)
	DeleteComputer(computer model.Computer, id uint) (computerOutput model.Computer, err error)
}

type ComputerConnection struct {
	connection *gorm.DB
}

func NewComputerRepository(db *gorm.DB) ComputerRepository {
	return &ComputerConnection{
		connection: db,
	}
}

func (db *ComputerConnection) FindComputers() (computerOutput []model.SelectComputerParameter, err error) {
	var (
		computers []model.SelectComputerParameter
	)

	res := db.connection.Debug().Table("computers").Select("computers.id, computers.computer_code, computers.user_id, users.username, computers.processor_id, processors.processor_name, computers.motherboard_id, motherboards.motherboard_name, computers.ram_id, rams.ram_name, computers.hdd_id, hdds.hdd_name, computers.cd, computers.casing_id, casings.casing_name, computers.keyboard_id, keyboards.keyboard_name, computers.mouse_id, mice.mouse_name, computers.monitor_id, monitors.monitor_name, computers.os_id, os.os_name, computers.product_key_os, computers.office_id, offices.office_name, computers.product_key_office, computers.remark, computers.created_user_id, computers.updated_user_id, computers.deleted_user_id, computers.created_at, computers.updated_at, computers.deleted_at").Joins("left join users ON computers.user_id = users.id").Joins("left join processors ON computers.processor_id = processors.id").Joins("left join motherboards ON computers.motherboard_id = motherboards.id").Joins("left join rams ON computers.ram_id = rams.id").Joins("left join hdds ON computers.hdd_id = hdds.id").Joins("left join casings ON computers.casing_id = casings.id").Joins("left join keyboards ON computers.keyboard_id = keyboards.id").Joins("left join mice ON computers.mouse_id = mice.id").Joins("left join monitors ON computers.monitor_id = monitors.id").Joins("left join os ON computers.os_id = os.id").Joins("left join offices ON computers.office_id = offices.id").Where("computers.deleted_at = 0").Order("computers.id").Find(&computers)
	return computers, res.Error
}

func (db *ComputerConnection) FindComputerById(id uint) (computerOutput model.SelectComputerParameter, err error) {
	var (
		computer model.SelectComputerParameter
	)

	res := db.connection.Debug().Table("computers").Select("computers.id, computers.computer_code, computers.user_id, users.username, computers.processor_id, processors.processor_name, computers.motherboard_id, motherboards.motherboard_name, computers.ram_id, rams.ram_name, computers.hdd_id, hdds.hdd_name, computers.cd, computers.casing_id, casings.casing_name, computers.keyboard_id, keyboards.keyboard_name, computers.mouse_id, mice.mouse_name, computers.monitor_id, monitors.monitor_name, computers.os_id, os.os_name, computers.product_key_os, computers.office_id, offices.office_name, computers.product_key_office, computers.remark, computers.created_user_id, computers.updated_user_id, computers.deleted_user_id, computers.created_at, computers.updated_at, computers.deleted_at").Joins("left join users ON computers.user_id = users.id").Joins("left join processors ON computers.processor_id = processors.id").Joins("left join motherboards ON computers.motherboard_id = motherboards.id").Joins("left join rams ON computers.ram_id = rams.id").Joins("left join hdds ON computers.hdd_id = hdds.id").Joins("left join casings ON computers.casing_id = casings.id").Joins("left join keyboards ON computers.keyboard_id = keyboards.id").Joins("left join mice ON computers.mouse_id = mice.id").Joins("left join monitors ON computers.monitor_id = monitors.id").Joins("left join os ON computers.os_id = os.id").Joins("left join offices ON computers.office_id = offices.id").Where("computers.id=? AND computers.deleted_at = 0", id).Take(&computer)
	return computer, res.Error
}

func (db *ComputerConnection) InsertComputer(computer model.Computer) (computerOutput model.Computer, err error) {
	res := db.connection.Save(&computer)
	return computer, res.Error
}

func (db *ComputerConnection) UpdateComputer(computer model.Computer, id uint) (computerOutput model.Computer, err error) {
	res := db.connection.Model(&computer).Where("id=?", id).Updates(map[string]interface{}{"computer_code": computer.ComputerCode, "user_id": computer.UserID, "processor_id": computer.ProcessorID, "motherboard_id": computer.MotherboardID, "ram_id": computer.RAMID, "hdd_id": computer.HDDID, "cd": computer.CD, "casing_id": computer.CasingID, "keyboard_id": computer.KeyboardID, "mouse_id": computer.MouseID, "monitor_id": computer.MonitorID, "os_id": computer.OSID, "product_key_os": computer.ProductKeyOS, "office_id": computer.OfficeID, "product_key_office": computer.ProductKeyOffice, "remark": computer.Remark, "updated_user_id": computer.UpdatedUserID, "updated_at": computer.UpdatedAt})
	return computer, res.Error
}

func (db *ComputerConnection) DeleteComputer(computer model.Computer, id uint) (computerOutput model.Computer, err error) {
	res := db.connection.Model(&computer).Where("id=?", id).Updates(map[string]interface{}{"computer_code": computer.ComputerCode, "deleted_user_id": computer.DeletedUserID, "deleted_at": computer.DeletedAt})
	return computer, res.Error
}
