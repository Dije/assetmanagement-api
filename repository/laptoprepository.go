package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type LaptopRepository interface {
	FindLaptops() (laptopOutput []model.SelectLaptopParameter, err error)
	FindLaptopById(id uint) (laptopOutput model.SelectLaptopParameter, err error)
	FindExcLaptop(id uint) (laptopOutput []model.SelectLaptopParameter, err error)
	InsertLaptop(laptop model.Laptop) (laptopOutput model.Laptop, err error)
	UpdateLaptop(laptop model.Laptop, id uint) (laptopOutput model.Laptop, err error)
	DeleteLaptop(laptop model.Laptop, id uint) (laptopOutput model.Laptop, err error)
}

type LaptopConnection struct {
	connection *gorm.DB
}

func NewLaptopRepository(db *gorm.DB) LaptopRepository {
	return &LaptopConnection{
		connection: db,
	}
}

func (db *LaptopConnection) FindLaptops() (laptopOutput []model.SelectLaptopParameter, err error) {
	var (
		laptops []model.SelectLaptopParameter
	)

	res := db.connection.Debug().Table("laptops").Select("laptops.id, laptops.laptop_code, laptops.user_id, users.username, laptops.processor_id, processors.processor_name, laptops.motherboard_id, motherboards.motherboard_name, laptops.ram_id, rams.ram_name, laptops.hdd_id, hdds.hdd_name, laptops.cd, laptops.monitor_id, monitors.monitor_name, laptops.os_id, os.os_name, laptops.product_key_os, laptops.office_id, offices.office_name, laptops.product_key_office, laptops.remark, laptops.created_user_id, laptops.updated_user_id, laptops.deleted_user_id, laptops.created_at, laptops.updated_at, laptops.deleted_at").Joins("left join users ON laptops.user_id = users.id").Joins("left join processors ON laptops.processor_id = processors.id").Joins("left join motherboards ON laptops.motherboard_id = motherboards.id").Joins("left join rams ON laptops.ram_id = rams.id").Joins("left join hdds ON laptops.hdd_id = hdds.id").Joins("left join monitors ON laptops.monitor_id = monitors.id").Joins("left join os ON laptops.os_id = os.id").Joins("left join offices ON laptops.office_id = offices.id").Where("laptops.deleted_at = 0").Order("laptops.id").Find(&laptops)
	return laptops, res.Error
}

func (db *LaptopConnection) FindLaptopById(id uint) (laptopOutput model.SelectLaptopParameter, err error) {
	var (
		laptop model.SelectLaptopParameter
	)

	res := db.connection.Debug().Table("laptops").Select("laptops.id, laptops.laptop_code, laptops.user_id, users.username, laptops.processor_id, processors.processor_name, laptops.motherboard_id, motherboards.motherboard_name, laptops.ram_id, rams.ram_name, laptops.hdd_id, hdds.hdd_name, laptops.cd, laptops.monitor_id, monitors.monitor_name, laptops.os_id, os.os_name, laptops.product_key_os, laptops.office_id, offices.office_name, laptops.product_key_office, laptops.remark, laptops.created_user_id, laptops.updated_user_id, laptops.deleted_user_id, laptops.created_at, laptops.updated_at, laptops.deleted_at").Joins("left join users ON laptops.user_id = users.id").Joins("left join processors ON laptops.processor_id = processors.id").Joins("left join motherboards ON laptops.motherboard_id = motherboards.id").Joins("left join rams ON laptops.ram_id = rams.id").Joins("left join hdds ON laptops.hdd_id = hdds.id").Joins("left join monitors ON laptops.monitor_id = monitors.id").Joins("left join os ON laptops.os_id = os.id").Joins("left join offices ON laptops.office_id = offices.id").Where("laptops.id=? AND laptops.deleted_at = 0", id).Take(&laptop)
	return laptop, res.Error
}

func (db *LaptopConnection) FindExcLaptop(id uint) (laptopOutput []model.SelectLaptopParameter, err error) {
	var (
		laptop []model.SelectLaptopParameter
	)
	res := db.connection.Debug().Table("laptops").Select("laptops.id, laptops.laptop_code, laptops.user_id, users.username, laptops.processor_id, processors.processor_name, laptops.motherboard_id, motherboards.motherboard_name, laptops.ram_id, rams.ram_name, laptops.hdd_id, hdds.hdd_name, laptops.cd, laptops.monitor_id, monitors.monitor_name, laptops.os_id, os.os_name, laptops.product_key_os, laptops.office_id, offices.office_name, laptops.product_key_office, laptops.remark, laptops.created_user_id, laptops.updated_user_id, laptops.deleted_user_id, laptops.created_at, laptops.updated_at, laptops.deleted_at").Joins("left join users ON laptops.user_id = users.id").Joins("left join processors ON laptops.processor_id = processors.id").Joins("left join motherboards ON laptops.motherboard_id = motherboards.id").Joins("left join rams ON laptops.ram_id = rams.id").Joins("left join hdds ON laptops.hdd_id = hdds.id").Joins("left join monitors ON laptops.monitor_id = monitors.id").Joins("left join os ON laptops.os_id = os.id").Joins("left join offices ON laptops.office_id = offices.id").Where("laptops.id!=? AND laptops.deleted_at = 0", id).Order("office_name").Find(&laptop)
	return laptop, res.Error
}

func (db *LaptopConnection) InsertLaptop(laptop model.Laptop) (laptopOutput model.Laptop, err error) {
	res := db.connection.Save(&laptop)
	return laptop, res.Error
}

func (db *LaptopConnection) UpdateLaptop(laptop model.Laptop, id uint) (laptopOutput model.Laptop, err error) {
	res := db.connection.Model(&laptop).Where("id=?", id).Updates(map[string]interface{}{"laptop_code": laptop.LaptopCode, "user_id": laptop.UserID, "processor_id": laptop.ProcessorID, "motherboard_id": laptop.MotherboardID, "ram_id": laptop.RAMID, "hdd_id": laptop.HDDID, "cd": laptop.CD, "monitor_id": laptop.MonitorID, "os_id": laptop.OSID, "product_key_os": laptop.ProductKeyOS, "office_id": laptop.OfficeID, "product_key_office": laptop.ProductKeyOffice, "remark": laptop.Remark, "updated_user_id": laptop.UpdatedUserID, "updated_at": laptop.UpdatedAt})
	return laptop, res.Error
}

func (db *LaptopConnection) DeleteLaptop(laptop model.Laptop, id uint) (laptopOutput model.Laptop, err error) {
	res := db.connection.Model(&laptop).Where("id=?", id).Updates(map[string]interface{}{"laptop_code": laptop.LaptopCode, "deleted_user_id": laptop.DeletedUserID, "deleted_at": laptop.DeletedAt})
	return laptop, res.Error
}
