package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type UPSRepository interface {
	FindUPSs() (upsOutput []model.SelectUPSParameter, err error)
	FindUPSById(id uint) (upsOutput model.SelectUPSParameter, err error)
	FindExcUPS(id uint) (upsOutput []model.SelectUPSParameter, err error)
	InsertUPS(ups model.UPS) (upsOutput model.UPS, err error)
	UpdateUPS(ups model.UPS, id uint) (upsOutput model.UPS, err error)
}

type UPSConnection struct {
	connection *gorm.DB
}

func NewUPSRepository(db *gorm.DB) UPSRepository {
	return &UPSConnection{
		connection: db,
	}
}

func (db *UPSConnection) FindUPSs() (upsOutput []model.SelectUPSParameter, err error) {
	var (
		ups []model.SelectUPSParameter
	)

	res := db.connection.Debug().Table("ups").Select("ups.id, ups.ups_code, ups.ups_type, ups.ups_vol, ups.supplier, ups.date_bought, ups.price, ups.warranty, ups.user_id, users.username, ups.remark, ups.created_user_id, ups.updated_user_id,	ups.deleted_user_id, ups.created_at, ups.updated_at, ups.deleted_at").Joins("left join users ON ups.user_id = users.id").Where("ups.deleted_at = 0").Order("ups.id").Find(&ups)
	return ups, res.Error
}

func (db *UPSConnection) FindUPSById(id uint) (upsOutput model.SelectUPSParameter, err error) {
	var (
		ups model.SelectUPSParameter
	)

	res := db.connection.Debug().Table("ups").Select("ups.id, ups.ups_code, ups.ups_type, ups.ups_vol, ups.supplier, ups.date_bought, ups.price, ups.warranty, ups.user_id, users.username, ups.remark, ups.created_user_id, ups.updated_user_id,	ups.deleted_user_id, ups.created_at, ups.updated_at, ups.deleted_at").Joins("left join users ON ups.user_id = users.id").Where("ups.id=? AND ups.deleted_at = 0", id).Take(&ups)
	return ups, res.Error
}

func (db *UPSConnection) FindExcUPS(id uint) (upsOutput []model.SelectUPSParameter, err error) {
	var (
		ups []model.SelectUPSParameter
	)

	res := db.connection.Debug().Table("ups").Select("ups.id, ups.ups_code, ups.ups_type, ups.ups_vol, ups.supplier, ups.date_bought, ups.price, ups.warranty, ups.user_id, users.username, ups.remark, ups.created_user_id, ups.updated_user_id,	ups.deleted_user_id, ups.created_at, ups.updated_at, ups.deleted_at").Joins("left join users ON ups.user_id = users.id").Where("ups.id!=? AND ups.deleted_at = 0", id).Order("ups.id").Find(&ups)
	return ups, res.Error
}

func (db *UPSConnection) InsertUPS(ups model.UPS) (upsOutput model.UPS, err error) {
	res := db.connection.Save(&ups)
	return ups, res.Error
}

func (db *UPSConnection) UpdateUPS(ups model.UPS, id uint) (upsOutput model.UPS, err error) {
	res := db.connection.Where("id=?", id).Updates(&ups)
	return ups, res.Error
}
