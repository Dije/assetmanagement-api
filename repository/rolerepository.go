package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type RoleRepository interface {
	FindRoles() (roleOutput []model.Role, err error)
	FindRoleById(id uint) (roleOutput model.Role, err error)
	FindExcRole(id uint) (roleOutput []model.Role, err error)
	InsertRole(role model.Role) (roleOutput model.Role, err error)
	UpdateRole(role model.Role, id uint) (roleOutput model.Role, err error)
}

type RoleConnection struct {
	connection *gorm.DB
}

func NewRoleRepository(db *gorm.DB) RoleRepository {
	return &RoleConnection{
		connection: db,
	}
}

func (db *RoleConnection) FindRoles() (roleOutput []model.Role, err error) {
	var (
		roles []model.Role
	)
	res := db.connection.Where("deleted_at = 0").Order("role_code").Find(&roles)
	return roles, res.Error
}

func (db *RoleConnection) FindRoleById(id uint) (roleOutput model.Role, err error) {
	var (
		role model.Role
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&role)
	return role, res.Error
}

func (db *RoleConnection) FindExcRole(id uint) (roleOutput []model.Role, err error) {
	var (
		roles []model.Role
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("role_code").Find(&roles)
	return roles, res.Error
}

func (db *RoleConnection) InsertRole(role model.Role) (roleOutput model.Role, err error) {
	res := db.connection.Save(&role)
	return role, res.Error
}

func (db *RoleConnection) UpdateRole(role model.Role, id uint) (roleOutput model.Role, err error) {
	res := db.connection.Where("id=?", id).Updates(&role)
	return role, res.Error
}
