package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type KeyboardRepository interface {
	FindKeyboards() (keyboardOutput []model.Keyboard, err error)
	FindKeyboardById(id uint) (keyboardOutput model.Keyboard, err error)
	FindExcKeyboard(id uint) (keyboardOutput []model.Keyboard, err error)
	InsertKeyboard(keyboard model.Keyboard) (keyboardOutput model.Keyboard, err error)
	UpdateKeyboard(keyboard model.Keyboard, id uint) (keyboardOutput model.Keyboard, err error)
}

type KeyboardConnection struct {
	connection *gorm.DB
}

func NewKeyboardRepository(db *gorm.DB) KeyboardRepository {
	return &KeyboardConnection{
		connection: db,
	}
}

func (db *KeyboardConnection) FindKeyboards() (keyboardOutput []model.Keyboard, err error) {
	var (
		keyboards []model.Keyboard
	)
	res := db.connection.Where("deleted_at = 0").Order("keyboard_name").Find(&keyboards)
	return keyboards, res.Error
}

func (db *KeyboardConnection) FindKeyboardById(id uint) (keyboardOutput model.Keyboard, err error) {
	var (
		keyboard model.Keyboard
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&keyboard)
	return keyboard, res.Error
}

func (db *KeyboardConnection) FindExcKeyboard(id uint) (keyboardOutput []model.Keyboard, err error) {
	var (
		keyboards []model.Keyboard
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("keyboard_name").Find(&keyboards)
	return keyboards, res.Error
}

func (db *KeyboardConnection) InsertKeyboard(keyboard model.Keyboard) (keyboardOutput model.Keyboard, err error) {
	res := db.connection.Save(&keyboard)
	return keyboard, res.Error
}

func (db *KeyboardConnection) UpdateKeyboard(keyboard model.Keyboard, id uint) (keyboardOutput model.Keyboard, err error) {
	res := db.connection.Where("id=?", id).Updates(&keyboard)
	return keyboard, res.Error
}
