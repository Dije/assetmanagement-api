package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type HarddiskRepository interface {
	FindHarddisks() (harddiskOutput []model.SelectHarddiskParameter, err error)
	FindHarddiskById(id uint) (harddiskOutput model.SelectHarddiskParameter, err error)
	FindExcHarddisk(id uint) (harddiskOutput []model.SelectHarddiskParameter, err error)
	InsertHarddisk(harddisk model.Harddisk) (harddiskOutput model.Harddisk, err error)
	UpdateHarddisk(harddisk model.Harddisk, id uint) (harddiskOutput model.Harddisk, err error)
}

type HarddiskConnection struct {
	connection *gorm.DB
}

func NewHarddiskRepository(db *gorm.DB) HarddiskRepository {
	return &HarddiskConnection{
		connection: db,
	}
}

func (db *HarddiskConnection) FindHarddisks() (harddiskOutput []model.SelectHarddiskParameter, err error) {
	var (
		harddisks []model.SelectHarddiskParameter
	)

	res := db.connection.Debug().Table("harddisks").Select("harddisks.id, harddisks.harddisk_code, harddisks.merk,  harddisks.type, harddisks.capacity, harddisks.warranty, harddisks.user_id, users.username, harddisks.remark, harddisks.created_user_id, harddisks.updated_user_id, harddisks.deleted_user_id, harddisks.created_at, harddisks.updated_at, harddisks.deleted_at").Joins("left join users ON harddisks.user_id = users.id").Where("harddisks.deleted_at = 0").Order("harddisks.harddisk_code").Find(&harddisks)
	return harddisks, res.Error
}

func (db *HarddiskConnection) FindHarddiskById(id uint) (harddiskOutput model.SelectHarddiskParameter, err error) {
	var (
		harddisk model.SelectHarddiskParameter
	)

	res := db.connection.Debug().Table("harddisks").Select("harddisks.id, harddisks.harddisk_code, harddisks.merk,  harddisks.type, harddisks.capacity, harddisks.warranty, harddisks.user_id, users.username, harddisks.remark, harddisks.created_user_id, harddisks.updated_user_id, harddisks.deleted_user_id, harddisks.created_at, harddisks.updated_at, harddisks.deleted_at").Joins("left join users ON harddisks.user_id = users.id").Where("harddisks.id=? AND harddisks.deleted_at = 0", id).Take(&harddisk)
	return harddisk, res.Error
}

func (db *HarddiskConnection) FindExcHarddisk(id uint) (harddiskOutput []model.SelectHarddiskParameter, err error) {
	var (
		harddisks []model.SelectHarddiskParameter
	)

	res := db.connection.Debug().Table("harddisks").Select("harddisks.id, harddisks.harddisk_code, harddisks.merk,  harddisks.type, harddisks.capacity, harddisks.warranty, harddisks.user_id, users.username, harddisks.remark, harddisks.created_user_id, harddisks.updated_user_id, harddisks.deleted_user_id, harddisks.created_at, harddisks.updated_at, harddisks.deleted_at").Joins("left join users ON harddisks.user_id = users.id").Where("harddisks.id!=? AND harddisks.deleted_at = 0", id).Order("harddisks.harddisk_code").Find(&harddisks)
	return harddisks, res.Error
}

func (db *HarddiskConnection) InsertHarddisk(harddisk model.Harddisk) (harddiskOutput model.Harddisk, err error) {
	res := db.connection.Save(&harddisk)
	return harddisk, res.Error
}

func (db *HarddiskConnection) UpdateHarddisk(harddisk model.Harddisk, id uint) (harddiskOutput model.Harddisk, err error) {
	res := db.connection.Where("id=?", id).Updates(&harddisk)
	return harddisk, res.Error
}
