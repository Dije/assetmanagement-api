package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type MonitorRepository interface {
	FindMonitors() (monitorOutput []model.Monitor, err error)
	FindMonitorById(id uint) (monitorOutput model.Monitor, err error)
	FindExcMonitor(id uint) (monitorOutput []model.Monitor, err error)
	InsertMonitor(monitor model.Monitor) (monitorOutput model.Monitor, err error)
	UpdateMonitor(monitor model.Monitor, id uint) (monitorOutput model.Monitor, err error)
}

type MonitorConnection struct {
	connection *gorm.DB
}

func NewMonitorRepository(db *gorm.DB) MonitorRepository {
	return &MonitorConnection{
		connection: db,
	}
}

func (db *MonitorConnection) FindMonitors() (monitorOutput []model.Monitor, err error) {
	var (
		monitors []model.Monitor
	)
	res := db.connection.Where("deleted_at = 0").Order("monitor_name").Find(&monitors)
	return monitors, res.Error
}

func (db *MonitorConnection) FindMonitorById(id uint) (monitorOutput model.Monitor, err error) {
	var (
		monitor model.Monitor
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&monitor)
	return monitor, res.Error
}

func (db *MonitorConnection) FindExcMonitor(id uint) (monitorOutput []model.Monitor, err error) {
	var (
		monitors []model.Monitor
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("monitor_name").Find(&monitors)
	return monitors, res.Error
}

func (db *MonitorConnection) InsertMonitor(monitor model.Monitor) (monitorOutput model.Monitor, err error) {
	res := db.connection.Save(&monitor)
	return monitor, res.Error
}

func (db *MonitorConnection) UpdateMonitor(monitor model.Monitor, id uint) (monitorOutput model.Monitor, err error) {
	res := db.connection.Where("id=?", id).Updates(&monitor)
	return monitor, res.Error
}
