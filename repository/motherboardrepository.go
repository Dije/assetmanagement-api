package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type MotherboardRepository interface {
	FindMotherboards() (motherboardOutput []model.Motherboard, err error)
	FindMotherboardById(id uint) (motherboardOutput model.Motherboard, err error)
	FindExcMotherboard(id uint) (motherboardOutput []model.Motherboard, err error)
	InsertMotherboard(motherboard model.Motherboard) (motherboardOutput model.Motherboard, err error)
	UpdateMotherboard(motherboard model.Motherboard, id uint) (motherboardOutput model.Motherboard, err error)
}

type MotherboardConnection struct {
	connection *gorm.DB
}

func NewMotherboardRepository(db *gorm.DB) MotherboardRepository {
	return &MotherboardConnection{
		connection: db,
	}
}

func (db *MotherboardConnection) FindMotherboards() (motherboardOutput []model.Motherboard, err error) {
	var (
		motherboards []model.Motherboard
	)
	res := db.connection.Where("deleted_at = 0").Order("motherboard_name").Find(&motherboards)
	return motherboards, res.Error
}

func (db *MotherboardConnection) FindMotherboardById(id uint) (motherboardOutput model.Motherboard, err error) {
	var (
		motherboard model.Motherboard
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&motherboard)
	return motherboard, res.Error
}

func (db *MotherboardConnection) FindExcMotherboard(id uint) (motherboardOutput []model.Motherboard, err error) {
	var (
		motherboards []model.Motherboard
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("motherboard_name").Find(&motherboards)
	return motherboards, res.Error
}

func (db *MotherboardConnection) InsertMotherboard(motherboard model.Motherboard) (motherboardOutput model.Motherboard, err error) {
	res := db.connection.Save(&motherboard)
	return motherboard, res.Error
}

func (db *MotherboardConnection) UpdateMotherboard(motherboard model.Motherboard, id uint) (motherboardOutput model.Motherboard, err error) {
	res := db.connection.Where("id=?", id).Updates(&motherboard)
	return motherboard, res.Error
}
