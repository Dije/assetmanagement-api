package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type HDDRepository interface {
	FindHDDs() (hddOutput []model.HDD, err error)
	FindHDDById(id uint) (hddOutput model.HDD, err error)
	FindExcHDD(id uint) (hddOutput []model.HDD, err error)
	InsertHDD(hdd model.HDD) (hddOutput model.HDD, err error)
	UpdateHDD(hdd model.HDD, id uint) (hddOutput model.HDD, err error)
}

type HDDConnection struct {
	connection *gorm.DB
}

func NewHDDRepository(db *gorm.DB) HDDRepository {
	return &HDDConnection{
		connection: db,
	}
}

func (db *HDDConnection) FindHDDs() (hddOutput []model.HDD, err error) {
	var (
		hdds []model.HDD
	)
	res := db.connection.Where("deleted_at = 0").Order("hdd_name").Find(&hdds)
	return hdds, res.Error
}

func (db *HDDConnection) FindHDDById(id uint) (hddOutput model.HDD, err error) {
	var (
		hdd model.HDD
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&hdd)
	return hdd, res.Error
}

func (db *HDDConnection) FindExcHDD(id uint) (hddOutput []model.HDD, err error) {
	var (
		hdds []model.HDD
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("hdd_name").Find(&hdds)
	return hdds, res.Error
}

func (db *HDDConnection) InsertHDD(hdd model.HDD) (hddOutput model.HDD, err error) {
	res := db.connection.Save(&hdd)
	return hdd, res.Error
}

func (db *HDDConnection) UpdateHDD(hdd model.HDD, id uint) (hddOutput model.HDD, err error) {
	res := db.connection.Where("id=?", id).Updates(&hdd)
	return hdd, res.Error
}
