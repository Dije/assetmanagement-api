package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type MouseRepository interface {
	FindMouses() (mouseOutput []model.Mouse, err error)
	FindMouseById(id uint) (mouseOutput model.Mouse, err error)
	FindExcMouse(id uint) (mouseOutput []model.Mouse, err error)
	InsertMouse(mouse model.Mouse) (mouseOutput model.Mouse, err error)
	UpdateMouse(mouse model.Mouse, id uint) (mouseOutput model.Mouse, err error)
}

type MouseConnection struct {
	connection *gorm.DB
}

func NewMouseRepository(db *gorm.DB) MouseRepository {
	return &MouseConnection{
		connection: db,
	}
}

func (db *MouseConnection) FindMouses() (mouseOutput []model.Mouse, err error) {
	var (
		mouses []model.Mouse
	)
	res := db.connection.Where("deleted_at = 0").Order("mouse_name").Find(&mouses)
	return mouses, res.Error
}

func (db *MouseConnection) FindMouseById(id uint) (mouseOutput model.Mouse, err error) {
	var (
		mouse model.Mouse
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&mouse)
	return mouse, res.Error
}

func (db *MouseConnection) FindExcMouse(id uint) (mouseOutput []model.Mouse, err error) {
	var (
		mouses []model.Mouse
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("mouse_name").Find(&mouses)
	return mouses, res.Error
}

func (db *MouseConnection) InsertMouse(mouse model.Mouse) (mouseOutput model.Mouse, err error) {
	res := db.connection.Save(&mouse)
	return mouse, res.Error
}

func (db *MouseConnection) UpdateMouse(mouse model.Mouse, id uint) (mouseOutput model.Mouse, err error) {
	res := db.connection.Where("id=?", id).Updates(&mouse)
	return mouse, res.Error
}
