package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type OfficeRepository interface {
	FindOffices() (officeOutput []model.Office, err error)
	FindOfficeById(id uint) (officeOutput model.Office, err error)
	FindExcOffice(id uint) (officeOutput []model.Office, err error)
	InsertOffice(office model.Office) (officeOutput model.Office, err error)
	UpdateOffice(office model.Office, id uint) (officeOutput model.Office, err error)
}

type OfficeConnection struct {
	connection *gorm.DB
}

func NewOfficeRepository(db *gorm.DB) OfficeRepository {
	return &OfficeConnection{
		connection: db,
	}
}

func (db *OfficeConnection) FindOffices() (officeOutput []model.Office, err error) {
	var (
		offices []model.Office
	)
	res := db.connection.Where("deleted_at = 0").Order("office_name").Find(&offices)
	return offices, res.Error
}

func (db *OfficeConnection) FindOfficeById(id uint) (officeOutput model.Office, err error) {
	var (
		office model.Office
	)
	res := db.connection.Where("id=? AND deleted_at = 0", id).Take(&office)
	return office, res.Error
}

func (db *OfficeConnection) FindExcOffice(id uint) (officeOutput []model.Office, err error) {
	var (
		offices []model.Office
	)
	res := db.connection.Where("id!=? AND deleted_at = 0", id).Order("office_name").Find(&offices)
	return offices, res.Error
}

func (db *OfficeConnection) InsertOffice(office model.Office) (officeOutput model.Office, err error) {
	res := db.connection.Save(&office)
	return office, res.Error
}

func (db *OfficeConnection) UpdateOffice(office model.Office, id uint) (officeOutput model.Office, err error) {
	res := db.connection.Where("id=?", id).Updates(&office)
	return office, res.Error
}
