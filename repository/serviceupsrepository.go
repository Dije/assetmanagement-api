package repository

import (
	"deliportal-api/model"

	"gorm.io/gorm"
)

type ServiceUPSRepository interface {
	FindServiceUPSs() (serviceUPSOutput []model.SelectServiceUPSParameter, err error)
	FindServiceUPSById(id uint) (serviceUPSOutput model.SelectServiceUPSParameter, err error)
	FindExcServiceUPS(id uint) (serviceUPSOutput []model.SelectServiceUPSParameter, err error)
	InsertServiceUPS(serviceUPS model.ServiceUPS) (serviceUPSOutput model.ServiceUPS, err error)
	UpdateServiceUPS(serviceUPS model.ServiceUPS, id uint) (serviceUPSOutput model.ServiceUPS, err error)
}

type ServiceUPSConnection struct {
	connection *gorm.DB
}

func NewServiceUPSRepository(db *gorm.DB) ServiceUPSRepository {
	return &ServiceUPSConnection{
		connection: db,
	}
}

func (db *ServiceUPSConnection) FindServiceUPSs() (serviceUPSOutput []model.SelectServiceUPSParameter, err error) {
	var (
		serviceUPSs []model.SelectServiceUPSParameter
	)
	res := db.connection.Debug().Table("service_ups").Select("service_ups.id, service_ups.ups_id, ups.ups_code, service_ups.date_service, service_ups.date_done, service_ups.company, service_ups.problem, service_ups.price, service_ups.solved, service_ups.remark, service_ups.created_user_id, service_ups.updated_user_id, service_ups.deleted_user_id, service_ups.created_at, service_ups.updated_at, service_ups.deleted_at").Joins("left join ups ON service_ups.ups_id = ups.id").Where("service_ups.deleted_at = 0").Order("service_ups.id").Find(&serviceUPSs)
	return serviceUPSs, res.Error
}

func (db *ServiceUPSConnection) FindServiceUPSById(id uint) (serviceUPSOutput model.SelectServiceUPSParameter, err error) {
	var (
		serviceUPS model.SelectServiceUPSParameter
	)
	res := db.connection.Debug().Table("service_ups").Select("service_ups.id, service_ups.ups_id, ups.ups_code, service_ups.date_service, service_ups.date_done, service_ups.company, service_ups.problem, service_ups.price, service_ups.solved, service_ups.remark, service_ups.created_user_id, service_ups.updated_user_id, service_ups.deleted_user_id, service_ups.created_at, service_ups.updated_at, service_ups.deleted_at").Joins("left join ups ON service_ups.ups_id = ups.id").Where("service_ups.id=? AND service_ups.deleted_at = 0", id).Take(&serviceUPS)
	return serviceUPS, res.Error
}

func (db *ServiceUPSConnection) FindExcServiceUPS(id uint) (serviceUPSOutput []model.SelectServiceUPSParameter, err error) {
	var (
		serviceUPSs []model.SelectServiceUPSParameter
	)
	res := db.connection.Debug().Table("service_ups").Select("service_ups.id, service_ups.ups_id, ups.ups_code, service_ups.date_service, service_ups.date_done, service_ups.company, service_ups.problem, service_ups.price, service_ups.solved, service_ups.remark, service_ups.created_user_id, service_ups.updated_user_id, service_ups.deleted_user_id, service_ups.created_at, service_ups.updated_at, service_ups.deleted_at").Joins("left join ups ON service_ups.ups_id = ups.id").Where("service_ups.id!=? AND service_ups.deleted_at = 0", id).Order("id").Find(&serviceUPSs)
	return serviceUPSs, res.Error
}

func (db *ServiceUPSConnection) InsertServiceUPS(serviceUPS model.ServiceUPS) (serviceUPSOutput model.ServiceUPS, err error) {
	res := db.connection.Save(&serviceUPS)
	return serviceUPS, res.Error
}

func (db *ServiceUPSConnection) UpdateServiceUPS(serviceUPS model.ServiceUPS, id uint) (serviceUPSOutput model.ServiceUPS, err error) {
	res := db.connection.Where("id=?", id).Updates(&serviceUPS)
	return serviceUPS, res.Error
}
