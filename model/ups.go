package model

type UPS struct {
	ID            uint    `json:"ups_id"`
	UPSCode       string  `gorm:"type:varchar(50);not null;unique" json:"ups_code"`
	UPSType       string  `gorm:"type:varchar(50);not null" json:"ups_type"`
	UPSVol        string  `gorm:"type:varchar(50);not null" json:"ups_vol"`
	Supplier      string  `gorm:"type:varchar(50);not null" json:"supplier"`
	DateBought    uint    `gorm:"type:bigint;typedefault:null" json:"date_bought"`
	Price         uint    `gorm:"type:bigint" json:"price"`
	Warranty      string  `gorm:"type:varchar(50)" json:"warranty"`
	UserID        uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"user_id"`
	Remark        string  `gorm:"type:varchar(200)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type SelectUPSParameter struct {
	ID            uint    `json:"ups_id"`
	UPSCode       string  `gorm:"type:varchar(50);not null;unique" json:"ups_code"`
	UPSType       string  `gorm:"type:varchar(50);not null" json:"ups_type"`
	UPSVol        string  `gorm:"type:varchar(50);not null" json:"ups_vol"`
	Supplier      string  `gorm:"type:varchar(50);not null" json:"supplier"`
	DateBought    uint    `gorm:"type:bigint;typedefault:null" json:"date_bought"`
	Price         uint    `gorm:"type:bigint" json:"price"`
	Warranty      string  `gorm:"type:varchar(50)" json:"warranty"`
	UserID        uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"user_id"`
	Username      string  `gorm:"type:varchar(50);not null;unique" json:"username"`
	Remark        string  `gorm:"type:varchar(200)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type CreateUPSParameter struct {
	UPSCode       string  `gorm:"type:varchar(50);not null;unique" json:"ups_code"`
	UPSType       string  `gorm:"type:varchar(50);not null" json:"ups_type"`
	UPSVol        string  `gorm:"type:varchar(50);not null" json:"ups_vol"`
	Supplier      string  `gorm:"type:varchar(50);not null" json:"supplier"`
	DateBought    uint    `gorm:"type:bigint;typedefault:null" json:"date_bought"`
	Price         uint    `gorm:"type:bigint" json:"price"`
	Warranty      string  `gorm:"type:varchar(50)" json:"warranty"`
	UserID        uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"user_id"`
	Remark        string  `gorm:"type:varchar(200)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}
