package model

type Harddisk struct {
	ID            uint    `json:"harddisk_id"`
	HarddiskCode  string  `gorm:"type:varchar(50);not null" json:"harddisk_code"`
	Merk          string  `gorm:"type:varchar(50);not null" json:"merk"`
	Type          string  `gorm:"type:varchar(50);not null" json:"type"`
	Capacity      string  `gorm:"type:varchar(50);not null" json:"capacity"`
	Warranty      string  `gorm:"type:varchar(50);not null" json:"warranty"`
	UserID        uint    `gorm:"type:bigint;foreign_key;index:" json:"user_id"`
	Remark        string  `gorm:"type:varchar(200)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type SelectHarddiskParameter struct {
	ID            uint    `json:"harddisk_id"`
	HarddiskCode  string  `gorm:"type:varchar(50);not null" json:"harddisk_code"`
	Merk          string  `gorm:"type:varchar(50);not null" json:"merk"`
	Type          string  `gorm:"type:varchar(50);not null" json:"type"`
	Capacity      string  `gorm:"type:varchar(50);not null" json:"capacity"`
	Warranty      string  `gorm:"type:varchar(50);not null" json:"warranty"`
	UserID        uint    `gorm:"type:bigint;foreign_key;index:" json:"user_id"`
	Username      string  `gorm:"type:varchar(50);not null;unique" json:"username"`
	Remark        string  `gorm:"type:varchar(200)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type CreateHarddiskParameter struct {
	HarddiskCode  string  `gorm:"type:varchar(50);not null" json:"harddisk_code"`
	Merk          string  `gorm:"type:varchar(50);not null" json:"merk"`
	Type          string  `gorm:"type:varchar(50);not null" json:"type"`
	Capacity      string  `gorm:"type:varchar(50);not null" json:"capacity"`
	Warranty      string  `gorm:"type:varchar(50);not null" json:"warranty"`
	UserID        uint    `gorm:"type:bigint;foreign_key;index:" json:"user_id"`
	Remark        string  `gorm:"type:varchar(200)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}
