package model

type ServiceUPS struct {
	ID            uint    `json:"service_ups_id"`
	UPSID         uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"ups_id"`
	DateService   uint    `gorm:"type:bigint;not null" json:"date_service"`
	DateDone      uint    `gorm:"type:bigint;not null" json:"date_done"`
	Company       string  `gorm:"type:varchar(50)" json:"company"`
	Problem       string  `gorm:"type:varchar(500)" json:"problem"`
	Price         uint    `gorm:"type:bigint" json:"price"`
	Solved        string  `gorm:"type:varchar(500)" json:"solved"`
	Remark        string  `gorm:"type:varchar(500)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type SelectServiceUPSParameter struct {
	ID            uint    `json:"service_ups_id"`
	UPSID         uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"ups_id"`
	UPSCode       string  `gorm:"type:varchar(50)" json:"ups_code"`
	DateService   uint    `gorm:"type:bigint;not null" json:"date_service"`
	DateDone      uint    `gorm:"type:bigint;not null" json:"date_done"`
	Company       string  `gorm:"type:varchar(50)" json:"company"`
	Problem       string  `gorm:"type:varchar(500)" json:"problem"`
	Price         uint    `gorm:"type:bigint" json:"price"`
	Solved        string  `gorm:"type:varchar(500)" json:"solved"`
	Remark        string  `gorm:"type:varchar(500)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type CreateServiceUPSParameter struct {
	UPSID         uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"ups_id"`
	DateService   uint    `gorm:"type:bigint;not null" json:"date_service"`
	DateDone      uint    `gorm:"type:bigint;not null" json:"date_done"`
	Company       string  `gorm:"type:varchar(50)" json:"company"`
	Problem       string  `gorm:"type:varchar(500)" json:"problem"`
	Price         uint    `gorm:"type:bigint" json:"price"`
	Solved        string  `gorm:"type:varchar(500)" json:"solved"`
	Remark        string  `gorm:"type:varchar(500)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}
