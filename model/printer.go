package model

type Printer struct {
	ID            uint    `json:"printer_id"`
	PrinterName   string  `gorm:"type:varchar(50);not null;unique" json:"printer_name"`
	BlackID       uint    `gorm:"type:bigint;foreign_key" json:"black_id"`
	CyanID        uint    `gorm:"type:bigint;foreign_key" json:"cyan_id"`
	MagentaID     uint    `gorm:"type:bigint;foreign_key" json:"magenta_id"`
	YellowID      uint    `gorm:"type:bigint;foreign_key" json:"yellow_id"`
	ColorID       uint    `gorm:"type:bigint;foreign_key" json:"color_id"`
	TricolorID    uint    `gorm:"type:bigint;foreign_key" json:"tricolor_id"`
	OthersID      uint    `gorm:"type:bigint;foreign_key" json:"others_id"`
	Remark        string  `gorm:"type:varchar(200)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type SelectPrinterParameter struct {
	ID            uint    `json:"printer_id"`
	PrinterName   string  `gorm:"type:varchar(50);not null;unique" json:"printer_name"`
	BlackID       uint    `gorm:"type:bigint;foreign_key" json:"black_id"`
	BlackName     string  `gorm:"type:varchar(50);not null;unique" json:"black_name"`
	CyanID        uint    `gorm:"type:bigint;foreign_key" json:"cyan_id"`
	CyanName      string  `gorm:"type:varchar(50);not null;unique" json:"cyan_name"`
	MagentaID     uint    `gorm:"type:bigint;foreign_key" json:"magenta_id"`
	MagentaName   string  `gorm:"type:varchar(50);not null;unique" json:"magenta_name"`
	YellowID      uint    `gorm:"type:bigint;foreign_key" json:"yellow_id"`
	YellowName    string  `gorm:"type:varchar(50);not null;unique" json:"yellow_name"`
	ColorID       uint    `gorm:"type:bigint;foreign_key" json:"color_id"`
	ColorName     string  `gorm:"type:varchar(50);not null;unique" json:"color_name"`
	TricolorID    uint    `gorm:"type:bigint;foreign_key" json:"tricolor_id"`
	TricolorName  string  `gorm:"type:varchar(50);not null;unique" json:"tricolor_name"`
	OthersID      uint    `gorm:"type:bigint;foreign_key" json:"others_id"`
	OthersName    string  `gorm:"type:varchar(50);not null;unique" json:"others_name"`
	Remark        string  `gorm:"type:varchar(200)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type CreatePrinterParameter struct {
	PrinterName   string  `gorm:"type:varchar(50);not null;unique" json:"printer_name"`
	BlackID       uint    `gorm:"type:bigint;foreign_key" json:"black_id"`
	CyanID        uint    `gorm:"type:bigint;foreign_key" json:"cyan_id"`
	MagentaID     uint    `gorm:"type:bigint;foreign_key" json:"magenta_id"`
	YellowID      uint    `gorm:"type:bigint;foreign_key" json:"yellow_id"`
	ColorID       uint    `gorm:"type:bigint;foreign_key" json:"color_id"`
	TricolorID    uint    `gorm:"type:bigint;foreign_key" json:"tricolor_id"`
	OthersID      uint    `gorm:"type:bigint;foreign_key" json:"others_id"`
	Remark        string  `gorm:"type:varchar(200)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}
