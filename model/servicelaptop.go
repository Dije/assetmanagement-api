package model

type ServiceLaptop struct {
	ID            uint    `json:"service_laptop_id"`
	LaptopID      uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"laptop_id"`
	DateService   uint    `gorm:"type:bigint;not null" json:"date_service"`
	DateDone      uint    `gorm:"type:bigint;not null" json:"date_done"`
	Company       string  `gorm:"type:varchar(50)" json:"company"`
	Problem       string  `gorm:"type:varchar(500)" json:"problem"`
	Price         uint    `gorm:"type:bigint" json:"price"`
	Solved        string  `gorm:"type:varchar(500)" json:"solved"`
	Remark        string  `gorm:"type:varchar(500)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type SelectServiceLaptopParameter struct {
	ID            uint    `json:"service_laptop_id"`
	LaptopID      uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"laptop_id"`
	LaptopCode    string  `gorm:"type:varchar(50)" json:"laptop_code"`
	DateService   uint    `gorm:"type:bigint;not null" json:"date_service"`
	DateDone      uint    `gorm:"type:bigint;not null" json:"date_done"`
	Company       string  `gorm:"type:varchar(50)" json:"company"`
	Problem       string  `gorm:"type:varchar(500)" json:"problem"`
	Price         uint    `gorm:"type:bigint" json:"price"`
	Solved        string  `gorm:"type:varchar(500)" json:"solved"`
	Remark        string  `gorm:"type:varchar(500)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type CreateServiceLaptopParameter struct {
	LaptopID      uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"laptop_id"`
	DateService   uint    `gorm:"type:bigint;not null" json:"date_service"`
	DateDone      uint    `gorm:"type:bigint;not null" json:"date_done"`
	Company       string  `gorm:"type:varchar(50)" json:"company"`
	Problem       string  `gorm:"type:varchar(500)" json:"problem"`
	Price         uint    `gorm:"type:bigint" json:"price"`
	Solved        string  `gorm:"type:varchar(500)" json:"solved"`
	Remark        string  `gorm:"type:varchar(500)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}
