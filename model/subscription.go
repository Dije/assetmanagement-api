package model

type Subscription struct {
	ID               uint    `json:"subscription_id"`
	SubscriptionName string  `gorm:"type:varchar(100);not null" json:"subscription_name"`
	DateStart        uint    `gorm:"type:bigint;typedefault:null" json:"date_start"`
	EndDate          uint    `gorm:"type:bigint;typedefault:null" json:"end_date"`
	Price            uint    `gorm:"type:bigint" json:"price"`
	Remark           string  `gorm:"type:varchar(500)" json:"remark"`
	CreatedUserID    uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID    uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID    uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt        float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt        float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt        float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type CreateSubscriptionParameter struct {
	SubscriptionName string  `gorm:"type:varchar(100);not null" json:"subscription_name"`
	DateStart        uint    `gorm:"type:bigint;typedefault:null" json:"date_start"`
	EndDate          uint    `gorm:"type:bigint;typedefault:null" json:"end_date"`
	Price            uint    `gorm:"type:bigint" json:"price"`
	Remark           string  `gorm:"type:varchar(500)" json:"remark"`
	CreatedUserID    uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID    uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID    uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt        float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt        float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt        float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}
