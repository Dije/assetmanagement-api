package model

type UserPrinter struct {
	ID            uint    `json:"user_printer_id"`
	PrinterCode   string  `gorm:"type:varchar(50);not null;unique" json:"printer_code"`
	PrinterID     uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"printer_id"`
	UserID        uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"user_id"`
	Price         uint    `gorm:"type:bigint" json:"price"`
	Warranty      string  `gorm:"type:varchar(50)" json:"warranty"`
	Remark        string  `gorm:"type:varchar(200)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type SelectUserPrinterParameter struct {
	ID            uint    `json:"user_printer_id"`
	PrinterCode   string  `gorm:"type:varchar(50);not null;unique" json:"printer_code"`
	PrinterID     uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"printer_id"`
	PrinterName   string  `gorm:"type:varchar(50);not null;unique" json:"printer_name"`
	UserID        uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"user_id"`
	Username      string  `gorm:"type:varchar(50);not null;unique" json:"username"`
	Price         uint    `gorm:"type:bigint" json:"price"`
	Warranty      string  `gorm:"type:varchar(50)" json:"warranty"`
	Remark        string  `gorm:"type:varchar(200)" json:"remark"`
	CreatedUserId uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserId uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserId uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type CreateUserPrinterParameter struct {
	PrinterCode   string  `gorm:"type:varchar(50);not null;unique" json:"printer_code"`
	PrinterID     uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"printer_id"`
	UserID        uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"user_id"`
	Price         uint    `gorm:"type:bigint" json:"price"`
	Warranty      string  `gorm:"type:varchar(50)" json:"warranty"`
	Remark        string  `gorm:"type:varchar(200)" json:"remark"`
	CreatedUserID uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt     float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt     float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt     float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}
