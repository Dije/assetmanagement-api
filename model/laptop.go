package model

type Laptop struct {
	ID               uint    `json:"laptop_id"`
	LaptopCode       string  `gorm:"type:varchar(50)" json:"laptop_code"`
	UserID           uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"user_id"`
	ProcessorID      uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"processor_id"`
	MotherboardID    uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"motherboard_id"`
	RAMID            uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"ram_id"`
	HDDID            uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"hdd_id"`
	CD               bool    `gorm:"not null;typedefault:null" json:"cd"`
	MonitorID        uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"monitor_id"`
	OSID             uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"os_id"`
	ProductKeyOS     string  `gorm:"type:varchar(50)" json:"product_key_os"`
	OfficeID         uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"office_id"`
	ProductKeyOffice string  `gorm:"type:varchar(50)" json:"product_key_office"`
	Remark           string  `gorm:"type:varchar(500)" json:"remark"`
	CreatedUserID    uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID    uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID    uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt        float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt        float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt        float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type SelectLaptopParameter struct {
	ID               uint    `json:"laptop_id"`
	LaptopCode       string  `gorm:"type:varchar(50)" json:"laptop_code"`
	UserID           uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"user_id"`
	Username         string  `gorm:"type:varchar(50);not null;unique" json:"username"`
	ProcessorID      uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"processor_id"`
	ProcessorName    string  `gorm:"type:varchar(50);not null;unique" json:"processor_name"`
	MotherboardID    uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"motherboard_id"`
	MotherboardName  string  `gorm:"type:varchar(50);not null;unique" json:"motherboard_name"`
	RAMID            uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"ram_id"`
	RAMName          string  `gorm:"type:varchar(50);not null;unique" json:"ram_name"`
	HDDID            uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"hdd_id"`
	HDDName          string  `gorm:"type:varchar(50);not null;unique" json:"hdd_name"`
	CD               bool    `gorm:"not null;typedefault:null" json:"cd"`
	MonitorID        uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"monitor_id"`
	MonitorName      string  `gorm:"type:varchar(50);not null;unique" json:"monitor_name"`
	OSID             uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"os_id"`
	OSName           string  `gorm:"type:varchar(50);not null;unique" json:"os_name"`
	ProductKeyOS     string  `gorm:"type:varchar(50)" json:"product_key_os"`
	OfficeID         uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"office_id"`
	OfficeName       string  `gorm:"type:varchar(50);not null;unique" json:"office_name"`
	ProductKeyOffice string  `gorm:"type:varchar(50)" json:"product_key_office"`
	Remark           string  `gorm:"type:varchar(500)" json:"remark"`
	CreatedUserID    uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID    uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID    uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt        float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt        float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt        float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}

type CreateLaptopParameter struct {
	LaptopCode       string  `gorm:"type:varchar(50)" json:"laptop_code"`
	UserID           uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"user_id"`
	ProcessorID      uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"processor_id"`
	MotherboardID    uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"motherboard_id"`
	RAMID            uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"ram_id"`
	HDDID            uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"hdd_id"`
	CD               bool    `gorm:"not null;typedefault:null" json:"cd"`
	MonitorID        uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"monitor_id"`
	OSID             uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"os_id"`
	ProductKeyOS     string  `gorm:"type:varchar(50)" json:"product_key_os"`
	OfficeID         uint    `gorm:"type:bigint;foreign_key;not null;index:" json:"office_id"`
	ProductKeyOffice string  `gorm:"type:varchar(50)" json:"product_key_office"`
	Remark           string  `gorm:"type:varchar(500)" json:"remark"`
	CreatedUserID    uint    `gorm:"type:bigint;not null" json:"created_user_id"`
	UpdatedUserID    uint    `gorm:"type:bigint;not null" json:"updated_user_id"`
	DeletedUserID    uint    `gorm:"type:bigint;typedefault:null" json:"deleted_user_id"`
	CreatedAt        float64 `gorm:"type:double precision;not null" json:"created_at"`
	UpdatedAt        float64 `gorm:"type:double precision;not null" json:"updated_at"`
	DeletedAt        float64 `gorm:"type:double precision;typedefault:null" json:"deleted_at"`
}
