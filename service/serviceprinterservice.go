package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type ServicePrinterService interface {
	FindServicePrinters() (servicePrinterOutput []model.SelectServicePrinterParameter, err error)
	FindServicePrinterById(id uint) (servicePrinterOutput model.SelectServicePrinterParameter, err error)
	FindExcServicePrinter(id uint) (servicePrinterOutput []model.SelectServicePrinterParameter, err error)
	InsertServicePrinter(servicePrinter model.CreateServicePrinterParameter) (servicePrinterOutput model.ServicePrinter, err error)
	UpdateServicePrinter(servicePrinter model.ServicePrinter, id uint) (servicePrinterOutput model.ServicePrinter, err error)
	DeleteServicePrinter(servicePrinter model.ServicePrinter, id uint) (servicePrinterOutput model.ServicePrinter, err error)
}

type servicePrinterService struct {
	servicePrinterRepository repository.ServicePrinterRepository
}

func NewServicePrinterService(servicePrinterRep repository.ServicePrinterRepository) ServicePrinterService {
	return &servicePrinterService{
		servicePrinterRepository: servicePrinterRep,
	}
}

func (service *servicePrinterService) FindServicePrinters() (servicePrinterOutput []model.SelectServicePrinterParameter, err error) {
	res, err := service.servicePrinterRepository.FindServicePrinters()
	return res, err
}

func (service *servicePrinterService) FindServicePrinterById(id uint) (servicePrinterOutput model.SelectServicePrinterParameter, err error) {
	res, err := service.servicePrinterRepository.FindServicePrinterById(id)
	return res, err
}

func (service *servicePrinterService) FindExcServicePrinter(id uint) (servicePrinterOutput []model.SelectServicePrinterParameter, err error) {
	res, err := service.servicePrinterRepository.FindExcServicePrinter(id)
	return res, err
}

func (service *servicePrinterService) InsertServicePrinter(servicePrinter model.CreateServicePrinterParameter) (servicePrinterOutput model.ServicePrinter, err error) {
	newServicePrinter := model.ServicePrinter{}
	err1 := smapping.FillStruct(&newServicePrinter, smapping.MapFields(&servicePrinter))
	if err != nil {
		return newServicePrinter, err1
	}
	res, err := service.servicePrinterRepository.InsertServicePrinter(newServicePrinter)
	return res, err
}

func (service *servicePrinterService) UpdateServicePrinter(servicePrinter model.ServicePrinter, id uint) (servicePrinterOutput model.ServicePrinter, err error) {
	newServicePrinter := model.ServicePrinter{}
	err1 := smapping.FillStruct(&newServicePrinter, smapping.MapFields(&servicePrinter))
	if err != nil {
		return newServicePrinter, err1
	}
	res, err := service.servicePrinterRepository.UpdateServicePrinter(newServicePrinter, id)
	return res, err
}

func (service *servicePrinterService) DeleteServicePrinter(servicePrinter model.ServicePrinter, id uint) (servicePrinterOutput model.ServicePrinter, err error) {
	newServicePrinter := model.ServicePrinter{}
	err1 := smapping.FillStruct(&newServicePrinter, smapping.MapFields(&servicePrinter))
	if err != nil {
		return newServicePrinter, err1
	}
	res, err := service.servicePrinterRepository.UpdateServicePrinter(newServicePrinter, id)
	return res, err
}
