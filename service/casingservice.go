package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type CasingService interface {
	FindCasings() (casingOutput []model.Casing, err error)
	FindCasingById(id uint) (casingOutput model.Casing, err error)
	FindExcCasing(id uint) (casingOutput []model.Casing, err error)
	InsertCasing(casing model.CreateCasingParameter) (casingOutput model.Casing, err error)
	UpdateCasing(casing model.Casing, id uint) (casingOutput model.Casing, err error)
	DeleteCasing(casing model.Casing, id uint) (casingOutput model.Casing, err error)
}

type casingService struct {
	casingRepository repository.CasingRepository
}

func NewCasingService(casingRep repository.CasingRepository) CasingService {
	return &casingService{
		casingRepository: casingRep,
	}
}

func (service *casingService) FindCasings() (casingOutput []model.Casing, err error) {
	res, err := service.casingRepository.FindCasings()
	return res, err
}

func (service *casingService) FindCasingById(id uint) (casingOutput model.Casing, err error) {
	res, err := service.casingRepository.FindCasingById(id)
	return res, err
}

func (service *casingService) FindExcCasing(id uint) (casingOutput []model.Casing, err error) {
	res, err := service.casingRepository.FindExcCasing(id)
	return res, err
}

func (service *casingService) InsertCasing(casing model.CreateCasingParameter) (casingOutput model.Casing, err error) {
	newCasing := model.Casing{}
	err1 := smapping.FillStruct(&newCasing, smapping.MapFields(&casing))
	if err != nil {
		return newCasing, err1
	}
	res, err := service.casingRepository.InsertCasing(newCasing)
	return res, err
}

func (service *casingService) UpdateCasing(casing model.Casing, id uint) (casingOutput model.Casing, err error) {
	newCasing := model.Casing{}
	err1 := smapping.FillStruct(&newCasing, smapping.MapFields(&casing))
	if err != nil {
		return newCasing, err1
	}
	res, err := service.casingRepository.UpdateCasing(newCasing, id)
	return res, err
}

func (service *casingService) DeleteCasing(casing model.Casing, id uint) (casingOutput model.Casing, err error) {
	newCasing := model.Casing{}
	err1 := smapping.FillStruct(&newCasing, smapping.MapFields(&casing))
	if err != nil {
		return newCasing, err1
	}
	res, err := service.casingRepository.UpdateCasing(newCasing, id)
	return res, err
}
