package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type KeyboardService interface {
	FindKeyboards() (keyboardOutput []model.Keyboard, err error)
	FindKeyboardById(id uint) (keyboardOutput model.Keyboard, err error)
	FindExcKeyboard(id uint) (keyboardOutput []model.Keyboard, err error)
	InsertKeyboard(keyboard model.CreateKeyboardParameter) (keyboardOutput model.Keyboard, err error)
	UpdateKeyboard(keyboard model.Keyboard, id uint) (keyboardOutput model.Keyboard, err error)
	DeleteKeyboard(keyboard model.Keyboard, id uint) (keyboardOutput model.Keyboard, err error)
}

type keyboardService struct {
	keyboardRepository repository.KeyboardRepository
}

func NewKeyboardService(keyboardRep repository.KeyboardRepository) KeyboardService {
	return &keyboardService{
		keyboardRepository: keyboardRep,
	}
}

func (service *keyboardService) FindKeyboards() (keyboardOutput []model.Keyboard, err error) {
	res, err := service.keyboardRepository.FindKeyboards()
	return res, err
}

func (service *keyboardService) FindKeyboardById(id uint) (keyboardOutput model.Keyboard, err error) {
	res, err := service.keyboardRepository.FindKeyboardById(id)
	return res, err
}

func (service *keyboardService) FindExcKeyboard(id uint) (keyboardOutput []model.Keyboard, err error) {
	res, err := service.keyboardRepository.FindExcKeyboard(id)
	return res, err
}

func (service *keyboardService) InsertKeyboard(keyboard model.CreateKeyboardParameter) (keyboardOutput model.Keyboard, err error) {
	newKeyboard := model.Keyboard{}
	err1 := smapping.FillStruct(&newKeyboard, smapping.MapFields(&keyboard))
	if err != nil {
		return newKeyboard, err1
	}
	res, err := service.keyboardRepository.InsertKeyboard(newKeyboard)
	return res, err
}

func (service *keyboardService) UpdateKeyboard(keyboard model.Keyboard, id uint) (keyboardOutput model.Keyboard, err error) {
	newKeyboard := model.Keyboard{}
	err1 := smapping.FillStruct(&newKeyboard, smapping.MapFields(&keyboard))
	if err != nil {
		return newKeyboard, err1
	}
	res, err := service.keyboardRepository.UpdateKeyboard(newKeyboard, id)
	return res, err
}

func (service *keyboardService) DeleteKeyboard(keyboard model.Keyboard, id uint) (keyboardOutput model.Keyboard, err error) {
	newKeyboard := model.Keyboard{}
	err1 := smapping.FillStruct(&newKeyboard, smapping.MapFields(&keyboard))
	if err != nil {
		return newKeyboard, err1
	}
	res, err := service.keyboardRepository.UpdateKeyboard(newKeyboard, id)
	return res, err
}
