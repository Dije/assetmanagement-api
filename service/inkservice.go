package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type InkService interface {
	FindInks() (inkOutput []model.Ink, err error)
	FindInkById(id uint) (inkOutput model.Ink, err error)
	FindExcInk(id uint) (inkOutput []model.Ink, err error)
	FindBlackInks() (inkOutput []model.Ink, err error)
	FindExcBlackInks(id uint) (inkOutput []model.Ink, err error)
	FindCyanInks() (inkOutput []model.Ink, err error)
	FindExcCyanInks(id uint) (inkOutput []model.Ink, err error)
	FindMagentaInks() (inkOutput []model.Ink, err error)
	FindExcMagentaInks(id uint) (inkOutput []model.Ink, err error)
	FindYellowInks() (inkOutput []model.Ink, err error)
	FindExcYellowInks(id uint) (inkOutput []model.Ink, err error)
	FindColorInks() (inkOutput []model.Ink, err error)
	FindExcColorInks(id uint) (inkOutput []model.Ink, err error)
	FindTricolorInks() (inkOutput []model.Ink, err error)
	FindExcTricolorInks(id uint) (inkOutput []model.Ink, err error)
	FindOthersInks() (inkOutput []model.Ink, err error)
	FindExcOthersInks(id uint) (inkOutput []model.Ink, err error)
	InsertInk(ink model.CreateInkParameter) (inkOutput model.Ink, err error)
	UpdateInk(ink model.Ink, id uint) (inkOutput model.Ink, err error)
	DeleteInk(ink model.Ink, id uint) (inkOutput model.Ink, err error)
}

type inkService struct {
	inkRepository repository.InkRepository
}

func NewInkService(inkRep repository.InkRepository) InkService {
	return &inkService{
		inkRepository: inkRep,
	}
}

func (service *inkService) FindInks() (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindInks()
	return res, err
}

func (service *inkService) FindInkById(id uint) (inkOutput model.Ink, err error) {
	res, err := service.inkRepository.FindInkById(id)
	return res, err
}

func (service *inkService) FindExcInk(id uint) (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindExcInk(id)
	return res, err
}

// FindBlackInks implements InkService
func (service *inkService) FindBlackInks() (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindBlackInks()
	return res, err
}

// FindColorInks implements InkService
func (service *inkService) FindColorInks() (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindColorInks()
	return res, err
}

// FindCyanInks implements InkService
func (service *inkService) FindCyanInks() (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindCyanInks()
	return res, err
}

// FindExcBlackInks implements InkService
func (service *inkService) FindExcBlackInks(id uint) (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindExcBlackInks(id)
	return res, err
}

// FindExcColorInks implements InkService
func (service *inkService) FindExcColorInks(id uint) (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindExcColorInks(id)
	return res, err
}

// FindExcCyanInks implements InkService
func (service *inkService) FindExcCyanInks(id uint) (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindExcCyanInks(id)
	return res, err
}

// FindExcMagentaInks implements InkService
func (service *inkService) FindExcMagentaInks(id uint) (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindExcMagentaInks(id)
	return res, err
}

// FindExcOthersInks implements InkService
func (service *inkService) FindExcOthersInks(id uint) (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindExcOthersInks(id)
	return res, err
}

// FindExcTricolorInks implements InkService
func (service *inkService) FindExcTricolorInks(id uint) (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindExcTricolorInks(id)
	return res, err
}

// FindExcYellowInks implements InkService
func (service *inkService) FindExcYellowInks(id uint) (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindExcYellowInks(id)
	return res, err
}

// FindMagentaInks implements InkService
func (service *inkService) FindMagentaInks() (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindMagentaInks()
	return res, err
}

// FindOthersInks implements InkService
func (service *inkService) FindOthersInks() (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindOthersInks()
	return res, err
}

// FindTricolorInks implements InkService
func (service *inkService) FindTricolorInks() (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindTricolorInks()
	return res, err
}

// FindYellowInks implements InkService
func (service *inkService) FindYellowInks() (inkOutput []model.Ink, err error) {
	res, err := service.inkRepository.FindYellowInks()
	return res, err
}

func (service *inkService) InsertInk(ink model.CreateInkParameter) (inkOutput model.Ink, err error) {
	newInk := model.Ink{}
	err1 := smapping.FillStruct(&newInk, smapping.MapFields(&ink))
	if err != nil {
		return newInk, err1
	}
	res, err := service.inkRepository.InsertInk(newInk)
	return res, err
}

func (service *inkService) UpdateInk(ink model.Ink, id uint) (inkOutput model.Ink, err error) {
	newInk := model.Ink{}
	err1 := smapping.FillStruct(&newInk, smapping.MapFields(&ink))
	if err != nil {
		return newInk, err1
	}
	res, err := service.inkRepository.UpdateInk(newInk, id)
	return res, err
}

func (service *inkService) DeleteInk(ink model.Ink, id uint) (inkOutput model.Ink, err error) {
	newInk := model.Ink{}
	err1 := smapping.FillStruct(&newInk, smapping.MapFields(&ink))
	if err != nil {
		return newInk, err1
	}
	res, err := service.inkRepository.UpdateInk(newInk, id)
	return res, err
}
