package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type OfficeService interface {
	FindOffices() (officeOutput []model.Office, err error)
	FindOfficeById(id uint) (officeOutput model.Office, err error)
	FindExcOffice(id uint) (officeOutput []model.Office, err error)
	InsertOffice(office model.CreateOfficeParameter) (officeOutput model.Office, err error)
	UpdateOffice(office model.Office, id uint) (officeOutput model.Office, err error)
	DeleteOffice(office model.Office, id uint) (officeOutput model.Office, err error)
}

type officeService struct {
	officeRepository repository.OfficeRepository
}

func NewOfficeService(officeRep repository.OfficeRepository) OfficeService {
	return &officeService{
		officeRepository: officeRep,
	}
}

func (service *officeService) FindOffices() (officeOutput []model.Office, err error) {
	res, err := service.officeRepository.FindOffices()
	return res, err
}

func (service *officeService) FindOfficeById(id uint) (officeOutput model.Office, err error) {
	res, err := service.officeRepository.FindOfficeById(id)
	return res, err
}

func (service *officeService) FindExcOffice(id uint) (officeOutput []model.Office, err error) {
	res, err := service.officeRepository.FindExcOffice(id)
	return res, err
}

func (service *officeService) InsertOffice(office model.CreateOfficeParameter) (officeOutput model.Office, err error) {
	newOffice := model.Office{}
	err1 := smapping.FillStruct(&newOffice, smapping.MapFields(&office))
	if err != nil {
		return newOffice, err1
	}
	res, err := service.officeRepository.InsertOffice(newOffice)
	return res, err
}

func (service *officeService) UpdateOffice(office model.Office, id uint) (officeOutput model.Office, err error) {
	newOffice := model.Office{}
	err1 := smapping.FillStruct(&newOffice, smapping.MapFields(&office))
	if err != nil {
		return newOffice, err1
	}
	res, err := service.officeRepository.UpdateOffice(newOffice, id)
	return res, err
}

func (service *officeService) DeleteOffice(office model.Office, id uint) (officeOutput model.Office, err error) {
	newOffice := model.Office{}
	err1 := smapping.FillStruct(&newOffice, smapping.MapFields(&office))
	if err != nil {
		return newOffice, err1
	}
	res, err := service.officeRepository.UpdateOffice(newOffice, id)
	return res, err
}
