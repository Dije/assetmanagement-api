package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type ComputerService interface {
	FindComputers() (computerOutput []model.SelectComputerParameter, err error)
	FindComputerById(id uint) (computerOutput model.SelectComputerParameter, err error)
	InsertComputer(computer model.CreateComputerParameter) (computerOutput model.Computer, err error)
	UpdateComputer(computer model.Computer, id uint) (computerOutput model.Computer, err error)
	DeleteComputer(computer model.Computer, id uint) (computerOutput model.Computer, err error)
}

type computerService struct {
	computerRepository repository.ComputerRepository
}

func NewComputerService(computerRep repository.ComputerRepository) ComputerService {
	return &computerService{
		computerRepository: computerRep,
	}
}

func (service *computerService) FindComputers() (computerOutput []model.SelectComputerParameter, err error) {
	res, err := service.computerRepository.FindComputers()
	return res, err
}

func (service *computerService) FindComputerById(id uint) (computerOutput model.SelectComputerParameter, err error) {
	res, err := service.computerRepository.FindComputerById(id)
	return res, err
}

func (service *computerService) InsertComputer(computer model.CreateComputerParameter) (computerOutput model.Computer, err error) {
	newComputer := model.Computer{}
	err1 := smapping.FillStruct(&newComputer, smapping.MapFields(&computer))
	if err != nil {
		return newComputer, err1
	}
	res, err := service.computerRepository.InsertComputer(newComputer)
	return res, err
}

func (service *computerService) UpdateComputer(computer model.Computer, id uint) (computerOutput model.Computer, err error) {
	newComputer := model.Computer{}
	err1 := smapping.FillStruct(&newComputer, smapping.MapFields(&computer))
	if err != nil {
		return newComputer, err1
	}
	res, err := service.computerRepository.UpdateComputer(newComputer, id)
	return res, err
}

func (service *computerService) DeleteComputer(computer model.Computer, id uint) (computerOutput model.Computer, err error) {
	newComputer := model.Computer{}
	err1 := smapping.FillStruct(&newComputer, smapping.MapFields(&computer))
	if err != nil {
		return newComputer, err1
	}
	res, err := service.computerRepository.DeleteComputer(newComputer, id)
	return res, err
}
