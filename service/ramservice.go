package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type RAMService interface {
	FindRAMs() (ramOutput []model.RAM, err error)
	FindRAMById(id uint) (ramOutput model.RAM, err error)
	FindExcRAM(id uint) (ramOutput []model.RAM, err error)
	InsertRAM(ram model.CreateRAMParameter) (ramOutput model.RAM, err error)
	UpdateRAM(ram model.RAM, id uint) (ramOutput model.RAM, err error)
	DeleteRAM(ram model.RAM, id uint) (ramOutput model.RAM, err error)
}

type ramService struct {
	ramRepository repository.RAMRepository
}

func NewRAMService(ramRep repository.RAMRepository) RAMService {
	return &ramService{
		ramRepository: ramRep,
	}
}

func (service *ramService) FindRAMs() (ramOutput []model.RAM, err error) {
	res, err := service.ramRepository.FindRAMs()
	return res, err
}

func (service *ramService) FindRAMById(id uint) (ramOutput model.RAM, err error) {
	res, err := service.ramRepository.FindRAMById(id)
	return res, err
}

func (service *ramService) FindExcRAM(id uint) (ramOutput []model.RAM, err error) {
	res, err := service.ramRepository.FindExcRAM(id)
	return res, err
}

func (service *ramService) InsertRAM(ram model.CreateRAMParameter) (ramOutput model.RAM, err error) {
	newRAM := model.RAM{}
	err1 := smapping.FillStruct(&newRAM, smapping.MapFields(&ram))
	if err != nil {
		return newRAM, err1
	}
	res, err := service.ramRepository.InsertRAM(newRAM)
	return res, err
}

func (service *ramService) UpdateRAM(ram model.RAM, id uint) (ramOutput model.RAM, err error) {
	newRAM := model.RAM{}
	err1 := smapping.FillStruct(&newRAM, smapping.MapFields(&ram))
	if err != nil {
		return newRAM, err1
	}
	res, err := service.ramRepository.UpdateRAM(newRAM, id)
	return res, err
}

func (service *ramService) DeleteRAM(ram model.RAM, id uint) (ramOutput model.RAM, err error) {
	newRAM := model.RAM{}
	err1 := smapping.FillStruct(&newRAM, smapping.MapFields(&ram))
	if err != nil {
		return newRAM, err1
	}
	res, err := service.ramRepository.UpdateRAM(newRAM, id)
	return res, err
}
