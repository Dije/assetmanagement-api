package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type MotherboardService interface {
	FindMotherboards() (motherboardOutput []model.Motherboard, err error)
	FindMotherboardById(id uint) (motherboardOutput model.Motherboard, err error)
	FindExcMotherboard(id uint) (motherboardOutput []model.Motherboard, err error)
	InsertMotherboard(motherboard model.CreateMotherboardParameter) (motherboardOutput model.Motherboard, err error)
	UpdateMotherboard(motherboard model.Motherboard, id uint) (motherboardOutput model.Motherboard, err error)
	DeleteMotherboard(motherboard model.Motherboard, id uint) (motherboardOutput model.Motherboard, err error)
}

type motherboardService struct {
	motherboardRepository repository.MotherboardRepository
}

func NewMotherboardService(motherboardRep repository.MotherboardRepository) MotherboardService {
	return &motherboardService{
		motherboardRepository: motherboardRep,
	}
}

func (service *motherboardService) FindMotherboards() (motherboardOutput []model.Motherboard, err error) {
	res, err := service.motherboardRepository.FindMotherboards()
	return res, err
}

func (service *motherboardService) FindMotherboardById(id uint) (motherboardOutput model.Motherboard, err error) {
	res, err := service.motherboardRepository.FindMotherboardById(id)
	return res, err
}

func (service *motherboardService) FindExcMotherboard(id uint) (motherboardOutput []model.Motherboard, err error) {
	res, err := service.motherboardRepository.FindExcMotherboard(id)
	return res, err
}

func (service *motherboardService) InsertMotherboard(motherboard model.CreateMotherboardParameter) (motherboardOutput model.Motherboard, err error) {
	newMotherboard := model.Motherboard{}
	err1 := smapping.FillStruct(&newMotherboard, smapping.MapFields(&motherboard))
	if err != nil {
		return newMotherboard, err1
	}
	res, err := service.motherboardRepository.InsertMotherboard(newMotherboard)
	return res, err
}

func (service *motherboardService) UpdateMotherboard(motherboard model.Motherboard, id uint) (motherboardOutput model.Motherboard, err error) {
	newMotherboard := model.Motherboard{}
	err1 := smapping.FillStruct(&newMotherboard, smapping.MapFields(&motherboard))
	if err != nil {
		return newMotherboard, err1
	}
	res, err := service.motherboardRepository.UpdateMotherboard(newMotherboard, id)
	return res, err
}

func (service *motherboardService) DeleteMotherboard(motherboard model.Motherboard, id uint) (motherboardOutput model.Motherboard, err error) {
	newMotherboard := model.Motherboard{}
	err1 := smapping.FillStruct(&newMotherboard, smapping.MapFields(&motherboard))
	if err != nil {
		return newMotherboard, err1
	}
	res, err := service.motherboardRepository.UpdateMotherboard(newMotherboard, id)
	return res, err
}
