package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type HDDService interface {
	FindHDDs() (hddOutput []model.HDD, err error)
	FindHDDById(id uint) (hddOutput model.HDD, err error)
	FindExcHDD(id uint) (hddOutput []model.HDD, err error)
	InsertHDD(hdd model.CreateHDDParameter) (hddOutput model.HDD, err error)
	UpdateHDD(hdd model.HDD, id uint) (hddOutput model.HDD, err error)
	DeleteHDD(hdd model.HDD, id uint) (hddOutput model.HDD, err error)
}

type hddService struct {
	hddRepository repository.HDDRepository
}

func NewHDDService(hddRep repository.HDDRepository) HDDService {
	return &hddService{
		hddRepository: hddRep,
	}
}

func (service *hddService) FindHDDs() (hddOutput []model.HDD, err error) {
	res, err := service.hddRepository.FindHDDs()
	return res, err
}

func (service *hddService) FindHDDById(id uint) (hddOutput model.HDD, err error) {
	res, err := service.hddRepository.FindHDDById(id)
	return res, err
}

func (service *hddService) FindExcHDD(id uint) (hddOutput []model.HDD, err error) {
	res, err := service.hddRepository.FindExcHDD(id)
	return res, err
}

func (service *hddService) InsertHDD(hdd model.CreateHDDParameter) (hddOutput model.HDD, err error) {
	newHDD := model.HDD{}
	err1 := smapping.FillStruct(&newHDD, smapping.MapFields(&hdd))
	if err != nil {
		return newHDD, err1
	}
	res, err := service.hddRepository.InsertHDD(newHDD)
	return res, err
}

func (service *hddService) UpdateHDD(hdd model.HDD, id uint) (hddOutput model.HDD, err error) {
	newHDD := model.HDD{}
	err1 := smapping.FillStruct(&newHDD, smapping.MapFields(&hdd))
	if err != nil {
		return newHDD, err1
	}
	res, err := service.hddRepository.UpdateHDD(newHDD, id)
	return res, err
}

func (service *hddService) DeleteHDD(hdd model.HDD, id uint) (hddOutput model.HDD, err error) {
	newHDD := model.HDD{}
	err1 := smapping.FillStruct(&newHDD, smapping.MapFields(&hdd))
	if err != nil {
		return newHDD, err1
	}
	res, err := service.hddRepository.UpdateHDD(newHDD, id)
	return res, err
}
