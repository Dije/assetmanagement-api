package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type SubscriptionService interface {
	FindSubscriptions() (subscriptionOutput []model.Subscription, err error)
	FindSubscriptionById(id uint) (subscriptionOutput model.Subscription, err error)
	FindExcSubscription(id uint) (subscriptionOutput []model.Subscription, err error)
	InsertSubscription(subscription model.CreateSubscriptionParameter) (subscriptionOutput model.Subscription, err error)
	UpdateSubscription(subscription model.Subscription, id uint) (subscriptionOutput model.Subscription, err error)
	DeleteSubscription(subscription model.Subscription, id uint) (subscriptionOutput model.Subscription, err error)
}

type subscriptionService struct {
	subscriptionRepository repository.SubscriptionRepository
}

func NewSubscriptionService(subscriptionRep repository.SubscriptionRepository) SubscriptionService {
	return &subscriptionService{
		subscriptionRepository: subscriptionRep,
	}
}

func (service *subscriptionService) FindSubscriptions() (subscriptionOutput []model.Subscription, err error) {
	res, err := service.subscriptionRepository.FindSubscriptions()
	return res, err
}

func (service *subscriptionService) FindSubscriptionById(id uint) (subscriptionOutput model.Subscription, err error) {
	res, err := service.subscriptionRepository.FindSubscriptionById(id)
	return res, err
}

func (service *subscriptionService) FindExcSubscription(id uint) (subscriptionOutput []model.Subscription, err error) {
	res, err := service.subscriptionRepository.FindExcSubscription(id)
	return res, err
}

func (service *subscriptionService) InsertSubscription(subscription model.CreateSubscriptionParameter) (subscriptionOutput model.Subscription, err error) {
	newSubscription := model.Subscription{}
	err1 := smapping.FillStruct(&newSubscription, smapping.MapFields(&subscription))
	if err != nil {
		return newSubscription, err1
	}
	res, err := service.subscriptionRepository.InsertSubscription(newSubscription)
	return res, err
}

func (service *subscriptionService) UpdateSubscription(subscription model.Subscription, id uint) (subscriptionOutput model.Subscription, err error) {
	newSubscription := model.Subscription{}
	err1 := smapping.FillStruct(&newSubscription, smapping.MapFields(&subscription))
	if err != nil {
		return newSubscription, err1
	}
	res, err := service.subscriptionRepository.UpdateSubscription(newSubscription, id)
	return res, err
}

func (service *subscriptionService) DeleteSubscription(subscription model.Subscription, id uint) (subscriptionOutput model.Subscription, err error) {
	newSubscription := model.Subscription{}
	err1 := smapping.FillStruct(&newSubscription, smapping.MapFields(&subscription))
	if err != nil {
		return newSubscription, err1
	}
	res, err := service.subscriptionRepository.UpdateSubscription(newSubscription, id)
	return res, err
}
