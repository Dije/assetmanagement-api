package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type ProcessorService interface {
	FindProcessors() (processorOutput []model.Processor, err error)
	FindProcessorById(id uint) (processorOutput model.Processor, err error)
	FindExcProcessor(id uint) (processorOutput []model.Processor, err error)
	InsertProcessor(processor model.CreateProcessorParameter) (processorOutput model.Processor, err error)
	UpdateProcessor(processor model.Processor, id uint) (processorOutput model.Processor, err error)
	DeleteProcessor(processor model.Processor, id uint) (processorOutput model.Processor, err error)
}

type processorService struct {
	processorRepository repository.ProcessorRepository
}

func NewProcessorService(processorRep repository.ProcessorRepository) ProcessorService {
	return &processorService{
		processorRepository: processorRep,
	}
}

func (service *processorService) FindProcessors() (processorOutput []model.Processor, err error) {
	res, err := service.processorRepository.FindProcessors()
	return res, err
}

func (service *processorService) FindProcessorById(id uint) (processorOutput model.Processor, err error) {
	res, err := service.processorRepository.FindProcessorById(id)
	return res, err
}

func (service *processorService) FindExcProcessor(id uint) (processorOutput []model.Processor, err error) {
	res, err := service.processorRepository.FindExcProcessor(id)
	return res, err
}

func (service *processorService) InsertProcessor(processor model.CreateProcessorParameter) (processorOutput model.Processor, err error) {
	newProcessor := model.Processor{}
	err1 := smapping.FillStruct(&newProcessor, smapping.MapFields(&processor))
	if err != nil {
		return newProcessor, err1
	}
	res, err := service.processorRepository.InsertProcessor(newProcessor)
	return res, err
}

func (service *processorService) UpdateProcessor(processor model.Processor, id uint) (processorOutput model.Processor, err error) {
	newProcessor := model.Processor{}
	err1 := smapping.FillStruct(&newProcessor, smapping.MapFields(&processor))
	if err != nil {
		return newProcessor, err1
	}
	res, err := service.processorRepository.UpdateProcessor(newProcessor, id)
	return res, err
}

func (service *processorService) DeleteProcessor(processor model.Processor, id uint) (processorOutput model.Processor, err error) {
	newProcessor := model.Processor{}
	err1 := smapping.FillStruct(&newProcessor, smapping.MapFields(&processor))
	if err != nil {
		return newProcessor, err1
	}
	res, err := service.processorRepository.UpdateProcessor(newProcessor, id)
	return res, err
}
