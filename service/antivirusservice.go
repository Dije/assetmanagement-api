package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type AntivirusService interface {
	FindAntiviruss() (antivirusOutput []model.SelectAntivirusParameter, err error)
	FindAntivirusById(id uint) (antivirusOutput model.SelectAntivirusParameter, err error)
	FindExcAntivirus(id uint) (antivirusOutput []model.SelectAntivirusParameter, err error)
	InsertAntivirus(antivirus model.CreateAntivirusParameter) (antivirusOutput model.Antivirus, err error)
	UpdateAntivirus(antivirus model.Antivirus, id uint) (antivirusOutput model.Antivirus, err error)
	DeleteAntivirus(antivirus model.Antivirus, id uint) (antivirusOutput model.Antivirus, err error)
}

type antivirusService struct {
	antivirusRepository repository.AntivirusRepository
}

func NewAntivirusService(antivirusRep repository.AntivirusRepository) AntivirusService {
	return &antivirusService{
		antivirusRepository: antivirusRep,
	}
}

func (service *antivirusService) FindAntiviruss() (antivirusOutput []model.SelectAntivirusParameter, err error) {
	res, err := service.antivirusRepository.FindAntiviruss()
	return res, err
}

func (service *antivirusService) FindAntivirusById(id uint) (antivirusOutput model.SelectAntivirusParameter, err error) {
	res, err := service.antivirusRepository.FindAntivirusById(id)
	return res, err
}

func (service *antivirusService) FindExcAntivirus(id uint) (antivirusOutput []model.SelectAntivirusParameter, err error) {
	res, err := service.antivirusRepository.FindExcAntivirus(id)
	return res, err
}
func (service *antivirusService) InsertAntivirus(antivirus model.CreateAntivirusParameter) (antivirusOutput model.Antivirus, err error) {
	newAntivirus := model.Antivirus{}
	err1 := smapping.FillStruct(&newAntivirus, smapping.MapFields(&antivirus))
	if err != nil {
		return newAntivirus, err1
	}
	res, err := service.antivirusRepository.InsertAntivirus(newAntivirus)
	return res, err
}

func (service *antivirusService) UpdateAntivirus(antivirus model.Antivirus, id uint) (antivirusOutput model.Antivirus, err error) {
	newAntivirus := model.Antivirus{}
	err1 := smapping.FillStruct(&newAntivirus, smapping.MapFields(&antivirus))
	if err != nil {
		return newAntivirus, err1
	}
	res, err := service.antivirusRepository.UpdateAntivirus(newAntivirus, id)
	return res, err
}

func (service *antivirusService) DeleteAntivirus(antivirus model.Antivirus, id uint) (antivirusOutput model.Antivirus, err error) {
	newAntivirus := model.Antivirus{}
	err1 := smapping.FillStruct(&newAntivirus, smapping.MapFields(&antivirus))
	if err != nil {
		return newAntivirus, err1
	}
	res, err := service.antivirusRepository.UpdateAntivirus(newAntivirus, id)
	return res, err
}
