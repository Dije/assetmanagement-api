package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type HarddiskService interface {
	FindHarddisks() (harddiskOutput []model.SelectHarddiskParameter, err error)
	FindHarddiskById(id uint) (harddiskOutput model.SelectHarddiskParameter, err error)
	FindExcHarddisk(id uint) (harddiskOutput []model.SelectHarddiskParameter, err error)
	InsertHarddisk(harddisk model.CreateHarddiskParameter) (harddiskOutput model.Harddisk, err error)
	UpdateHarddisk(harddisk model.Harddisk, id uint) (harddiskOutput model.Harddisk, err error)
	DeleteHarddisk(harddisk model.Harddisk, id uint) (harddiskOutput model.Harddisk, err error)
}

type harddiskService struct {
	harddiskRepository repository.HarddiskRepository
}

func NewHarddiskService(harddiskRep repository.HarddiskRepository) HarddiskService {
	return &harddiskService{
		harddiskRepository: harddiskRep,
	}
}

func (service *harddiskService) FindHarddisks() (harddiskOutput []model.SelectHarddiskParameter, err error) {
	res, err := service.harddiskRepository.FindHarddisks()
	return res, err
}

func (service *harddiskService) FindHarddiskById(id uint) (harddiskOutput model.SelectHarddiskParameter, err error) {
	res, err := service.harddiskRepository.FindHarddiskById(id)
	return res, err
}

func (service *harddiskService) FindExcHarddisk(id uint) (harddiskOutput []model.SelectHarddiskParameter, err error) {
	res, err := service.harddiskRepository.FindExcHarddisk(id)
	return res, err
}

func (service *harddiskService) InsertHarddisk(harddisk model.CreateHarddiskParameter) (harddiskOutput model.Harddisk, err error) {
	newHarddisk := model.Harddisk{}
	err1 := smapping.FillStruct(&newHarddisk, smapping.MapFields(&harddisk))
	if err != nil {
		return newHarddisk, err1
	}
	res, err := service.harddiskRepository.InsertHarddisk(newHarddisk)
	return res, err
}

func (service *harddiskService) UpdateHarddisk(harddisk model.Harddisk, id uint) (harddiskOutput model.Harddisk, err error) {
	newHarddisk := model.Harddisk{}
	err1 := smapping.FillStruct(&newHarddisk, smapping.MapFields(&harddisk))
	if err != nil {
		return newHarddisk, err1
	}
	res, err := service.harddiskRepository.UpdateHarddisk(newHarddisk, id)
	return res, err
}

func (service *harddiskService) DeleteHarddisk(harddisk model.Harddisk, id uint) (harddiskOutput model.Harddisk, err error) {
	newHarddisk := model.Harddisk{}
	err1 := smapping.FillStruct(&newHarddisk, smapping.MapFields(&harddisk))
	if err != nil {
		return newHarddisk, err1
	}
	res, err := service.harddiskRepository.UpdateHarddisk(newHarddisk, id)
	return res, err
}
