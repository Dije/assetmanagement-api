package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type PrinterService interface {
	FindPrinters() (printerOutput []model.SelectPrinterParameter, err error)
	FindPrinterById(id uint) (printerOutput model.SelectPrinterParameter, err error)
	FindExcPrinter(id uint) (printerOutput []model.SelectPrinterParameter, err error)
	InsertPrinter(printer model.CreatePrinterParameter) (printerOutput model.Printer, err error)
	UpdatePrinter(printer model.Printer, id uint) (printerOutput model.Printer, err error)
	DeletePrinter(printer model.Printer, id uint) (printerOutput model.Printer, err error)
}

type printerService struct {
	printerRepository repository.PrinterRepository
}

func NewPrinterService(printerRep repository.PrinterRepository) PrinterService {
	return &printerService{
		printerRepository: printerRep,
	}
}

func (service *printerService) FindPrinters() (printerOutput []model.SelectPrinterParameter, err error) {
	res, err := service.printerRepository.FindPrinters()
	return res, err
}

func (service *printerService) FindPrinterById(id uint) (printerOutput model.SelectPrinterParameter, err error) {
	res, err := service.printerRepository.FindPrinterById(id)
	return res, err
}

func (service *printerService) FindExcPrinter(id uint) (printerOutput []model.SelectPrinterParameter, err error) {
	res, err := service.printerRepository.FindExcPrinter(id)
	return res, err
}

func (service *printerService) InsertPrinter(printer model.CreatePrinterParameter) (printerOutput model.Printer, err error) {
	newPrinter := model.Printer{}
	err1 := smapping.FillStruct(&newPrinter, smapping.MapFields(&printer))
	if err != nil {
		return newPrinter, err1
	}
	res, err := service.printerRepository.InsertPrinter(newPrinter)
	return res, err
}

func (service *printerService) UpdatePrinter(printer model.Printer, id uint) (printerOutput model.Printer, err error) {
	newPrinter := model.Printer{}
	err1 := smapping.FillStruct(&newPrinter, smapping.MapFields(&printer))
	if err != nil {
		return newPrinter, err1
	}
	res, err := service.printerRepository.UpdatePrinter(newPrinter, id)
	return res, err
}

func (service *printerService) DeletePrinter(printer model.Printer, id uint) (printerOutput model.Printer, err error) {
	newPrinter := model.Printer{}
	err1 := smapping.FillStruct(&newPrinter, smapping.MapFields(&printer))
	if err != nil {
		return newPrinter, err1
	}
	res, err := service.printerRepository.UpdatePrinter(newPrinter, id)
	return res, err
}
