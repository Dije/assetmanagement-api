package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type ServiceUPSService interface {
	FindServiceUPSs() (serviceUPSOutput []model.SelectServiceUPSParameter, err error)
	FindServiceUPSById(id uint) (serviceUPSOutput model.SelectServiceUPSParameter, err error)
	FindExcServiceUPS(id uint) (serviceUPSOutput []model.SelectServiceUPSParameter, err error)
	InsertServiceUPS(serviceUPS model.CreateServiceUPSParameter) (serviceUPSOutput model.ServiceUPS, err error)
	UpdateServiceUPS(serviceUPS model.ServiceUPS, id uint) (serviceUPSOutput model.ServiceUPS, err error)
	DeleteServiceUPS(serviceUPS model.ServiceUPS, id uint) (serviceUPSOutput model.ServiceUPS, err error)
}

type serviceUPSService struct {
	serviceUPSRepository repository.ServiceUPSRepository
}

func NewServiceUPSService(serviceUPSRep repository.ServiceUPSRepository) ServiceUPSService {
	return &serviceUPSService{
		serviceUPSRepository: serviceUPSRep,
	}
}

func (service *serviceUPSService) FindServiceUPSs() (serviceUPSOutput []model.SelectServiceUPSParameter, err error) {
	res, err := service.serviceUPSRepository.FindServiceUPSs()
	return res, err
}

func (service *serviceUPSService) FindServiceUPSById(id uint) (serviceUPSOutput model.SelectServiceUPSParameter, err error) {
	res, err := service.serviceUPSRepository.FindServiceUPSById(id)
	return res, err
}

func (service *serviceUPSService) FindExcServiceUPS(id uint) (serviceUPSOutput []model.SelectServiceUPSParameter, err error) {
	res, err := service.serviceUPSRepository.FindExcServiceUPS(id)
	return res, err
}

func (service *serviceUPSService) InsertServiceUPS(serviceUPS model.CreateServiceUPSParameter) (serviceUPSOutput model.ServiceUPS, err error) {
	newServiceUPS := model.ServiceUPS{}
	err1 := smapping.FillStruct(&newServiceUPS, smapping.MapFields(&serviceUPS))
	if err != nil {
		return newServiceUPS, err1
	}
	res, err := service.serviceUPSRepository.InsertServiceUPS(newServiceUPS)
	return res, err
}

func (service *serviceUPSService) UpdateServiceUPS(serviceUPS model.ServiceUPS, id uint) (serviceUPSOutput model.ServiceUPS, err error) {
	newServiceUPS := model.ServiceUPS{}
	err1 := smapping.FillStruct(&newServiceUPS, smapping.MapFields(&serviceUPS))
	if err != nil {
		return newServiceUPS, err1
	}
	res, err := service.serviceUPSRepository.UpdateServiceUPS(newServiceUPS, id)
	return res, err
}

func (service *serviceUPSService) DeleteServiceUPS(serviceUPS model.ServiceUPS, id uint) (serviceUPSOutput model.ServiceUPS, err error) {
	newServiceUPS := model.ServiceUPS{}
	err1 := smapping.FillStruct(&newServiceUPS, smapping.MapFields(&serviceUPS))
	if err != nil {
		return newServiceUPS, err1
	}
	res, err := service.serviceUPSRepository.UpdateServiceUPS(newServiceUPS, id)
	return res, err
}
