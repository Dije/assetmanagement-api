package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type UPSService interface {
	FindUPSs() (upsOutput []model.SelectUPSParameter, err error)
	FindUPSById(id uint) (upsOutput model.SelectUPSParameter, err error)
	FindExcUPS(id uint) (upsOutput []model.SelectUPSParameter, err error)
	InsertUPS(ups model.CreateUPSParameter) (upsOutput model.UPS, err error)
	UpdateUPS(ups model.UPS, id uint) (upsOutput model.UPS, err error)
	DeleteUPS(ups model.UPS, id uint) (upsOutput model.UPS, err error)
}

type upsService struct {
	upsRepository repository.UPSRepository
}

func NewUPSService(upsRep repository.UPSRepository) UPSService {
	return &upsService{
		upsRepository: upsRep,
	}
}

func (service *upsService) FindUPSs() (upsOutput []model.SelectUPSParameter, err error) {
	res, err := service.upsRepository.FindUPSs()
	return res, err
}

func (service *upsService) FindUPSById(id uint) (upsOutput model.SelectUPSParameter, err error) {
	res, err := service.upsRepository.FindUPSById(id)
	return res, err
}

func (service *upsService) FindExcUPS(id uint) (upsOutput []model.SelectUPSParameter, err error) {
	res, err := service.upsRepository.FindExcUPS(id)
	return res, err
}

func (service *upsService) InsertUPS(ups model.CreateUPSParameter) (upsOutput model.UPS, err error) {
	newUPS := model.UPS{}
	err1 := smapping.FillStruct(&newUPS, smapping.MapFields(&ups))
	if err != nil {
		return newUPS, err1
	}
	res, err := service.upsRepository.InsertUPS(newUPS)
	return res, err
}

func (service *upsService) UpdateUPS(ups model.UPS, id uint) (upsOutput model.UPS, err error) {
	newUPS := model.UPS{}
	err1 := smapping.FillStruct(&newUPS, smapping.MapFields(&ups))
	if err != nil {
		return newUPS, err1
	}
	res, err := service.upsRepository.UpdateUPS(newUPS, id)
	return res, err
}

func (service *upsService) DeleteUPS(ups model.UPS, id uint) (upsOutput model.UPS, err error) {
	newUPS := model.UPS{}
	err1 := smapping.FillStruct(&newUPS, smapping.MapFields(&ups))
	if err != nil {
		return newUPS, err1
	}
	res, err := service.upsRepository.UpdateUPS(newUPS, id)
	return res, err
}
