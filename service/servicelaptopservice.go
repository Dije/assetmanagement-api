package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type ServiceLaptopService interface {
	FindServiceLaptops() (serviceLaptopOutput []model.SelectServiceLaptopParameter, err error)
	FindServiceLaptopById(id uint) (serviceLaptopOutput model.SelectServiceLaptopParameter, err error)
	FindExcServiceLaptop(id uint) (serviceLaptopOutput []model.SelectServiceLaptopParameter, err error)
	InsertServiceLaptop(serviceLaptop model.CreateServiceLaptopParameter) (serviceLaptopOutput model.ServiceLaptop, err error)
	UpdateServiceLaptop(serviceLaptop model.ServiceLaptop, id uint) (serviceLaptopOutput model.ServiceLaptop, err error)
	DeleteServiceLaptop(serviceLaptop model.ServiceLaptop, id uint) (serviceLaptopOutput model.ServiceLaptop, err error)
}

type serviceLaptopService struct {
	serviceLaptopRepository repository.ServiceLaptopRepository
}

func NewServiceLaptopService(serviceLaptopRep repository.ServiceLaptopRepository) ServiceLaptopService {
	return &serviceLaptopService{
		serviceLaptopRepository: serviceLaptopRep,
	}
}

func (service *serviceLaptopService) FindServiceLaptops() (serviceLaptopOutput []model.SelectServiceLaptopParameter, err error) {
	res, err := service.serviceLaptopRepository.FindServiceLaptops()
	return res, err
}

func (service *serviceLaptopService) FindServiceLaptopById(id uint) (serviceLaptopOutput model.SelectServiceLaptopParameter, err error) {
	res, err := service.serviceLaptopRepository.FindServiceLaptopById(id)
	return res, err
}

func (service *serviceLaptopService) FindExcServiceLaptop(id uint) (serviceLaptopOutput []model.SelectServiceLaptopParameter, err error) {
	res, err := service.serviceLaptopRepository.FindExcServiceLaptop(id)
	return res, err
}

func (service *serviceLaptopService) InsertServiceLaptop(serviceLaptop model.CreateServiceLaptopParameter) (serviceLaptopOutput model.ServiceLaptop, err error) {
	newServiceLaptop := model.ServiceLaptop{}
	err1 := smapping.FillStruct(&newServiceLaptop, smapping.MapFields(&serviceLaptop))
	if err != nil {
		return newServiceLaptop, err1
	}
	res, err := service.serviceLaptopRepository.InsertServiceLaptop(newServiceLaptop)
	return res, err
}

func (service *serviceLaptopService) UpdateServiceLaptop(serviceLaptop model.ServiceLaptop, id uint) (serviceLaptopOutput model.ServiceLaptop, err error) {
	newServiceLaptop := model.ServiceLaptop{}
	err1 := smapping.FillStruct(&newServiceLaptop, smapping.MapFields(&serviceLaptop))
	if err != nil {
		return newServiceLaptop, err1
	}
	res, err := service.serviceLaptopRepository.UpdateServiceLaptop(newServiceLaptop, id)
	return res, err
}

func (service *serviceLaptopService) DeleteServiceLaptop(serviceLaptop model.ServiceLaptop, id uint) (serviceLaptopOutput model.ServiceLaptop, err error) {
	newServiceLaptop := model.ServiceLaptop{}
	err1 := smapping.FillStruct(&newServiceLaptop, smapping.MapFields(&serviceLaptop))
	if err != nil {
		return newServiceLaptop, err1
	}
	res, err := service.serviceLaptopRepository.UpdateServiceLaptop(newServiceLaptop, id)
	return res, err
}
