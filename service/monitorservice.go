package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type MonitorService interface {
	FindMonitors() (monitorOutput []model.Monitor, err error)
	FindMonitorById(id uint) (monitorOutput model.Monitor, err error)
	FindExcMonitor(id uint) (monitorOutput []model.Monitor, err error)
	InsertMonitor(monitor model.CreateMonitorParameter) (monitorOutput model.Monitor, err error)
	UpdateMonitor(monitor model.Monitor, id uint) (monitorOutput model.Monitor, err error)
	DeleteMonitor(monitor model.Monitor, id uint) (monitorOutput model.Monitor, err error)
}

type monitorService struct {
	monitorRepository repository.MonitorRepository
}

func NewMonitorService(monitorRep repository.MonitorRepository) MonitorService {
	return &monitorService{
		monitorRepository: monitorRep,
	}
}

func (service *monitorService) FindMonitors() (monitorOutput []model.Monitor, err error) {
	res, err := service.monitorRepository.FindMonitors()
	return res, err
}

func (service *monitorService) FindMonitorById(id uint) (monitorOutput model.Monitor, err error) {
	res, err := service.monitorRepository.FindMonitorById(id)
	return res, err
}

func (service *monitorService) FindExcMonitor(id uint) (monitorOutput []model.Monitor, err error) {
	res, err := service.monitorRepository.FindExcMonitor(id)
	return res, err
}

func (service *monitorService) InsertMonitor(monitor model.CreateMonitorParameter) (monitorOutput model.Monitor, err error) {
	newMonitor := model.Monitor{}
	err1 := smapping.FillStruct(&newMonitor, smapping.MapFields(&monitor))
	if err != nil {
		return newMonitor, err1
	}
	res, err := service.monitorRepository.InsertMonitor(newMonitor)
	return res, err
}

func (service *monitorService) UpdateMonitor(monitor model.Monitor, id uint) (monitorOutput model.Monitor, err error) {
	newMonitor := model.Monitor{}
	err1 := smapping.FillStruct(&newMonitor, smapping.MapFields(&monitor))
	if err != nil {
		return newMonitor, err1
	}
	res, err := service.monitorRepository.UpdateMonitor(newMonitor, id)
	return res, err
}

func (service *monitorService) DeleteMonitor(monitor model.Monitor, id uint) (monitorOutput model.Monitor, err error) {
	newMonitor := model.Monitor{}
	err1 := smapping.FillStruct(&newMonitor, smapping.MapFields(&monitor))
	if err != nil {
		return newMonitor, err1
	}
	res, err := service.monitorRepository.UpdateMonitor(newMonitor, id)
	return res, err
}
