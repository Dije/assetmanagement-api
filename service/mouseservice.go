package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type MouseService interface {
	FindMouses() (mouseOutput []model.Mouse, err error)
	FindMouseById(id uint) (mouseOutput model.Mouse, err error)
	FindExcMouse(id uint) (mouseOutput []model.Mouse, err error)
	InsertMouse(mouse model.CreateMouseParameter) (mouseOutput model.Mouse, err error)
	UpdateMouse(mouse model.Mouse, id uint) (mouseOutput model.Mouse, err error)
	DeleteMouse(mouse model.Mouse, id uint) (mouseOutput model.Mouse, err error)
}

type mouseService struct {
	mouseRepository repository.MouseRepository
}

func NewMouseService(mouseRep repository.MouseRepository) MouseService {
	return &mouseService{
		mouseRepository: mouseRep,
	}
}

func (service *mouseService) FindMouses() (mouseOutput []model.Mouse, err error) {
	res, err := service.mouseRepository.FindMouses()
	return res, err
}

func (service *mouseService) FindMouseById(id uint) (mouseOutput model.Mouse, err error) {
	res, err := service.mouseRepository.FindMouseById(id)
	return res, err
}

func (service *mouseService) FindExcMouse(id uint) (mouseOutput []model.Mouse, err error) {
	res, err := service.mouseRepository.FindExcMouse(id)
	return res, err
}

func (service *mouseService) InsertMouse(mouse model.CreateMouseParameter) (mouseOutput model.Mouse, err error) {
	newMouse := model.Mouse{}
	err1 := smapping.FillStruct(&newMouse, smapping.MapFields(&mouse))
	if err != nil {
		return newMouse, err1
	}
	res, err := service.mouseRepository.InsertMouse(newMouse)
	return res, err
}

func (service *mouseService) UpdateMouse(mouse model.Mouse, id uint) (mouseOutput model.Mouse, err error) {
	newMouse := model.Mouse{}
	err1 := smapping.FillStruct(&newMouse, smapping.MapFields(&mouse))
	if err != nil {
		return newMouse, err1
	}
	res, err := service.mouseRepository.UpdateMouse(newMouse, id)
	return res, err
}

func (service *mouseService) DeleteMouse(mouse model.Mouse, id uint) (mouseOutput model.Mouse, err error) {
	newMouse := model.Mouse{}
	err1 := smapping.FillStruct(&newMouse, smapping.MapFields(&mouse))
	if err != nil {
		return newMouse, err1
	}
	res, err := service.mouseRepository.UpdateMouse(newMouse, id)
	return res, err
}
