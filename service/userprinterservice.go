package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type UserPrinterService interface {
	FindUserPrinters() (userPrinterOutput []model.SelectUserPrinterParameter, err error)
	FindUserPrinterById(id uint) (userPrinterOutput model.SelectUserPrinterParameter, err error)
	FindExcUserPrinter(id uint) (userPrinterOutput []model.SelectUserPrinterParameter, err error)
	InsertUserPrinter(userPrinter model.CreateUserPrinterParameter) (userPrinterOutput model.UserPrinter, err error)
	UpdateUserPrinter(userPrinter model.UserPrinter, id uint) (userPrinterOutput model.UserPrinter, err error)
	DeleteUserPrinter(userPrinter model.UserPrinter, id uint) (userPrinterOutput model.UserPrinter, err error)
}

type userPrinterService struct {
	userPrinterRepository repository.UserPrinterRepository
}

func NewUserPrinterService(userPrinterRep repository.UserPrinterRepository) UserPrinterService {
	return &userPrinterService{
		userPrinterRepository: userPrinterRep,
	}
}

func (service *userPrinterService) FindUserPrinters() (userPrinterOutput []model.SelectUserPrinterParameter, err error) {
	res, err := service.userPrinterRepository.FindUserPrinters()
	return res, err
}

func (service *userPrinterService) FindUserPrinterById(id uint) (userPrinterOutput model.SelectUserPrinterParameter, err error) {
	res, err := service.userPrinterRepository.FindUserPrinterById(id)
	return res, err
}

func (service *userPrinterService) FindExcUserPrinter(id uint) (userPrinterOutput []model.SelectUserPrinterParameter, err error) {
	res, err := service.userPrinterRepository.FindExcUserPrinter(id)
	return res, err
}

func (service *userPrinterService) InsertUserPrinter(userPrinter model.CreateUserPrinterParameter) (userPrinterOutput model.UserPrinter, err error) {
	newUserPrinter := model.UserPrinter{}
	err1 := smapping.FillStruct(&newUserPrinter, smapping.MapFields(&userPrinter))
	if err != nil {
		return newUserPrinter, err1
	}
	res, err := service.userPrinterRepository.InsertUserPrinter(newUserPrinter)
	return res, err
}

func (service *userPrinterService) UpdateUserPrinter(userPrinter model.UserPrinter, id uint) (userPrinterOutput model.UserPrinter, err error) {
	newUserPrinter := model.UserPrinter{}
	err1 := smapping.FillStruct(&newUserPrinter, smapping.MapFields(&userPrinter))
	if err != nil {
		return newUserPrinter, err1
	}
	res, err := service.userPrinterRepository.UpdateUserPrinter(newUserPrinter, id)
	return res, err
}

func (service *userPrinterService) DeleteUserPrinter(userPrinter model.UserPrinter, id uint) (userPrinterOutput model.UserPrinter, err error) {
	newUserPrinter := model.UserPrinter{}
	err1 := smapping.FillStruct(&newUserPrinter, smapping.MapFields(&userPrinter))
	if err != nil {
		return newUserPrinter, err1
	}
	res, err := service.userPrinterRepository.UpdateUserPrinter(newUserPrinter, id)
	return res, err
}
