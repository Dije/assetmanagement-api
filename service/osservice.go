package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type OSService interface {
	FindOSs() (osOutput []model.OS, err error)
	FindOSById(id uint) (osOutput model.OS, err error)
	FindExcOS(id uint) (osOutput []model.OS, err error)
	InsertOS(os model.CreateOSParameter) (osOutput model.OS, err error)
	UpdateOS(os model.OS, id uint) (osOutput model.OS, err error)
	DeleteOS(os model.OS, id uint) (osOutput model.OS, err error)
}

type osService struct {
	osRepository repository.OSRepository
}

func NewOSService(osRep repository.OSRepository) OSService {
	return &osService{
		osRepository: osRep,
	}
}

func (service *osService) FindOSs() (osOutput []model.OS, err error) {
	res, err := service.osRepository.FindOSs()
	return res, err
}

func (service *osService) FindOSById(id uint) (osOutput model.OS, err error) {
	res, err := service.osRepository.FindOSById(id)
	return res, err
}

func (service *osService) FindExcOS(id uint) (osOutput []model.OS, err error) {
	res, err := service.osRepository.FindExcOS(id)
	return res, err
}

func (service *osService) InsertOS(os model.CreateOSParameter) (osOutput model.OS, err error) {
	newOS := model.OS{}
	err1 := smapping.FillStruct(&newOS, smapping.MapFields(&os))
	if err != nil {
		return newOS, err1
	}
	res, err := service.osRepository.InsertOS(newOS)
	return res, err
}

func (service *osService) UpdateOS(os model.OS, id uint) (osOutput model.OS, err error) {
	newOS := model.OS{}
	err1 := smapping.FillStruct(&newOS, smapping.MapFields(&os))
	if err != nil {
		return newOS, err1
	}
	res, err := service.osRepository.UpdateOS(newOS, id)
	return res, err
}

func (service *osService) DeleteOS(os model.OS, id uint) (osOutput model.OS, err error) {
	newOS := model.OS{}
	err1 := smapping.FillStruct(&newOS, smapping.MapFields(&os))
	if err != nil {
		return newOS, err1
	}
	res, err := service.osRepository.UpdateOS(newOS, id)
	return res, err
}
