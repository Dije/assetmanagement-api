package service

import (
	"deliportal-api/model"
	"deliportal-api/repository"

	"github.com/mashingan/smapping"
)

type LaptopService interface {
	FindLaptops() (laptopOutput []model.SelectLaptopParameter, err error)
	FindLaptopById(id uint) (laptopOutput model.SelectLaptopParameter, err error)
	FindExcLaptop(id uint) (laptopOutput []model.SelectLaptopParameter, err error)
	InsertLaptop(laptop model.CreateLaptopParameter) (laptopOutput model.Laptop, err error)
	UpdateLaptop(laptop model.Laptop, id uint) (laptopOutput model.Laptop, err error)
	DeleteLaptop(laptop model.Laptop, id uint) (laptopOutput model.Laptop, err error)
}

type laptopService struct {
	laptopRepository repository.LaptopRepository
}

func NewLaptopService(laptopRep repository.LaptopRepository) LaptopService {
	return &laptopService{
		laptopRepository: laptopRep,
	}
}

func (service *laptopService) FindLaptops() (laptopOutput []model.SelectLaptopParameter, err error) {
	res, err := service.laptopRepository.FindLaptops()
	return res, err
}

func (service *laptopService) FindLaptopById(id uint) (laptopOutput model.SelectLaptopParameter, err error) {
	res, err := service.laptopRepository.FindLaptopById(id)
	return res, err
}

func (service *laptopService) FindExcLaptop(id uint) (laptopOutput []model.SelectLaptopParameter, err error) {
	res, err := service.laptopRepository.FindExcLaptop(id)
	return res, err
}

func (service *laptopService) InsertLaptop(laptop model.CreateLaptopParameter) (laptopOutput model.Laptop, err error) {
	newLaptop := model.Laptop{}
	err1 := smapping.FillStruct(&newLaptop, smapping.MapFields(&laptop))
	if err != nil {
		return newLaptop, err1
	}
	res, err := service.laptopRepository.InsertLaptop(newLaptop)
	return res, err
}

func (service *laptopService) UpdateLaptop(laptop model.Laptop, id uint) (laptopOutput model.Laptop, err error) {
	newLaptop := model.Laptop{}
	err1 := smapping.FillStruct(&newLaptop, smapping.MapFields(&laptop))
	if err != nil {
		return newLaptop, err1
	}
	res, err := service.laptopRepository.UpdateLaptop(newLaptop, id)
	return res, err
}

func (service *laptopService) DeleteLaptop(laptop model.Laptop, id uint) (laptopOutput model.Laptop, err error) {
	newLaptop := model.Laptop{}
	err1 := smapping.FillStruct(&newLaptop, smapping.MapFields(&laptop))
	if err != nil {
		return newLaptop, err1
	}
	res, err := service.laptopRepository.DeleteLaptop(newLaptop, id)
	return res, err
}
